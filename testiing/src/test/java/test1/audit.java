package test1;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;

import pages.*;
public class audit {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {	System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);}
 
  
	






///////////////////////////////////////////////////////////////////////






@DataProvider(name="adminaudit")
public Object[][] adminauditdata() throws IOException{
	
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("audit1","excel/agilen.xlsx");
	
  return object;	 
}






@Test(dataProvider ="adminaudit", priority=2)
public void adminauditf(String plants,String atrole ,String startdate,String enddate, String n) throws InterruptedException{



	 try {
		 
		 
		 System.out.println("audit  clicked");	
		 this.driver=base.driver;
			agilepg o = PageFactory.initElements(driver, agilepg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
		 
		 if(o.usrreg.isDisplayed()){	 Thread.sleep(3000);
			
			System.out.println("clicked ");}
		 else{
			 System.out.println("administrator clicked then fmaster ");
				oo.clickpg(o.administrator);Thread.sleep(5000);
				
			}

		 
		 
			oo.clickpg(o.audit1);
			
			Thread.sleep(3000);
			oo.waits(o.bplants);			 
				
				oo.dropdownpg(o.plants, plants);
				 Thread.sleep(3000);
				 
				 oo.dropdownpg(o.atrole, atrole);
				 Thread.sleep(3000);
				 
				 oo.clickpg(o.selectdate1);
				 
				 Thread.sleep(3000);
				 
				 double d = Double.parseDouble(startdate);
					int i = (int) d;	
				 oo.calenderpg(o.bcalstartt,String.valueOf(i));
				 
				 Thread.sleep(2000);
				 
				 oo.clickpg(o.selectdate2);
				 
				 Thread.sleep(2000);
				 
				 double dd = Double.parseDouble(enddate);
					int ii = (int) dd;
				 oo.calenderpg(o.bcalstartt,String.valueOf(ii));
				 Thread.sleep(4000);
				 oo.clickpg(o.sub);
				 
				 
				 Thread.sleep(5000);
				 				 
				 if(o.closebtn.isDisplayed())
				 {
				 Thread.sleep(3000);
				 o.closebtn.click();
				 System.out.println(" NO data present...............");
				 Logger ll =Logger.getLogger("audit ");
				 ll.trace("no data in audit trails");

				 
				 }else{
					 System.out.println(" data present "); Thread.sleep(2000);
					 oo.jsdownbye(o.divaudit); Thread.sleep(2000);
					 oo.srnsht(driver);
					
					 
					 Thread.sleep(3000);
				
						if(n.contains(".")){
							oo.textipg(o.auditsearch, n);
						}else{
							oo.textpg(o.auditsearch,n);
						
						}
					 String p =o.auditsearchbody.getText();
						 Logger ll =Logger.getLogger("nani");
							ll.trace(p);
				System.out.println(p +"ddddddd"); 
					} 
				
				 
				 
		
			 
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());

	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("roleadd with valid input");
		 Assert.assertTrue(true);
		 
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
		 
	 
		
		}
	 
	 
	 
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{	System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);} 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
	
 extent.flush();

}



/*@AfterSuite
public void tearDown() {
   driver.quit();
}
*/

 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
