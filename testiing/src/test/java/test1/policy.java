package test1;

import java.awt.List;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;

import pages.*;
public class policy {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {System.out.println("testing...");
	 test = extent.createTest("plant clicked2");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);}
 
  
	






///////////////////////////////////////////////////////////////////////






@DataProvider(name="addpolicy")
public Object[][] addpolicydata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("addpolicy","excel/agilen.xlsx");
	
  return object;	 
}






//public  void adminuseraddf(String name, String n,String alname,String fname, String lastn,String gen,String email,String pwd, String cpwd,String mnum, String rnum,String roles, String add1, String add2,String coun,String state,String city, String zip) throws InterruptedException {
	@Test(dataProvider ="addpolicy", priority=2)
public void addpolicyf(String au,String useridmin,String useridmax,String usernamemin,String usernamemax,String pwdage,String attacktolck,String Pwdlength,String resetpwd,String passwordcomplexity,String session,String Remark,String ppwd) throws InterruptedException, IOException{


//System.out.println(driver.manage().getCookies());

	 try {
		 
		 
		 System.out.println("addpolicy  clicked");	
		 this.driver=base.driver;
			agilepg o = PageFactory.initElements(driver, agilepg.class);
			util oo = PageFactory.initElements(driver, util.class);
			 if(o.usrreg.isDisplayed()){	 Thread.sleep(3000);
				
				System.out.println("clicked ");}
			 else{
				 System.out.println("administrator clicked then fmaster ");
					oo.clickpg(o.administrator);Thread.sleep(5000);
					
				}

		
		 Thread.sleep(3000);
			oo.clickpg(o.policy);
			 Thread.sleep(2000);
			 
			 
			 
			 if(au.equals("update")){
			oo.jsdownbye(o.policyud); Thread.sleep(3000);
			 oo.clickpg(o.policyud);
			 }
			 Thread.sleep(2000);
	
			 o.passwordcomplexity.getText();
			
	
			 
			 
			 // policy details
			 oo.clearpg(o.useridmin);
			 Thread.sleep(3000);
				
				oo.textipg(o.useridmin, useridmin);
				
				 oo.clearpg(o.useridmax);
				 Thread.sleep(2000);
				 oo.textipg(o.useridmax, useridmax);
				
				 oo.clearpg(o.usernamemin);
				 Thread.sleep(2000);
				 oo.textipg(o.usernamemin, usernamemin);
				 
				 oo.clearpg(o.usernamemax);
				 Thread.sleep(2000);
				 oo.textipg(o.usernamemax, usernamemax);
				 oo.clearpg(o.pwdage);
				 Thread.sleep(2000);
				 oo.textipg(o.pwdage, pwdage);
				 
				 
				 oo.clearpg(o.attacktolck);
				 Thread.sleep(2000);
				 oo.textipg(o.attacktolck, attacktolck);
				 oo.clearpg(o.Pwdlength);
				 Thread.sleep(2000);
				 oo.textipg(o.Pwdlength, Pwdlength);
				 oo.clearpg(o.resetpwd);
				 Thread.sleep(2000);
				 oo.textipg(o.resetpwd, resetpwd);
				 
				 
				 
				 
				 Thread.sleep(2000);
				 
				 Select select = new Select(o.passwordcomplexity);
				 WebElement option = select.getFirstSelectedOption();
				 String defaultItem = option.getText();
				 System.out.println(defaultItem );
		
			
				 if(defaultItem.equals(passwordcomplexity)){
					 System.out.println("same dropdown already");
				 }else{ System.out.println(" dropdown ");
				oo.dropdownpg(o.passwordcomplexity, passwordcomplexity);
				 }
				
				
				
				oo.clearpg(o.session);
				Thread.sleep(2000);
				 oo.textipg(o.session, session);
				 
				 
				 Thread.sleep(2000); 
				 
				 oo.jsdownbye(o.submit);
				 oo.clickpg(o.submit);
				 Thread.sleep(2000);
				 
				 
				 Thread.sleep(3000);
					oo.waits(o.bRemark);
					
					Thread.sleep(2000);
					 oo.textpg(o.Remark, Remark);
					Thread.sleep(2000);
					 oo.textpg(o.pwd, ppwd);Thread.sleep(2000);
					 
					 //temp we are not submit so refresh code
					//driver.navigate().refresh();
					 
					 
					 
					 oo.clickpg(o.validateuser);
					 oo.waits(o.bERES);
					 Thread.sleep(3000);
					 		 Assert.assertEquals(o.paalert.getText(), "Electronic Signature Process Completed Successfully");				 
					 						 
					 
					 oo.clickpg(o.ERES);
					 	Thread.sleep(3000);
					 	
					 	Logger ll =Logger.getLogger("n role ");
					 	ll.trace(o.paalert.getText());

					 	Assert.assertEquals(o.paalert.getText(), "Policy Settings Updated Successfully");
					 oo.waits(o.bokbtn);	 
					 oo.clickpg(o.okbtn);
					 Thread.sleep(2000);
				 
				 
			 
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);

	 	}
	 	test = extent.createTest("depaddf");
		test.createNode("dep with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
		
		}
	 
	 
	 
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{System.out.println("testing...");
	 test = extent.createTest("plant clicked2");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);} 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
	
 extent.flush();

}



/*@AfterSuite
public void tearDown() {
   driver.quit();
}
*/

 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
