package reports;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import pages.agilepg;
import plantpages.*;
import test1.base;
import planttests.*;

public class nholdareaR {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {
		System.out.println("testing...");
		 test = extent.createTest("plant clicked");
			test.createNode("click on plant2");
			 Assert.assertTrue(true);
			 /*

		this.driver = plantbase.driver;
		Thread.sleep(900);
		System.out.println("testing on master module ");
		plantpg o = PageFactory.initElements(driver, plantpg.class);
		util oo = PageFactory.initElements(driver, util.class);
		
		try {	
			//oo.flu(o.badmin); 
			Thread.sleep(4000);
		//	oo.clickpg(o.master);
		Thread.sleep(3000);//oo.clickpg(o.usrreg);
		
		}catch (Exception e) {
			// TODO: handle exception
		System.out.println(e.getMessage());
			
		}
		 test = extent.createTest("master");
		test.createNode("click on master");
		 Assert.assertTrue(true);
	
	*/}
 
  
	






///////////////////////////////////////////////////////////////////////






@DataProvider(name="holdareaR")
public Object[][] holdareaRadddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("nholdareaR","excel/reports.xlsx");
	
  return object;	 
}






//public  void adminuseraddf(String name, String n,String alname,String fname, String lastn,String gen,String email,String pwd, String cpwd,String mnum, String rnum,String roles, String add1, String add2,String coun,String state,String city, String zip) throws InterruptedException {
	@Test(dataProvider ="holdareaR", priority=2)
public void holdareaRaddf(String holdarea, String v1, String v2) throws InterruptedException, IOException{



	 try {
		 
		 System.out.println("role  clicked");	
		 this.driver=plantbase.driver;
			plantpg o = PageFactory.initElements(driver, plantpg.class);
			util oo = PageFactory.initElements(driver, util.class);
			
	 Thread.sleep(4000);
			 if(o.har.isDisplayed()){	 Thread.sleep(3000);
			
				System.out.println("clicked ");}
			 else{
				 System.out.println("matster clicked then fmaster ");
					oo.clickpg(o.reports);Thread.sleep(5000);
					
				}
			
			
			 oo.waits(o.bhar);
		 Thread.sleep(4000);
		 
	
			oo.clickpg(o.har);
				Thread.sleep(3000);
		 //create
			
				oo.waits(o.bholdarea);
				
				if(holdarea==null || holdarea.equals(""))
				 {}else{
				Thread.sleep(3000);			
				oo.dropdownpg(o.holdarea, holdarea);
				
				 }
				
				Thread.sleep(3000);	
				oo.waits(o.brbholdin);
				oo.clickpg(o.rbholdin);
				Thread.sleep(3000);	
				if(v1==null || v1.equals(""))
				 {}else{
				oo.clickpg(o.selectdate1);
				Thread.sleep(3000);				
				oo.calenderpg(o.bdp, v1);
				 }
				Thread.sleep(5000);
			oo.waits(o.bselectdate2);
			if(v2==null || v2.equals(""))
			 {}else{
			Thread.sleep(3000);	
				oo.clickpg(o.selectdate2);				
				Thread.sleep(5000);
				oo.waits(o.bdp1);
				oo.calenderpg(o.bdp, v2);
			 }
			
			
				Thread.sleep(5000);	
				oo.clickpg(o.sub);
				
				Thread.sleep(3000);	

			if(o.hab.getText().equals(""))
			{
				System.out.println("report  no data gettting");
			}
			else {
				System.out.println("data getting");		// oo.wait(8000);
			}
				
	
		
				 
				
		 
	 }catch (Exception e)
	 {
		 System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 System.out.println(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);
	 
	 
	 
	 
	 	}
	 	test = extent.createTest("block master");
		test.createNode("block master information");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
			
			
		}
	 
	 
	 
	 
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{
		
		System.out.println("tested");
		 test = extent.createTest("plant clicked");
			test.createNode("click on plant2");
			 Assert.assertTrue(true);
			 
			 /*



		this.driver = plantbase.driver;
		Thread.sleep(100);
		System.out.println("testing on admin role module2 ");
		plantpg o = PageFactory.initElements(driver, plantpg.class);
		util oo = PageFactory.initElements(driver, util.class);
		
		try {	
			//oo.flu(o.badmin); 
			Thread.sleep(7000);
			oo.clickpg(o.master);
		Thread.sleep(3000);//oo.clickpg(o.usrreg);
		
		}catch (Exception e) {
			// TODO: handle exception
		System.out.println(e.getMessage());
			
		}
		 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
		
		
	*/}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
