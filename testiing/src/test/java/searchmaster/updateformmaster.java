package searchmaster;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import pages.agilepg;
import plantpages.*;
import test1.base;
import planttests.*;
public class updateformmaster {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {	System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);}
 
  
	






///////////////////////////////////////////////////////////////////////






@DataProvider(name="addbmaster")
public Object[][] adminuseradddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("updatef","excel/plant.xlsx");
	
  return object;	 
}






//public  void adminuseraddf(String name, String n,String alname,String fname, String lastn,String gen,String email,String pwd, String cpwd,String mnum, String rnum,String roles, String add1, String add2,String coun,String state,String city, String zip) throws InterruptedException {
	@Test(dataProvider ="addbmaster", priority=2)
public void searchformf(String dd, String status, String fn , String Remark , String ppwd ) throws InterruptedException, IOException{



	try {
		 
		 System.out.println("form master  clicked");	
		 this.driver=plantbase.driver;
			plantpg o = PageFactory.initElements(driver, plantpg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
		 
		 
	
		 
		 if(o.fmasterl.isDisplayed()){	 Thread.sleep(3000);
		
			System.out.println("clicked ");}
		 else{
			 System.out.println("matster clicked then fmaster ");
				oo.clickpg(o.master);Thread.sleep(5000);
				
			}
			
		 oo.waits(o.bfmasterl);
		 Thread.sleep(3000);
			oo.clickpg(o.fmasterl);
			 Thread.sleep(3000);		
				oo.clickpg(o.drs);
			 Thread.sleep(3000);
			 
			
			 oo.textpg(o.fsearch, dd);
			 Thread.sleep(3000);
			 
			 
			 List <WebElement> searchusize =driver.findElements(By.xpath(o.bformaster));
				
				System.out.println(searchusize.size()+"size......");
				
				
				
				if(searchusize.size()>1){		Thread.sleep(3000);	
				System.out.println("matched more rows");
				
				Thread.sleep(4000);	
			
			
					System.out.println("nagaraju.............");
					Thread.sleep(3000);
					//oo.formupdate(o.bform, dd, status);
					
					oo.ulocation(o.bform,"uf", dd, "/td[1]", "/td[2]");
					
					
					
					Thread.sleep(4000);	
					
				
					//System.out.println(o.bfdiv4.getText()+"tt");	
					//System.out.println(o.bfdiv4.getText()+"aa");
					
						System.out.println("update/drop now");
						Thread.sleep(4000);
					///	oo.clickpg(o.bfdiv3);
						
						
						////////
					
						
						List <WebElement> searchusize1 =driver.findElements(By.xpath(o.FORMULATIONNAME1));
						System.out.println(searchusize1.size()+"dddddd");
						//System.out.println(o.FORMULATIONNAME.isDisplayed()+"dsssssssssssssss");
						
						if(searchusize1.size()==1){
							Thread.sleep(4000);
							System.out.println("size");
						if(status.equals("delete")){
							Thread.sleep(2000);
							System.out.println("delete here ");
							oo.waits(o.bfdeactive);
							Thread.sleep(4000);
							oo.clickpg(o.fdeactive);
							
							
							 Thread.sleep(3000);						
								oo.waits(o.bRemark);						
								Thread.sleep(2000);
								 oo.textpg(o.Remark, Remark);
								Thread.sleep(2000);
								 oo.textpg(o.pwd, ppwd);Thread.sleep(2000);
								 oo.clickpg(o.validateuser);
							
								 //alert 
								 Thread.sleep(2000);
								 
								 	if(o.uaalert.getText().equals("Electronic Signature Process Completed Successfully")){
									 
								 
								
								 
								 Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");
								 oo.clickpg(o.ERES);Thread.sleep(2000);											
							 Assert.assertEquals(o.uaalert.getText(), "Formulation Master De-Activation Process Completed Successfully");
							 Thread.sleep(2000);
							 
							 Logger ll =Logger.getLogger("for d ");
								ll.trace(o.uaalert.getText());
							 oo.clickpg(o.okbtn);
							 
							 
								
									 }else{
										 Logger ll =Logger.getLogger("for d ");
											ll.trace(o.uaalert.getText());
										// Assert.assertEquals(o.uaalert.getText(), "FORMULATION HAS BEEN ASSINGED TO LOCATIONS CANNOT DE-ACTIVE");
										 oo.waits(o.bErrorbtn);	 
										 oo.clickpg(o.Errorbtn);	Thread.sleep(2000);	
								 
									}
					
							
						}
						if(status.equals("update")){
							System.out.println("update here");
							oo.waits(o.bfupdate);
							 Thread.sleep(4000);				 
												 						 
							/* JavascriptExecutor js = (JavascriptExecutor)driver;
							 js.executeScript("arguments[0].value = '';", o.FORMULATIONNAME);
							 Thread.sleep(4000);*/
						
							oo.clearpg(o.FORMULATIONNAME);
							Thread.sleep(2000);
							oo.textpg(o.FORMULATIONNAME,fn);
							 Thread.sleep(2000);
							
							oo.clickpg(o.fupdate);
								 
							
							Thread.sleep(3000);						
							oo.waits(o.bRemark);						
							Thread.sleep(2000);
							 oo.textpg(o.Remark, Remark);
							Thread.sleep(2000);
							 oo.textpg(o.pwd, ppwd);Thread.sleep(2000);
							 oo.clickpg(o.validateuser);
							 
							 
							Thread.sleep(2000);
							 if(o.uaalert.getText().equals("Electronic Signature Process Completed Successfully")){
							
							 
								Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");
								 oo.clickpg(o.ERES);Thread.sleep(2000);
								 Logger ll =Logger.getLogger("for u ");
									ll.trace(o.uaalert.getText());
								
									 Assert.assertEquals(o.uaalert.getText(), "Formulation Master Modification Process Completed Successfully");
									 Thread.sleep(2000);
									 oo.clickpg(o.okbtn);
							 
							 
							 }
							 else	 {
								 Logger ll =Logger.getLogger("for u ");
									ll.trace(o.uaalert.getText());
								// Assert.assertEquals(o.uaalert.getText(), "FORMULATION HAS BEEN ASSIGNED TO LOCATIONS,CANNOT BE MODIFIED");
								 oo.waits(o.bErrorbtn);	 
								 oo.clickpg(o.Errorbtn);	Thread.sleep(2000);	
						
							 }
								 
								 
							 
								 
							}
						
			
					////////////
					
					
						}
					
						else{
							System.out.println("not active record");
							Logger ll =Logger.getLogger("deactive");
							ll.trace("deactive");
							
						}
			
					
					
				
				
					}
			else{
					
						List <WebElement> searchusize1 =driver.findElements(By.xpath(o.bfdiv));
						if(searchusize1.size()>1){		Thread.sleep(3000);	
							System.out.println("record present");
							//System.out.println(o.ussearchdiv1.getText()+"text");
							String ee1=o.bfdiv1.getText();
							String ee2=o.bfdiv2.getText();		Thread.sleep(3000);	
							if(ee1.equals(dd))
							{
								
								Logger ll =Logger.getLogger("formation id ");
							ll.trace(dd);
								System.out.println("formation id");
								
								
								
								
							}else 	if(ee2.equals(dd)){
								
								
								Logger ll =Logger.getLogger("formation name ");
								ll.trace(dd);
								System.out.println("formation name");
							
								
								
							}
							
							
							System.out.println(o.bfdiv4.getText()+"tt");	
							System.out.println(o.bfdiv4.getText()+"aa");
							if(o.bfdiv4.getText().equals("ACTIVE")){
								System.out.println("update/drop now");
								Thread.sleep(4000);
								oo.clickpg(o.bfdiv3);
								
								
								////////
								
								if(status.equals("delete")){
									oo.waits(o.bfdeactive);
									Thread.sleep(2000);
									oo.clickpg(o.fdeactive);
									
									
									 Thread.sleep(3000);						
										oo.waits(o.bRemark);						
										Thread.sleep(2000);
										 oo.textpg(o.Remark, Remark);
										Thread.sleep(2000);
										 oo.textpg(o.pwd, ppwd);Thread.sleep(2000);
										 oo.clickpg(o.validateuser);
									
										 //alert 
										 Thread.sleep(2000);
										 
										 	if(o.uaalert.getText().equals("Electronic Signature Process Completed Successfully")){
											 
										 		Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");
												 oo.clickpg(o.ERES);Thread.sleep(2000);											
											 Assert.assertEquals(o.uaalert.getText(), "Formulation Master De-Activation Process Completed Successfully");
											 Thread.sleep(2000);
											 oo.clickpg(o.okbtn);
									
											 Logger ll =Logger.getLogger("for d ");
												ll.trace(o.uaalert.getText());
										 
										
											 }else{
												 Logger ll =Logger.getLogger("for d ");
													ll.trace(o.uaalert.getText());
												// Assert.assertEquals(o.uaalert.getText(), "FORMULATION HAS BEEN ASSINGED TO LOCATIONS CANNOT DE-ACTIVE");
												 oo.waits(o.bErrorbtn);	 
												 oo.clickpg(o.Errorbtn);	Thread.sleep(2000);	
											
										 
											 }
							
									
								}
								if(status.equals("update")){
									
									 Thread.sleep(2000);oo.clearpg(o.FORMULATIONNAME);
										Thread.sleep(2000);
									
									oo.textpg(o.FORMULATIONNAME,fn);
									 Thread.sleep(2000);
									
									oo.clickpg(o.fupdate);
										 
									
									Thread.sleep(3000);						
									oo.waits(o.bRemark);						
									Thread.sleep(2000);
									 oo.textpg(o.Remark, Remark);
									Thread.sleep(2000);
									 oo.textpg(o.pwd, ppwd);Thread.sleep(2000);
									 oo.clickpg(o.validateuser);
									 
									 
									Thread.sleep(2000);
									 if(o.uaalert.getText().equals("Electronic Signature Process Completed Successfully")){
									
									

										Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");
										 oo.clickpg(o.ERES);Thread.sleep(2000);
									
										 Logger ll =Logger.getLogger("for u ");
									      	ll.trace(o.uaalert.getText());
											 Assert.assertEquals(o.uaalert.getText(), "Formulation Master Modification Process Completed Successfully");
											 Thread.sleep(2000);
											 oo.clickpg(o.okbtn);
										 
											 

									 
									 }
									 else	 {
										 Logger ll =Logger.getLogger("for u ");
									      	ll.trace(o.uaalert.getText());

										// Assert.assertEquals(o.uaalert.getText(), "FORMULATION HAS BEEN ASSIGNED TO LOCATIONS,CANNOT BE MODIFIED");
										 oo.waits(o.bErrorbtn);	 
										 oo.clickpg(o.Errorbtn);	Thread.sleep(2000);	
									 }
										 
										 
									 
										 
									}
								
								
							}
							
							
							
							
							
						}else{
							System.out.println("no record present");
							//trying to fail bcs no data case screenshot 
							test = extent.createTest("no search results");
							test.createNode("input data based results not there");
							 Assert.assertTrue(true);
						}
					
			}
			
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
		 
		
				 
				
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);

	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		
	
		}
	 
	 
	 
	 
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{	System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
