package searchmaster;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import pages.agilepg;
import plantpages.*;
import test1.base;
import planttests.*;
public class updateamaster {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {	System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);}
 
  
	






///////////////////////////////////////////////////////////////////////






@DataProvider(name="addbmaster")
public Object[][] adminuseradddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("updatea","excel/plant.xlsx");
	
  return object;	 
}





//public  void adminuseraddf(String name, String n,String alname,String fname, String lastn,String gen,String email,String pwd, String cpwd,String mnum, String rnum,String roles, String add1, String add2,String coun,String state,String city, String zip) throws InterruptedException {
	@Test(dataProvider ="addbmaster", priority=2)
public void searclocf(String dd, String status, String Remark, String ppwd,String AREANAME) throws InterruptedException, IOException{



	 try {
		 
		 System.out.println("form master  clicked");	
		 this.driver=plantbase.driver;
			plantpg o = PageFactory.initElements(driver, plantpg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
		 
		 
	
		 
		 if(o.fmasterl.isDisplayed()){	 Thread.sleep(3000);
		
			System.out.println("clicked ");}
		 else{
			 System.out.println("matster clicked then fmaster ");
				oo.clickpg(o.master);Thread.sleep(5000);
				
			}
			
		 oo.waits(o.bamaster1);
		 Thread.sleep(3000);
			oo.clickpg(o.amaster1);
			 Thread.sleep(3000);		
				oo.clickpg(o.drs);
			 Thread.sleep(3000);
			 
			
			 oo.textpg(o.fsearch1, dd);
			 Thread.sleep(3000);
			 
			 
			 List <WebElement> searchusize =driver.findElements(By.xpath(o.bformastern));
				
				System.out.println(searchusize.size()+"size......");
				
				
				
				if(searchusize.size()>1){		Thread.sleep(3000);	
				System.out.println("matched more rows");
				
				Thread.sleep(3000);	
			oo.ulocation(o.bform,"ua", dd, "/td[3]", "/td[4]");
			
			
			
			System.out.println("update/drop now");
			Thread.sleep(4000);
			
			/////////
			List <WebElement> searchusize1 =driver.findElements(By.xpath(o.AREANAME1));
			if(searchusize1.size()==1){

			if(status.equals("delete")){
			
				oo.waits(o.bdroparea);
				Thread.sleep(2000);
				oo.jsdownbye(o.droparea);Thread.sleep(4000);
				oo.clickpg(o.droparea);
				
				
				 Thread.sleep(3000);						
					oo.waits(o.bRemark);						
					Thread.sleep(2000);
					 oo.textpg(o.Remark, Remark);
					Thread.sleep(2000);
					 oo.textpg(o.pwd, ppwd);Thread.sleep(2000);
					 oo.clickpg(o.validateuser);
				
					 //alert 
					 Thread.sleep(2000);
					 
					 	if(o.uaalert.getText().equals("Electronic Signature Process Completed Successfully")){
					 		Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");
							 oo.clickpg(o.ERES);Thread.sleep(2000);	
							 Logger ll =Logger.getLogger("area d ");
						      	ll.trace(o.uaalert.getText());

						 Assert.assertEquals(o.uaalert.getText(), "Area Master Deactive Process Completed Successfully");
						 Thread.sleep(2000);
						 oo.clickpg(o.okbtn);
					 
					
					 
					 
					
						 }else{
							 Logger ll =Logger.getLogger("area d ");
						      	ll.trace(o.uaalert.getText());
							 //Assert.assertEquals(o.uaalert.getText(), "EQUIPMENTS HAS BEEN ASSINGED TO AREA UNABLE TO DE-ACTIVE");
							 oo.waits(o.bErrorbtn);	 
							 oo.clickpg(o.Errorbtn);	Thread.sleep(2000);	
					}
		
				
			}
			
	
			if(status.equals("update")){
				System.out.println("update operation");
				 Thread.sleep(2000);
				 
					oo.waits(o.bAREANAME);	
					Thread.sleep(3000);
												
					oo.clearpg(o.AREANAME);	Thread.sleep(2000);							 
				 oo.textpg(o.AREANAME, AREANAME);
				
											
					 Thread.sleep(3000);oo.jsdownbye(o.uarea);Thread.sleep(4000);
				oo.clickpg(o.uarea);
					 
				
				Thread.sleep(3000);						
				oo.waits(o.bRemark);						
				Thread.sleep(2000);
				 oo.textpg(o.Remark, Remark);
				Thread.sleep(2000);
				 oo.textpg(o.pwd, ppwd);Thread.sleep(2000);
				 oo.clickpg(o.validateuser);
				 
				 
				Thread.sleep(2000);
				 if(o.uaalert.getText().equals("Electronic Signature Process Completed Successfully")){
					 
					 
					 Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");
					 oo.clickpg(o.ERES);Thread.sleep(2000);
					 Logger ll =Logger.getLogger("area u ");
				      	ll.trace(o.uaalert.getText());
					
						 Assert.assertEquals(o.uaalert.getText(), "Area Master Modification Process Completed Successfully");
						 Thread.sleep(2000);
						 oo.clickpg(o.okbtn);
			
				 	}
				 else	 {
					 Logger ll =Logger.getLogger("area u ");
				      	ll.trace(o.uaalert.getText());
					 //Assert.assertEquals(o.uaalert.getText(), "AREAS HAS BEEN ASSIGNED, CANNOT BE MODIFIED ACTIVE 1");
					 oo.waits(o.bErrorbtn);	 
					 oo.clickpg(o.Errorbtn);	Thread.sleep(2000);	
				
				 }
					 
					 
				 
					 
				}
				}	else{
					System.out.println("not active record");
					Logger ll =Logger.getLogger("deactive");
					ll.trace("deactive");
					
				}

			
			
			//////////
			
			
			
			
			
				
					}
			else{
					
						List <WebElement> searchusize1 =driver.findElements(By.xpath(o.bfdivn));
						if(searchusize1.size()>1){		Thread.sleep(3000);	
							System.out.println("record present");
							//System.out.println(o.ussearchdiv1.getText()+"text");
							String ee1=o.ldiv1n.getText();
							String ee2=o.ldiv2n.getText();		Thread.sleep(3000);	
							if(ee1.equals(dd))
							{Logger ll =Logger.getLogger("area id ");
							ll.trace(dd);
								System.out.println("area id");
							}else 	if(ee2.equals(dd)){
								Logger ll =Logger.getLogger("area name ");
								ll.trace(dd);
								System.out.println("area name");
							}
							
							
							
								if(o.adiv10.getText().equals("ACTIVE")){
								
								
							
								
								oo.clickpg(o.adiv12);
								System.out.println("update/drop now");
								Thread.sleep(4000);
								

							if(status.equals("delete")){
							
								oo.waits(o.bdroparea);
								Thread.sleep(2000);
								oo.jsdownbye(o.droparea);Thread.sleep(4000);
								oo.clickpg(o.droparea);
								
								
								 Thread.sleep(3000);						
									oo.waits(o.bRemark);						
									Thread.sleep(2000);
									 oo.textpg(o.Remark, Remark);
									Thread.sleep(2000);
									 oo.textpg(o.pwd, ppwd);Thread.sleep(2000);
									 oo.clickpg(o.validateuser);
								
									 //alert 
									 Thread.sleep(2000);
									 
									 	if(o.uaalert.getText().equals("Electronic Signature Process Completed Successfully")){
									 		Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");
											 oo.clickpg(o.ERES);Thread.sleep(2000);	
											 
												
											 Logger ll =Logger.getLogger("area d ");
										      	ll.trace(o.uaalert.getText());
										 Assert.assertEquals(o.uaalert.getText(), "Area Master Deactive Process Completed Successfully");
										 Thread.sleep(2000);
										 oo.clickpg(o.okbtn);
							

									 
									
										 }else{ Logger ll =Logger.getLogger("area d ");
									      	ll.trace(o.uaalert.getText());

											 //Assert.assertEquals(o.uaalert.getText(), "EQUIPMENTS HAS BEEN ASSINGED TO AREA UNABLE TO DE-ACTIVE");
											 oo.waits(o.bErrorbtn);	 
											 oo.clickpg(o.Errorbtn);	Thread.sleep(2000);	
									 
										}
						
								
							}
							
					
							if(status.equals("update")){
								System.out.println("update operation");
								 Thread.sleep(2000);
								 
									oo.waits(o.bAREANAME);	
									Thread.sleep(3000);
																
									oo.clearpg(o.AREANAME);	Thread.sleep(2000);							 
								 oo.textpg(o.AREANAME, AREANAME);
								
															
									 Thread.sleep(3000);oo.jsdownbye(o.uarea);Thread.sleep(4000);
								oo.clickpg(o.uarea);
									 
								
								Thread.sleep(3000);						
								oo.waits(o.bRemark);						
								Thread.sleep(2000);
								 oo.textpg(o.Remark, Remark);
								Thread.sleep(2000);
								 oo.textpg(o.pwd, ppwd);Thread.sleep(2000);
								 oo.clickpg(o.validateuser);
								 
								 
								Thread.sleep(2000);
								 if(o.uaalert.getText().equals("Electronic Signature Process Completed Successfully")){
									 
									 
									 Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");
									 oo.clickpg(o.ERES);Thread.sleep(2000);
								
									 
										
									 Logger ll =Logger.getLogger("area u ");
								      	ll.trace(o.uaalert.getText());
										 Assert.assertEquals(o.uaalert.getText(), "Area Master Modification Process Completed Successfully");
										 Thread.sleep(2000);
										 oo.clickpg(o.okbtn);
							
								 	}
								 else	 {
									 
									 Logger ll =Logger.getLogger("area u ");
								      	ll.trace(o.uaalert.getText());	 Thread.sleep(2000);
									 //Assert.assertEquals(o.uaalert.getText(), "AREAS HAS BEEN ASSIGNED, CANNOT BE MODIFIED ACTIVE 1");
									 oo.waits(o.bErrorbtn);	 
									 oo.clickpg(o.Errorbtn);	Thread.sleep(2000);	
								
								 }
									 
									 
								 
									 
								}
							
							
							
							
							
							
							
							
					}
							
							
							
							
							
							
							
							
						}else{
							System.out.println("no record present");
							//trying to fail bcs no data case screenshot 
							test = extent.createTest("no search results");
							test.createNode("input data based results not there");
							 Assert.assertTrue(false);
						}
					
			}
			
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
		 
		
				 
				
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);

	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		
	
		}
	 
	 
	 
	 
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{	System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
