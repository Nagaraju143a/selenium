package checklist;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import dwf.ds;
import pages.agilepg;
import plantpages.*;
import planttests.*;
import test1.base;
import dwf.ds;
public class groupmaster {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {System.out.println("Gm...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
		 this.driver=plantbase.driver;
		 System.out.println(driver+"driver2");
}
 





@DataProvider(name="addbmaster1")
public Object[][] adminuseradddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("gg","excel/plant3.xlsx");//chlgroupm

  return object;	 
}



@Test(dataProvider ="addbmaster1", priority=2)
public void sss(String n, String p, String gmname, String gmdsc, String Remark) throws InterruptedException, IOException{

System.out.println("ddddddddddddddd");

	 try {
		 
		 System.out.println("11111111111111111111111");
		 // import import dwf.ds;
				/* Properties u= new Properties();	u.load(ds.class.getResourceAsStream("object.properties"));
				    System.out.println(u.getProperty("url"));*/
		
				 System.out.println("block master  clickedssssssssssssssssssssssssss");	
				 this.driver=plantbase.driver;
					plantpg o = PageFactory.initElements(driver, plantpg.class);
					util oo = PageFactory.initElements(driver, util.class);
				 Thread.sleep(3000);
			
				 
				 driver.manage().timeouts().implicitlyWait(4000, TimeUnit.SECONDS);
			
				
				Thread.sleep(4000);
				oo.waits(o.blogout1);
				oo.clickpg(o.logout1);
			Thread.sleep(3000);//oo.clickpg(o.usrreg);
			oo.clickpg(o.logout2);
			Thread.sleep(3000);
			//driver.get(u.getProperty("url"));
			
			driver.get("http://10.10.10.71/PMTS_WOE/UI/Login/NewLogin");
			Thread.sleep(4000);
			if(n.contains(".")){
				oo.textipg(o.loginname, n);
			}else{
			oo.textpg(o.loginname, n);
			
			}
			
			
			
					
		Thread.sleep(5000);
		oo.textpg(o.loginpassword, p);
		
		Thread.sleep(4000);
		oo.clickpg(o.loginsub);
		
		Thread.sleep(3000);
		
		oo.clickpg(o.clm);
		Thread.sleep(3000);
		
		if(o.clgm.isDisplayed()){	 Thread.sleep(3000);
		
		System.out.println("clicked ");}
		else{
		 System.out.println("matster clicked then fmaster ");
		
			oo.clickpg(o.clm);
			Thread.sleep(5000);
			
			
		}
		
		oo.waits(o.bclgm);
		Thread.sleep(3000);
		
		oo.clickpg(o.clgm);	
						 
		Thread.sleep(3000);
		oo.waits(o.baddbm);
		 
		 oo.clickpg(o.addbm);
		 
		 /////////////
		 
		 
		 oo.waits(o.bgmname);	 Thread.sleep(3000);			
		 oo.textpg(o.gmname, gmname);
		
		 oo.textpg(o.gmdsc, gmdsc);
		 Thread.sleep(2000);
		 
		 
		 oo.clickpg(o.gmsubmit);
		 
		 Thread.sleep(3000);
			oo.waits(o.bRemark);
			
			Thread.sleep(2000);
			 oo.textpg(o.Remark, Remark);
			Thread.sleep(2000);
			 oo.textpg(o.pwd, p);
			 oo.clickpg(o.validateuser);	Thread.sleep(4000);
			 
	
			 Assert.assertEquals(o.ealert.getText(), "Electronic Signature Process Completed Successfully");	 
			 oo.waits(o.bERES);	 	Thread.sleep(2000);
			 oo.clickpg(o.ERES);	System.out.println("tested");
			 oo.waits(o.bokbtn);Thread.sleep(4000); 
			 Logger ll =Logger.getLogger("group master in checklist ");
			 ll.trace(o.ealert.getText());
		
			 oo.clickpg(o.okbtn);
			 Thread.sleep(4000);

 
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);

	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
		}
	 
	 
	 
	 
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{
		

		System.out.println("testing.2..");
		 test = extent.createTest("plant clicked");
			test.createNode("click on plant2");
			 Assert.assertTrue(true);

		
	}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
