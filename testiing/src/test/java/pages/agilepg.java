
package pages;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



import org.openqa.selenium.Keys;

public class agilepg {
	
	WebDriver driver;
/*
	@FindBy(xpath="//input[@value='Login']")  public WebElement aaaaa;
	public By aaaaa =By.xpath("//input[@value='Login']");
	
	extending one class into other class is caled inheritance 
		
	*/
	
	//			public By bsubrc =By.xpath("//*[contains(@type,'submit')]");
	//System.out.println(driver.findElement(By.xpath("//label[text()='AREA ID']")).getText()+"txt1");	 
	
	//login
	@FindBy(xpath="//*[@id='username']")  public WebElement loginname;
	public By bloginname =By.xpath("//*[@id='username']");
	
	@FindBy(xpath="//*[@id='password']")  public WebElement loginpassword;
	public By bloginpassword =By.xpath("//*[@id='password']");
	
	/*@FindBy(xpath="//*[@value='LogIn']")  public WebElement loginsub;
	public By bloginsub =By.xpath("//*[@value='LogIn']");*/
	
	
	@FindBy(xpath="/html/body/div/div/div/div[2]/form/div[6]/input")  public WebElement loginsub;
	public By bloginsub =By.xpath("/html/body/div/div/div/div[2]/form/div[6]/input");
	
	
	

	@FindBy(xpath="//*[@id='sidebar-menu']/div/ul/li[2]/a/i")  public WebElement masers;
	
	public By bmasers = By.xpath("//*[@id='sidebar-menu']/div/ul/li[2]/a/i");
	
	@FindBy(xpath="//*[@id='sidebar-menu']/div/ul/li[1]/a/i")  public WebElement administrator;
	
	public By badministrator = By.xpath("//*[@id='sidebar-menu']/div/ul/li[1]/a/i");
	
	
	//a[contains(@href,'RegistereduserList')]// ..// ..// ..//a/i
	@FindBy(xpath="//a[contains(@href,'RegistereduserList')]// ..// ..// ..//a/i")  public WebElement administratorf;
	public By badministratorf = By.xpath("//a[contains(@href,'RegistereduserList')]// ..// ..// ..//a/i");
	//user Reg 
	
	
	/*@FindBy(xpath="//a[@href='/PMTSBV/UI/AdminUI/RegistereduserList']")  public WebElement usrreg;
	public By busrreg =By.xpath("//a[@href='/PMTSBV/UI/AdminUI/RegistereduserList']");*/
	
	
	@FindBy(xpath="//a[contains(@href,'RegistereduserList')]")  public WebElement usrreg;
	public By busrreg =By.xpath("//a[contains(@href,'RegistereduserList')]");
	
	
	@FindBy(xpath="//*[@class='collapse-link']")  public WebElement adduser;
	public By badduser = By.xpath("//*[@class='collapse-link']");
	
	
	//usr reg df
	@FindBy(xpath="//*[@id='btnrev']")  public WebElement usrr;
	public By busrr = By.xpath("//*[@id='btnrev']");
	@FindBy(xpath="//*[@id='Reject']")  public WebElement usrrj;
	public By busrrj = By.xpath("//*[@id='Reject']");
	@FindBy(xpath="//*[@id='btnsubmit']")  public WebElement usra;
	public By busra = By.xpath("//*[@id='btnsubmit']");
	
	@FindBy(xpath="//*[@id='regform']/div[9]/button[1]")  public WebElement usrdrp;
	public By busrdrp = By.xpath("//*[@id='regform']/div[9]/button[1]");
	
	// user details added here 
	
	@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[3]")  public WebElement userstate;
	public By buserstate = By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[3]");
	
	
	

	@FindBy(xpath="//*[@id='plantname']")  public WebElement plantname;
	public By bplantname = By.xpath("//*[@id='plantname']");
	
	@FindBy(xpath="//*[@id='username']")  public WebElement username;
	public By busername = By.xpath("//*[@id='username']");
	
	@FindBy(xpath="//*[@id='Uname']")  public WebElement Uname;
	public String Uname1 ="//*[@id='Uname']";
	public By bUname = By.xpath("//*[@id='Uname']");
	
	@FindBy(xpath="//*[@id='email']")  public WebElement email;
	public By bemail = By.xpath("//*[@id='email']");
	
	
	@FindBy(xpath="//*[@id='role']")  public WebElement role;
	public By brole = By.xpath("//*[@id='role']");
	
	@FindBy(xpath="//*[@id='Department']")  public WebElement Department;
	public By bDepartment = By.xpath("//*[@id='Department']");
	
	
	@FindBy(xpath="//*[@id='password']")  public WebElement password;
	public By bpassword = By.xpath("//*[@id='password']");
	
	
	@FindBy(xpath="//*[@id='confirmpassword']")  public WebElement confirmpassword;
	public By bconfirmpassword = By.xpath("//*[@id='confirmpassword']");
	
	
	@FindBy(xpath="//*[@id='companyname']")  public WebElement companyname;
	public By bcompanyname = By.xpath("//*[@id='companyname']");
	
	
	@FindBy(xpath="//*[@id='reggistraion']")  public WebElement reggistraion;
	public By breggistraion = By.xpath("//*[@id='reggistraion']");
	
	// electronic sign
	@FindBy(xpath="//*[@name='Remark']")  public WebElement Remark;
	public By bRemark = By.xpath("//*[@name='Remark']");	
	@FindBy(xpath="//*[@name='pwd']")  public WebElement pwd;
	public By bpwd = By.xpath("//*[@name='pwd']");	
	@FindBy(xpath="//*[@name='validateuser']")  public WebElement validateuser;
	public By bvalidateuser = By.xpath("//*[@name='validateuser']");

	@FindBy(xpath="//*[@id='ERES']")  public WebElement ERES;
	public By bERES = By.xpath("//*[@id='ERES']");
	@FindBy(xpath="//*[@id='okbtn']")  public WebElement okbtn;
	public By bokbtn = By.xpath("//*[@id='okbtn']");
	
	@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div[2]/form/div/div[7]/div/input")  public WebElement weasgblk;
	
	public	String asgblk= "/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div[2]/form/div/div[7]/div/input";
	public	String asgblkdiv="/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div[2]/form/div/div[7]/div/";
	
	
	@FindBy(xpath="//*[@id='myModal1']/div/div/div[1]/div/div[1]/h4")  public WebElement uaalert;
	public By buaalert = By.xpath("//*[@id='myModal1']/div/div/div[1]/div/div[1]/h4");

	@FindBy(xpath="//*[@id='Errorbtn']")  public WebElement Errorbtn;
	public By bErrorbtn = By.xpath("//*[@id='Errorbtn']");
	
	

	//////////////////////  nuser
	
	@FindBy(xpath="//*[@class='alert']")  public WebElement useralert;
	public By buseralert = By.xpath("//*[@class='alert']");
	
	
	
	
	@FindBy(xpath="//*[@id='Unamenull']")  public WebElement usrvalidate;
	public By busrvalidate = By.xpath("//*[@id='Unamenull']");
	@FindBy(xpath="//*[@id='cap']")  public WebElement psdvalidate;
	public By bpsdvalidate = By.xpath("//*[@id='cap']");
	
	@FindBy(xpath="//*[@id='compcimpwd']")  public WebElement cpswdvalidate;
	public By bcpswdvalidate = By.xpath("//*[@id='compcimpwd']");
	
	
	//update user
	@FindBy(xpath="//*[@id='regform']/div[9]/button[2]")  public WebElement deactiveuser;
	public By bdeactiveuser = By.xpath("//*[@id='regform']/div[9]/button[2]");
	
	
	
	
	
//////////////user search
	
	@FindBy(xpath="//a[contains(@href,'#menu11')]")  public WebElement drs;
	public By bdrs =By.xpath("//a[contains(@href,'#menu11')]");

	
@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div/div/div[2]/div/ul/li[2]/a")  public WebElement usad;
public By busad =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div/div/div[2]/div/ul/li[2]/a");

@FindBy(xpath="//*[@id='datatable-responsive_filter']/label/input")  public WebElement ussearch;
public By bussearch =By.xpath("//*[@id='datatable-responsive_filter']/label/input");

//*[@id='datatable-responsive_filter']/label/input
@FindBy(xpath="//*[@id='datatable-responsives_filter']/label/input")  public WebElement depsearch;
public By bdepsearch =By.xpath("//*[@id='datatable-responsives_filter']/label/input");




@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div/div/div[2]/div/div/div[2]/div/div[2]/div/table/tbody/tr")  public WebElement usersearch1;
public By busersearch1 =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div/div/div[2]/div/div/div[2]/div/div[2]/div/table/tbody/tr");
	
public String chdepup1="/html/body/div[1]/div/div[3]/div/div[3]/div/div/div/div/div[2]/div/div/div[1]/div/div[2]/div/table/tbody/tr";								

public	String busersearch1s1="/html/body/div[1]/div/div[3]/div/div[3]/div/div/div/div/div[2]/div/div/div[2]/div/div[2]/div/table/tbody/tr";

public	String busersearch1s1div="/html/body/div[1]/div/div[3]/div/div[3]/div/div/div/div/div[2]/div/div/div[2]/div/div[2]/div/table/tbody/tr/td";
public	String bupdatechdepdev=	"/html/body/div[1]/div/div[3]/div/div[3]/div/div/div/div/div[2]/div/div/div[1]/div/div[2]/div/table/tbody/tr/td";							 
public	String depdev="/html/body/div[1]/div/div[3]/div/div[3]/div/div/div/div/div[2]/div/div/div[2]/div/div[2]/div/table/tbody/tr";

							  //html/body/div[1]/div/div[3]/div/div[3]/div/div/div/div/div[2]/div/div/div[2]/div/div[2]/div/table/tbody/tr/td[2]/text
public	String busersearch1s="/html/body/div[1]/div/div[3]/div/div[3]/div/div/div/div[2]/div/div/div[2]/div/div[2]/div/table/tbody/tr";
								//html/body/div[1]/div/div[3]/div/div[3]/div/div/div/div/div[2]/div/div/div[2]/div/div[2]/div/table/tbody/tr/td[2]/text
public	String busersearch1sdiv="/html/body/div[1]/div/div[3]/div/div[3]/div/div/div/div[2]/div/div/div[2]/div/div[2]/div/table/tbody/tr/td";



@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[6]/i")  public WebElement updatechdep;
				//*[@id='datatable-responsives']/tbody/tr/td[4]/i
@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[4]/i")  public WebElement depupdatebtn;
@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[1]")  public WebElement ussearchdiv1;
@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[2]")  public WebElement ussearchdiv2;
@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr[1]/td[3]")  public WebElement st;

//*[@id="datatable-responsives"]/tbody/tr[1]/td[5]
@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[4]/i")  public WebElement depupdatebtn1;
public By bdepupdatebtn1 = By.xpath("//*[@id='datatable-responsives']/tbody/tr/td[4]/i");


@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[1]")  public WebElement ussearchdiv1s;
@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[2]")  public WebElement ussearchdiv2s;


public	String ussearchrow="//*[@id='datatable-responsive']/tbody/";

public	String ussearchrows="//*[@id='datatable-responsives']/tbody/";

//plant 


@FindBy(xpath="//*[@id='PLANTID']")  public WebElement PLANTID;
public By bPLANTID = By.xpath("//*[@id='PLANTID']");


@FindBy(xpath="//*[@id='PLANTNAME']")  public WebElement PLANTNAME;
public By bPLANTNAME = By.xpath("//*[@id='PLANTNAME']");


@FindBy(xpath="//*[@id='PLANTLOCATION']")  public WebElement PLANTLOCATION;
public By bPLANTLOCATION = By.xpath("//*[@id='PLANTLOCATION']");
@FindBy(xpath="//*[@id='validateuser']")  public WebElement validateuserplant;
public By bvalidateuserplant = By.xpath("//*[@id='validateuser']");
	
	// create user role 
	
		/*@FindBy(xpath="//a[@href='/PMTSBV/UI/AdminUI/CreateUserRoleList']")  public WebElement usrrole;
		public By busrrole =By.xpath("//a[@href='/PMTSBV/UI/AdminUI/CreateUserRoleList']");*/
		
		@FindBy(xpath="//a[contains(@href,'CreateUserRoleList')]")  public WebElement usrrole;
		public By busrrole =By.xpath("//a[contains(@href,'CreateUserRoleList')]");
	
		@FindBy(xpath="//a[contains(@href,'MastersUI')]")  public WebElement plant1;
		public By bplant1 =By.xpath("//a[contains(@href,'MastersUI')]");
		

		@FindBy(xpath="//*[@class='collapse-link']")  public WebElement adduserrole;
		public By badduserrole = By.xpath("//*[@class='collapse-link']");
		
	
		@FindBy(xpath="//*[@id='Level']")  public WebElement Level;
		public By bLevel = By.xpath("//*[@id='Level']");
	
		@FindBy(xpath="//*[@id='typeofroles']")  public WebElement typeofroles;
		public By btypeofroles = By.xpath("//*[@id='typeofroles']");
	
		@FindBy(xpath="//*[@id='RoleDesc']")  public WebElement RoleDesc;
		public By bRoleDesc = By.xpath("//*[@id='RoleDesc']");
	
		@FindBy(xpath="//*[@id='sub']")  public WebElement sub;
		public By bsub = By.xpath("//*[@id='sub']");
	
	
	
		
		
		@FindBy(xpath="//*[@id='bls']")  public WebElement abcheck;
		public By babcheck = By.xpath("//*[@id='bls']");
		
		

		
		public	String bcheck="/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div[2]/form/div/div[7]/div/";
		
		@FindBy(xpath="//*[@id='Deactive']")  public WebElement userroleda;
		public String userroleda1="//*[@id='Deactive']";
		public By buserroleda = By.xpath("//*[@id='Deactive']");
		public String buserroleda1 ="//*[@id='Deactive']";
		////html/body/div[1]/div/div[3]/div/div[3]/div/div/div/div[2]/div/div/div[2]/div/div[2]/div/table/tbody/tr[1]/td[4]/i
		
	
	//logout
	
	
	
	@FindBy(xpath="//*[@class='user-profile dropdown-toggle']")  public WebElement logout1;
	public By blogout1 = By.xpath("//*[@class='user-profile dropdown-toggle']");
	
	/*@FindBy(xpath="//a[@href='/PMTSBV/UI/Login/Logout']") public WebElement logout2;*/
	@FindBy(xpath="//a[contains(@href,'Login/Logout')]")  public WebElement logout2;
	
	
	// Deportment Creation 
	
	//*[@id="regform"]/div[5]/button[1]//*[@id="regform"]/div[5]/button[1]
	
	@FindBy(xpath="//*[@id='regform']/div[5]/button[1]")  public WebElement chngedepdrop;
	
	public String chngedepdrop1 ="//*[@id='regform']/div[5]/button[1]";
	public By bchngedepdrop = By.xpath("//*[@id='regform']/div[5]/button[1]");
	
	@FindBy(xpath="//a[contains(@href,'CreateDepartmentList')]")  public WebElement clickdep;
	public By bclickdep =By.xpath("//a[contains(@href,'CreateDepartmentList')]");
	
/*	@FindBy(xpath="//a[@href='/PMTSBV/UI/AdminUI/CreateDepartmentList']")  public WebElement clickdep;
	public By bclickdep =By.xpath("//a[@href='/PMTSBV/UI/AdminUI/CreateDepartmentList']");*/
	
	
	
	
	@FindBy(xpath="//*[@class='collapse-link']")  public WebElement adddep;
	public By badddep = By.xpath("//*[@class='collapse-link']");
	//Department
	
	@FindBy(xpath="//*[@id='DeptDesc']")  public WebElement DeptDesc;
	public By bDeptDesc = By.xpath("//*[@id='DeptDesc']");
	
	
	@FindBy(xpath="//*[@id='sub1']")  public WebElement sub1;
	public By bsub1 = By.xpath("//*[@id='sub1']");
	

	
	//audit trails	
	/*@FindBy(xpath="//a[@href='/PMTSBV/UI/AdminUI/Audittrails']")  public WebElement audit1;
	public By baudit1 =By.xpath("//a[@href='/PMTSBV/UI/AdminUI/Audittrails']");*/
	
	
	@FindBy(xpath="//a[contains(@href,'Audittrails')]")  public WebElement audit1;
	public By baudit1 =By.xpath("//a[contains(@href,'Audittrails')]");
	
	
	
	@FindBy(xpath="//*[@id='plants']")  public WebElement plants;
	public By bplants = By.xpath("//*[@id='plants']");
	
	@FindBy(xpath="//*[@id='role']")  public WebElement atrole;
	public By batrole = By.xpath("//*[@id='plants']");
	@FindBy(xpath="//*[@id='selectdate1']")  public WebElement selectdate1;
	public By bselectdate1 = By.xpath("//*[@id='selectdate1']");
	
	
	@FindBy(xpath="//*[@id='selectdate2']")  public WebElement selectdate2;
	public By bselectdate2 = By.xpath("//*[@id='selectdate2']");	
				//title Prev
				//title Next	
	@FindBy(xpath = "//table[@class='ui-datepicker-calendar']//td")	public WebElement calstartt;
	public By bcalstartt = By.xpath("//table[@class='ui-datepicker-calendar']//td");
	
	//sub element  is alredy there
				
					
	@FindBy(xpath = "/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div/div[3]/button")	public WebElement closebtn;
	public By bclosebtn = By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div/div[3]/button");
			
	@FindBy(xpath="//*[@id='datatable-responsive']")  public WebElement divaudit;
	public By bdivaudit = By.xpath("//*[@id='datatable-responsive']");	
	
	@FindBy(xpath="//input[@class='form-control input-sm']")  public WebElement auditsearch;
	@FindBy(xpath="//*[@id='tblbody']")  public WebElement auditsearchbody;
	
	//change dep flow 
	//*[@id="datatable-responsive"]/tbody/tr/td[6]/i
	
	@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[6]/i")  public WebElement chdepu;
	public By bchdepu =By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[6]/i");
	
	@FindBy(xpath="//*[@id='regform']/div[5]/button[1]")  public WebElement chdepa;
	public By bchdepa =By.xpath("//*[@id='regform']/div[5]/button[1]");
	
	@FindBy(xpath="//*[@id='regform']/div[5]/button[2]")  public WebElement chdepr;
	public By bchdepr =By.xpath("//*[@id='regform']/div[5]/button[2]");
	
	
	
	//change dep 
		
	@FindBy(xpath="//a[contains(@href,'ChangeDepartmentList')]")  public WebElement chdep;
	public By bchdep =By.xpath("//a[contains(@href,'ChangeDepartmentList')]");
			
	//adduser here(change dep) use for add buttom 
	@FindBy(xpath="//*[@id='Userid']")  public WebElement chdepUserid;
	public By bchdepUserid = By.xpath("//*[@id='Userid']");	
	
	@FindBy(xpath="//*[@id='Tobechangedept']")  public WebElement Tobechangedept;
	public By bTobechangedept = By.xpath("//*[@id='Tobechangedept']");	
		
	@FindBy(xpath="//*[@id='regform']/div/div[5]/button[1]")  public WebElement chdepsubmit;
	public By bchdepsubmit = By.xpath("*[@id='regform']/div/div[5]/button[1]");	
	
	
	
	// in user add  electronic sign  Remark  pwd  validateuser      ERES   okbtn
	
	
	
	// policy settings
	
	
	@FindBy(xpath="//a[contains(@href,'PasswordAttact')]")  public WebElement policy;
	public By bpolicy =By.xpath("//a[contains(@href,'PasswordAttact')]");
	
	
	
	@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[3]/div/div[2]/table/tbody/tr/td[11]/i")  public WebElement policyud;
	public By bpolicyud =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[3]/div/div[2]/table/tbody/tr/td[11]/i");
	
	
	// useridmax  usernamemin usernamemax pwdage  attacktolck Pwdlength  resetpwd   passwordcomplexity   session
	//submit
	
	@FindBy(xpath="//*[@id='useridmin']")  public WebElement useridmin;
	public By buseridmin = By.xpath("//*[@id='useridmin']");
	
	@FindBy(xpath="//*[@id='useridmax']")  public WebElement useridmax;
	public By buseridmax = By.xpath("//*[@id='useridmax']");
	
	@FindBy(xpath="//*[@id='usernamemin']")  public WebElement usernamemin;
	public By busernamemin = By.xpath("//*[@id='usernamemin']");
	@FindBy(xpath="//*[@id='usernamemax']")  public WebElement usernamemax;
	public By busernamemax = By.xpath("//*[@id='usernamemax']");
	@FindBy(xpath="//*[@id='pwdage']")  public WebElement pwdage;
	public By bpwdage = By.xpath("//*[@id='pwdage']");
	@FindBy(xpath="//*[@id='attacktolck']")  public WebElement attacktolck;
	public By battacktolck = By.xpath("//*[@id='attacktolck']");
	@FindBy(xpath="//*[@id='Pwdlength']")  public WebElement Pwdlength;
	public By bPwdlength = By.xpath("//*[@id='Pwdlength']");
	@FindBy(xpath="//*[@id='resetpwd']")  public WebElement resetpwd;
	public By bresetpwd = By.xpath("//*[@id='resetpwd']");
	
	@FindBy(xpath="//*[@id='passwordcomplexity']")  public WebElement passwordcomplexity;
	public By bpasswordcomplexity = By.xpath("//*[@id='passwordcomplexity']");
	@FindBy(xpath="//*[@id='session']")  public WebElement session;
	public By bsession = By.xpath("//*[@id='session']");
	@FindBy(xpath="//*[@id='submit']")  public WebElement submit;
	public By bsubmit = By.xpath("//*[@id='submit']");
	
	// here we are using user in used electronic signatur
	
	
	@FindBy(xpath="//*[@id='myModal']/div/div/div[1]/div/div[1]/h4")  public WebElement paalert;
	public By bpaalert = By.xpath("//*[@id='myModal']/div/div/div[1]/div/div[1]/h4");
	
	
	// Role Based Privileges
	

	
	
	@FindBy(xpath="//a[contains(@href,'AssignrolesList')]")  public WebElement rolepv;
	public By brolepv =By.xpath("//a[contains(@href,'AssignrolesList')]");
	
	
	
	@FindBy(xpath="//*[@id='datatable-responsive_filter']/label/input")  public WebElement rolepvsearch;
	public By brolepvsearch =By.xpath("//*[@id='datatable-responsive_filter']/label/input");
	
	
	@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[2]")  public WebElement rolepvele;
	public By brolepvele =By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[2]");
	

	@FindBy(xpath="//*[@value='Masters']")  public WebElement masters;
	public By bmasters =By.xpath("//*[@value='Masters']");
	


	public	String backlog1 ="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[2]/ul/li[3]/ul/";
	public	String backlog2="/div/div[1]/input";

	public	String backlog2l="/div/div[2]/div[1]/input";
	public	String backlog2in="/div/div[2]/div[2]/input";
	public	String backlog2e="/div/div[2]/div[3]/input";
	//PlantMaster  value
	
	
	//ckecksub_role_PlantMaster_Insert  id
	@FindBy(xpath="//*[@id='ckecksub_role_PlantMaster_Insert']")  public WebElement rbpmasterinsert;
	public By brbpmasterinsert = By.xpath("//*[@id='ckecksub_role_PlantMaster_Insert']");
	
	/*
	/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[2]/ul/li[3]/ul/li[1]/div/div[2]/div[1]/input
	/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[2]/ul/li[3]/ul/li[1]/div/div[2]/div[2]/input
	/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[2]/ul/li[3]/ul/li[1]/div/div[2]/div[3]/input*/
	
	
	@FindBy(xpath="//*[@id='btnsubmit']")  public WebElement updatedeps;
	public By bupdatedeps = By.xpath("//*[@id='btnsubmit']");
	
	
	
	@FindBy(xpath="//*[@id='Department']")  public WebElement updatedepname;
	public By bupdatedepname = By.xpath("//*[@id='Department']");
	@FindBy(xpath="//*[@id='DeptDesc']")  public WebElement updatedepdsc;
	public By bupdatedepdsc = By.xpath("//*[@id='DeptDesc']");
	
	@FindBy(xpath="//*[@id='btnSubmit']")  public WebElement rbpsubmit;
	public By brbpsubmit = By.xpath("//*[@id='btnSubmit']");
	

	public	String badmin1 ="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[2]/ul/li[1]/ul/";
	public	String chklistmaster1 ="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[2]/ul/li[2]/ul/";	
	public	String bprodop1 ="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[2]/ul/li[4]/ul/";	
	public	String breports1 ="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[2]/ul/li[5]/ul/";	
	public	String busrreq1 ="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[2]/ul/li[6]/ul/";	
	
	
	
	
	
	// Define WorkFlow
	
	
	@FindBy(xpath="//a[contains(@href,'Dynamicworkflow')]")  public WebElement definewf;
	public By bdefinewf =By.xpath("//a[contains(@href,'Dynamicworkflow')]");
	
	@FindBy(xpath="//*[@value='PasswordRequestandUserAccountUnlock']")  public WebElement passrequsraccunl;
	public By bpassrequsraccunl =By.xpath("//*[@value='PasswordRequestandUserAccountUnlock']");
	
	
	

	@FindBy(xpath="//*[@id='tree']/li[1]/ul/li[1]/div/div[2]/div[1]/div[1]/button")  public WebElement pruinit;
	public By bpruinit =By.xpath("//*[@id='tree']/li[1]/ul/li[1]/div/div[2]/div[1]/div[1]/button");
	
	
	@FindBy(xpath="//*[@value='HO Administrator']")  public WebElement hoadmin;
	public By bhoadmin =By.xpath("//*[@value='HO Administrator']");
	
	
	
	
	@FindBy(xpath="//*[@id='regform']/div[3]/div/button[1]")  public WebElement dwfsubmit;
	public By bdwfsubmit =By.xpath("//*[@id='regform']/div[3]/div/button[1]");
	
	
	
	
	
	
	
	
	
	
	
	
	
	//agile
	
	
		

	

	public agilepg(WebDriver driver){ 
        this.driver=driver; 
        
        
        // driver from tc assigned to  webdriver  driver by using this.driver here assigned to our WebDriver driver;
}

	
	public void waits(By  userName2){
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.visibilityOfElementLocated((By) userName2));
		System.out.println(" wait for ............");
		
	}
	
	

	public void clearpg(WebElement  name2)
	{
		
		
		name2.clear();
	}
	
	
	
	
	
	
	
	
	
	
	

	}
