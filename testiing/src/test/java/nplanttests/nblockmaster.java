package nplanttests;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import pages.agilepg;
import plantpages.*;
import test1.base;
import planttests.*;
public class nblockmaster {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
}
 
  
	






///////////////////////////////////////////////////////////////////////






@DataProvider(name="addbmaster")
public Object[][] adminuseradddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("blockmaster1","excel/nplant.xlsx");
	
  return object;	 
}






//public  void adminuseraddf(String name, String n,String alname,String fname, String lastn,String gen,String email,String pwd, String cpwd,String mnum, String rnum,String roles, String add1, String add2,String coun,String state,String city, String zip) throws InterruptedException {
	@Test(dataProvider ="addbmaster", priority=2)
public void adminuseraddf(String BLOCKID,String BLOCKNAME ,String BLOCKDESCRIPTION ,String Remark,String ppwd) throws InterruptedException, IOException{



	 try {
		 
		 System.out.println("role  clicked");	
		 this.driver=plantbase.driver;
			plantpg o = PageFactory.initElements(driver, plantpg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
		 
		 
		 
		 if(o.blockmaster.isDisplayed()){	 Thread.sleep(3000);
			
			System.out.println("clicked ");}
		 else{
			 System.out.println("matster clicked then fmaster ");
				oo.clickpg(o.master);Thread.sleep(5000);
				
			}
			oo.clickpg(o.blockmaster);
			
			Thread.sleep(3000);
			oo.waits(o.baddbm);
			 
			 oo.clickpg(o.addbm);
		 
		 
		 
		 //create
			 
			 
			 
			 Thread.sleep(3000);
				oo.waits(o.bBLOCKID);
				
				 if(BLOCKID==null || BLOCKID.equals(""))
				 {}else{
			 oo.textpg(o.BLOCKID, BLOCKID);}
			 Thread.sleep(2000);
				 
			 if(BLOCKNAME==null || BLOCKNAME.equals(""))
			 {}else{
			 oo.textpg(o.BLOCKNAME, BLOCKNAME);
			 Thread.sleep(2000);
			 }
		 
			 
			 if(BLOCKDESCRIPTION==null || BLOCKDESCRIPTION.equals(""))
			 {}else{
			 oo.textpg(o.BLOCKDESCRIPTION, BLOCKDESCRIPTION);
			 Thread.sleep(2000);}
			 
			 oo.clickpg(o.sub1);
			 Thread.sleep(4000);
		boolean b=	driver.findElements(o.bRemark).size()>0; System.out.println("nnnnnnnnnnnnnnn"+b);
		/*boolean bb=	driver.findElements(o.bvalBlocknull).size()>0;
		boolean bbb=	driver.findElements(o.bUnamenull).size()>0;
		 System.out.println("nnnnnnnnnnnnnnn"+bb);
	    System.out.println("iiiiiiiiiiiiiiiiiiiii"+bbb);*/
			
			
				
				 
				 if(b){
		
			 
			 Thread.sleep(3000);
				oo.waits(o.bRemark);
				
				Thread.sleep(2000);
				 oo.textpg(o.Remark, Remark);
				Thread.sleep(2000);
				 oo.textpg(o.pwd, ppwd);
				 oo.clickpg(o.validateuser);	Thread.sleep(4000);
				 
				 
					Thread.sleep(3000);  oo.waits(o.buaalert);	
					 System.out.println(o.uaalert.getText());
					 Assert.assertEquals(o.uaalert.getText(), "BLOCK ID ALREADY EXISTS");Thread.sleep(3000);
					 Logger ll =Logger.getLogger("blockmaster adding negativecase alredy existing");
						ll.trace(o.uaalert.getText());Thread.sleep(2000);
					 oo.clickpg(o.Errorbtn);		
					 Thread.sleep(2000);
					 
				 
			 }
				 //
			 else if(o.valBlocknull.isDisplayed()){	 Thread.sleep(2000);
				
			 System.out.println(o.valBlocknull.getText()+"Block ID Min 3 char's And Max 20 char's");
			 Assert.assertEquals(o.valBlocknull.getText(), "Block ID Min 3 char's And Max 20 char's");
			 Thread.sleep(2000);
			 Logger ll =Logger.getLogger("blockmaster adding negativecase blockid");
				ll.trace(o.valBlocknull.getText());
				
				
			 } else if(o.Unamenull.isDisplayed()){	 Thread.sleep(2000);
				
			 System.out.println(o.Unamenull.getText()+"Block Name Should be Min 4 char's And Max 30 char's");
			 Assert.assertEquals(o.Unamenull.getText(), "Block Name Should be Min 4 char's And Max 30 char's");
			 Thread.sleep(2000);
			 Logger ll =Logger.getLogger("blockmaster adding negativecase blockname");
				ll.trace(o.Unamenull.getText());
				
				
			 } else if(o.valdescriptionnull.isDisplayed()){	 Thread.sleep(2000);
				
			 System.out.println(o.valdescriptionnull.getText()+"Block Description Should be Min 3 char's And Max 100 char's");
			 Assert.assertEquals(o.valdescriptionnull.getText(), "Block Description Should be Min 3 char's And Max 100 char's");
			 Thread.sleep(2000);
			 Logger ll =Logger.getLogger("blockmaster adding negativecase block dsc");
				ll.trace(o.valdescriptionnull.getText());
				
				
			 }
			 else{
				 Thread.sleep(2000);
				 System.out.println("please put something here");//*[@id="regform"]/div[1]/div[2]/text()
				 Assert.assertEquals(o.useralert.getText(), "please put something here");
				 
			 }
				  
				 
				  
			 
			 
		
			 	 
				
			 
	/*
			 Block Name Should be Min 4 char's And Max 30 char's 
			 
			 
			
			 Block Description Should be Min 3 char's And Max 100 char's */
			 
			 
			 
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("catch blockmaster negativecase  ");
		ll.trace(e.getMessage());
		 Thread.sleep(4000);
			util oo = PageFactory.initElements(driver, util.class);
		oo.srnsht(driver);

	 }
	 	test = extent.createTest("blockmaster");
		test.createNode("blockmaster negative");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
	driver.navigate().refresh();Thread.sleep(2000);}
	 
	 
	 
	 
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
