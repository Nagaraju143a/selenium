package nplanttests;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import pages.agilepg;
import plantpages.*;
import test1.base;
import planttests.*;
public class nareamaster {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
}
 
  
	






///////////////////////////////////////////////////////////////////////






@DataProvider(name="addbmaster")
public Object[][] adminuseradddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("areamaster1","excel/nplant.xlsx");
	
  return object;	 
}






//public  void adminuseraddf(String name, String n,String alname,String fname, String lastn,String gen,String email,String pwd, String cpwd,String mnum, String rnum,String roles, String add1, String add2,String coun,String state,String city, String zip) throws InterruptedException {
	@Test(dataProvider ="addbmaster", priority=2)
public void adminuseraddf(String blockid,String LOCATIONID ,String ERPSTATUS,String v1,String ERPAREAID,String AREANAME,String v2,String yn1 ,String yn2 ,String yn3 ,String yn4, String Remark,String ppwd) throws InterruptedException, IOException{



	 try {
		 
		 System.out.println("area master  clicked");	
		 this.driver=plantbase.driver;
			plantpg o = PageFactory.initElements(driver, plantpg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
		 
		 if(o.amaster1.isDisplayed()){	 Thread.sleep(3000);
			
			System.out.println("clicked ");}
		 else{
			 System.out.println("matster clicked then fmaster ");
				oo.clickpg(o.master);Thread.sleep(5000);
				
			}
		 
		 
			oo.clickpg(o.amaster1);
			
			Thread.sleep(3000);
			oo.waits(o.baddbm);
			 
			 oo.clickpg(o.addbm);
		 		 
		 //create
			 			 
			 Thread.sleep(3000);
				oo.waits(o.bblockid);			
				
				 if(blockid==null || blockid.equals(""))
				 {}else{
				
				Thread.sleep(3000);
				oo.dropdownpg(o.blockid, blockid);}
				
				 
				 if(LOCATIONID==null || LOCATIONID.equals(""))
				 {}else{
				Thread.sleep(3000);
				oo.dropdownpg(o.LOCATIONID, LOCATIONID);}
				 
				 if(ERPSTATUS==null || ERPSTATUS.equals(""))
				 {}else{
				Thread.sleep(3000);
				oo.dropdownpg(o.ERPSTATUS, ERPSTATUS);}
				
				
				
				
				
				if(ERPSTATUS.contains("without")){
			
				}else{
					Thread.sleep(3000);
					oo.clickpg(o.button);
					
					Thread.sleep(3000);
					
					///				
					
					List<WebElement> ls= driver.findElements(By.xpath("//*[@id='ingredientcode1']"));					 
					 for(WebElement l : ls){
						 System.out.println(l.getText());
						 System.out.println(l.getAttribute("value")+"hiiiiiiiiii");
						 
						  if(v1==null || v1.equals(""))
						 {}else{
							   if(l.getAttribute("value").equals(v1)){
										 l.click();
							   		}
					   }
				
					 	}
						Thread.sleep(3000);
					 oo.clickpg(o.button);
				}
				
				
				 if(ERPAREAID==null || ERPAREAID.equals(""))
				 {}else{
				 Thread.sleep(2000);
				 oo.textpg(o.ERPAREAID, ERPAREAID);		}	
				 
				 
				 if(AREANAME==null || AREANAME.equals(""))
				 {}else{
				 Thread.sleep(2000);
				 oo.textpg(o.AREANAME, AREANAME);
				 }
		
				 if(v2==null || v2.equals(""))
				 {}else{
				 if("Processing Area".contains(v2)){
					 oo.clickpg(o.ProcessingArea);
					 Thread.sleep(2000);
					 }else if("HoldArea".contains(v2)){
						 oo.clickpg(o.HoldArea);
						 Thread.sleep(2000);
						 } if("WashArea".contains(v2)){
							 oo.clickpg(o.WashArea);
							 Thread.sleep(2000);
							 } if("wipholdarea".contains(v2)){
								 oo.clickpg(o.wipholdarea);
								 Thread.sleep(2000);
								 }
					 
				 }
					 
			 
					 Thread.sleep(4000);
					 
						oo.jsdownbye(o.btnsubmit);
		 
						 Thread.sleep(2000);
		 
			 
			 //drop down yes/no
			 System.out.println( o.IS_IT_HAS_BALANCE.isSelected()+"test.............");
			
		
				 Thread.sleep(3000);System.out.println("enter................");
				 if(yn1==null || yn1.equals(""))
				 {}else{
					oo.dropdownpg(o.IS_IT_HAS_BALANCE, yn1);}
	
				 if(yn2==null || yn2.equals(""))
				 {}else{
				 Thread.sleep(3000);
					oo.dropdownpg(o.IS_IT_HAS_TYPEA, yn2);}
		
				 if(yn3==null || yn3.equals(""))
				 {}else{
				 Thread.sleep(3000);
					oo.dropdownpg(o.IS_IT_HAS_LOTS, yn3);}
	
				 if(yn4==null || yn4.equals(""))
				 {}else{
				 Thread.sleep(3000);
					oo.dropdownpg(o.CONT_WISE_AREA_START, yn4);
				 }
				
				
				 
				 
				 oo.clickpg(o.btnsubmit);
				 boolean b=	driver.findElements(o.bRemark).size()>0; 	
				 Thread.sleep(3000);
			 

	 
	 if(b){
			
			oo.waits(o.bRemark);
							
			Thread.sleep(2000);
			 oo.textpg(o.Remark, Remark);
			Thread.sleep(2000);
			 oo.textpg(o.pwd, ppwd);
			 oo.clickpg(o.validateuser);	Thread.sleep(4000);
			 
				Thread.sleep(3000);  oo.waits(o.buaalert);	
				 System.out.println(o.uaalert.getText());
				 Assert.assertEquals(o.uaalert.getText(), "AREAMASTER ID ALREADY EXISTS");Thread.sleep(3000);
				 Logger ll =Logger.getLogger("AREAMASTER already exist Negative case");
					ll.trace(o.uaalert.getText());Thread.sleep(2000);
				 oo.clickpg(o.Errorbtn);		
				 Thread.sleep(2000);
		 }
	 
	 
			 
	 else if(o.Unamenull.isDisplayed()){	 Thread.sleep(2000);
		
	 System.out.println(o.Unamenull.getText()+" Area ID Should be Min 2 char's And Max 30 char's ");
	 Assert.assertEquals(o.Unamenull.getText(), "Area ID Should be Min 2 char's And Max 30 char's");
	 Thread.sleep(2000);
	 Logger ll =Logger.getLogger("area Id Should");
		ll.trace(o.Unamenull.getText());
		
		
	 }
	 else{
		 Thread.sleep(2000);
		 System.out.println("please put something here");
		 Assert.assertEquals(o.useralert.getText(), "please put something here");
		 
		 Logger ll =Logger.getLogger("Location adding negativecase ");
			ll.trace(o.useralert.getText());
		 
	 }
				 
				
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("catch areamasternegativecase  ");
		ll.trace(e.getMessage());
		 Thread.sleep(4000);
			util oo = PageFactory.initElements(driver, util.class);
		oo.srnsht(driver);

	 
	 }
	 	test = extent.createTest("area");
		test.createNode("areamaster negative");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);}
	 
	 
	 
	 
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
