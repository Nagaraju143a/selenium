package nplanttests;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import pages.agilepg;
import plantpages.*;
import test1.base;
import planttests.*;
public class nuserreq1 {

private static final TimeUnit TimeUnit = null;
public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {
		System.out.println("testing...");
		 test = extent.createTest("plant clicked");
			test.createNode("click on plant2");
			 Assert.assertTrue(true);
			 /*

		this.driver = plantbase.driver;
		Thread.sleep(900);
		System.out.println("testing on master module ");
		plantpg o = PageFactory.initElements(driver, plantpg.class);
		util oo = PageFactory.initElements(driver, util.class);
		
		try {	
			//oo.flu(o.badmin); 
			Thread.sleep(4000);
		//	oo.clickpg(o.master);
		Thread.sleep(3000);//oo.clickpg(o.usrreg);
		
		}catch (Exception e) {
			// TODO: handle exception
		System.out.println(e.getMessage());
			
		}
		 test = extent.createTest("master");
		test.createNode("click on master");
		 Assert.assertTrue(true);
	
	*/}
 
  
	






///////////////////////////////////////////////////////////////////////






@DataProvider(name="nusr1")
public Object[][] usr1adddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("nusr1","excel/nuser.xlsx");
	
  return object;	 
}






//public  void adminuseraddf(String name, String n,String alname,String fname, String lastn,String gen,String email,String pwd, String cpwd,String mnum, String rnum,String roles, String add1, String add2,String coun,String state,String city, String zip) throws InterruptedException {
	@Test(dataProvider ="nusr1", priority=2)
public void usr1addf(String usrarea,String Eqid ,String Block, String Location ,String Remark,String ppwd) throws InterruptedException, IOException{



	 try {
		 
		 System.out.println("role  clicked");	
		 this.driver=plantbase.driver;
			plantpg o = PageFactory.initElements(driver, plantpg.class);
			util oo = PageFactory.initElements(driver, util.class);
			
	 Thread.sleep(4000);
			 if(o.usrreq1.isDisplayed()){	 Thread.sleep(3000);
			
				System.out.println("clicked ");}
			 else{
				 System.out.println("usrreq clicked then usrreq1 ");
					oo.clickpg(o.usrreq);Thread.sleep(5000);
					
				}
			
			
			 oo.waits(o.busrreq1);
		 Thread.sleep(4000);
		 
	
			oo.clickpg(o.usrreq1);
			
			Thread.sleep(3000);
			oo.waits(o.baddbm);
			 
			 oo.clickpg(o.addbm);
		 
		 
				Thread.sleep(3000);
		 //create
			
				
				
				oo.waits(o.busrarea);	 Thread.sleep(3000);	

				 if(usrarea==null || usrarea.equals(""))
				 {}else{
					 oo.textpg(o.usrarea, usrarea);
					 Thread.sleep(6000);
				 }	
				
				
				
				 if(Eqid==null || Eqid.equals(""))
				 {}else{
					 oo.textpg(o.Eqid, Eqid);
					 Thread.sleep(6000);
				 }	
				
				
				 if(Block==null || Block.equals(""))
				 {}else{
					 oo.textpg(o.Block, Block);
					 Thread.sleep(6000);
				 }
				
		
		 
					
				 if(Location==null || Location.equals(""))
				 {}else{
					 oo.textpg(o.Location, Location);
					 Thread.sleep(9000);
				 }
			 

			// oo.clickpg(o.submit);
			 
			 
			 oo.jsclickpg(o.submit);
			 
			 
			 
			 
			 
			 Thread.sleep(3000);
		
				 if(o.wve.isDisplayed())				 
				 {
					 System.out.println("plssssssss");
					 Logger ll =Logger.getLogger("pls");
						ll.trace(o.wve.getText());Thread.sleep(2000);
					 
					 
				 }else {
					 System.out.println("alredy exiasting");
					 Logger ll =Logger.getLogger("INSTRUMENT ID ALREADY EXISTS");
						ll.trace("alredy existing");Thread.sleep(2000);
				 }
				 
				 
				
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);
	 
	 
	 
	 
	 	}
	 	test = extent.createTest("block master");
		test.createNode("block master information");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
			
			
		}
	 
	 
	 
	 
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{
		
		System.out.println("tested");
		 test = extent.createTest("plant clicked");
			test.createNode("click on plant2");
			 Assert.assertTrue(true);/*
		


		this.driver = plantbase.driver;
		Thread.sleep(100);
		System.out.println("testing on admin role module2 ");
		plantpg o = PageFactory.initElements(driver, plantpg.class);
		util oo = PageFactory.initElements(driver, util.class);
		
		try {	
			//oo.flu(o.badmin); 
			Thread.sleep(7000);
			oo.clickpg(o.master);
		Thread.sleep(3000);//oo.clickpg(o.usrreg);
		
		}catch (Exception e) {
			// TODO: handle exception
		System.out.println(e.getMessage());
			
		}
		 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
		
		
	*/}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
