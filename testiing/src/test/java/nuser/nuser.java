package nuser;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import test1.*;
import pages.*;
public class nuser {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
}
 
  
	






///////////////////////////////////////////////////////////////////////






@DataProvider(name="auseradd")
public Object[][] adminuseradddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("adduser","excel/nuser.xlsx");
	
  return object;	 
}






//public  void adminuseraddf(String name, String n,String alname,String fname, String lastn,String gen,String email,String pwd, String cpwd,String mnum, String rnum,String roles, String add1, String add2,String coun,String state,String city, String zip) throws InterruptedException {
	@Test(dataProvider ="auseradd", priority=2)
public void adminuseraddf(String plantname,String username ,String Uname ,String email ,String role,String dep ,String pwd ,String cpwd ,String cname,String Remark,String ppwd) throws InterruptedException, IOException{

System.out.println(plantname+"plant name is...............am showing here ");


	 try {
		 
		 
		 System.out.println("user  clicked");	
		 this.driver=base.driver;
			agilepg o = PageFactory.initElements(driver, agilepg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
		 
		 
		 if(o.usrreg.isDisplayed()){	 Thread.sleep(3000);
			
			System.out.println("clicked ");}
		 else{
			 System.out.println("administrator clicked then fmaster ");
				oo.clickpg(o.administrator);Thread.sleep(5000);
				
			}
		 
		 
			oo.clickpg(o.usrreg);			
			Thread.sleep(3000);
			oo.waits(o.badduser);
			 
			 oo.clickpg(o.adduser);
			 
			 
			 // user details
			 Thread.sleep(3000);
				oo.waits(o.busername);
				 
				 if(plantname==null || plantname.equals(""))
					 {}else{
				oo.dropdownpg(o.plantname, plantname);
					 }//plantname
				 Thread.sleep(2000);
				 
			 if(username==null || username.equals(""))
				 {}else{
			 oo.textpg(o.username, username);}
			 Thread.sleep(2000);
			 
			 
			 
			 if(Uname==null || Uname.equals(""))
				 {}
			 else  if(Uname.length()<4){
				 
				 
			 }
				 
				 else{
			 oo.textpg(o.Uname, Uname);}
			 Thread.sleep(2000);
			 
			 if(email==null || email.equals(""))
			 {}else{
			 oo.textpg(o.email, email);}
			 Thread.sleep(2000);
			 if(role==null || role.equals(""))
			 {}else{
			 oo.dropdownpg(o.role, role);
			 
			 }
			 Thread.sleep(2000);
			 if(dep==null || dep.equals(""))
			 {}else{
			 
			 oo.dropdownpg(o.Department, dep);}
			 
			 
			 
			 Thread.sleep(2000);
			 if(pwd==null || pwd.equals(""))
			 {}
			 
			 else{
			 oo.textpg(o.password, pwd);}
			 
			 Thread.sleep(2000);
			 
			 if(cpwd==null || cpwd.equals(""))
			 {}else{
			 oo.textpg(o.confirmpassword, cpwd);
			 }
			 Thread.sleep(2000);
			 
			 
			 oo.dropdownpg(o.companyname, cname);
 
			 Thread.sleep(2000);
			 oo.jsdownbye(o.reggistraion);Thread.sleep(2000);
			 oo.clickpg(o.reggistraion); Thread.sleep(2000);
			 
			 JavascriptExecutor jse = (JavascriptExecutor)driver;
				jse.executeScript("window.scrollBy(0,-250)");
			 Thread.sleep(3000);
			 
			 
			 
			 
			 if(o.Remark.isDisplayed()  ){	
				 oo.waits(o.bRemark);
					
					Thread.sleep(2000);								
					
					 oo.textpg(o.Remark, Remark);
					Thread.sleep(2000);
					 oo.textpg(o.pwd, ppwd);Thread.sleep(2000);
					 oo.clickpg(o.validateuser);	 
					 
					Thread.sleep(3000);  oo.waits(o.buaalert);	
					 System.out.println(o.uaalert.getText());
					 
					 Logger ll =Logger.getLogger("nuser ");
					 ll.trace(o.uaalert.getText());

					 Assert.assertEquals(o.uaalert.getText(), "0_USER ALREADY EXISTS");Thread.sleep(3000);
					 oo.clickpg(o.Errorbtn);		
					 Thread.sleep(2000);
				 
				
				 
			 }else if(o.psdvalidate.isDisplayed()){	 Thread.sleep(2000);
			 Logger ll =Logger.getLogger("nuser ");
			 ll.trace(o.psdvalidate.getText());
				
				 System.out.println(o.psdvalidate.getText()+"passs word data is  not correct format2222");
				 
				 
			 }	 	 
			 else if(o.cpswdvalidate.isDisplayed()){	 Thread.sleep(2000);
			 Logger ll =Logger.getLogger("nuser ");
			 ll.trace(o.cpswdvalidate.getText());
		
			 System.out.println(o.cpswdvalidate.getText()+"passs word missmatched");
			 Assert.assertEquals(o.cpswdvalidate.getText(), "Passwords Does not Match");
			 Thread.sleep(2000);
			 }
			 
			 else{
				 Thread.sleep(2000);
				 Logger ll =Logger.getLogger("n user ");
				 ll.trace(o.useralert.getText());

				 System.out.println("please put something here");
				 Assert.assertEquals(o.useralert.getText(), "please put something here");
				 
			 }
			 
			 
			 
			 
			
				
					 
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("nuser ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);
	 
	 
	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("we can't add user  with existing  values");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
		}
	 
	 
	 
	 
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
