package nuser;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import test1.*;
import pages.*;
public class nrole {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
}
 
  
	






///////////////////////////////////////////////////////////////////////






@DataProvider(name="adminroleadd")
public Object[][] adminroleadddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("addrole","excel/nuser.xlsx");
	
  return object;	 
}






//public  void adminuseraddf(String name, String n,String alname,String fname, String lastn,String gen,String email,String pwd, String cpwd,String mnum, String rnum,String roles, String add1, String add2,String coun,String state,String city, String zip) throws InterruptedException {
	@Test(dataProvider ="adminroleadd", priority=2)
public void adminroleaddf(String Level,String typeofroles ,String RoleDesc ,String Remark,String ppwd) throws InterruptedException, IOException{




	 try {
		 
		 
		 System.out.println("role  clicked");	
		 this.driver=base.driver;
			agilepg o = PageFactory.initElements(driver, agilepg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
		 
		 
		 if(o.usrreg.isDisplayed()){	 Thread.sleep(3000);
			
			System.out.println("clicked ");}
		 else{
			 System.out.println("administrator clicked then fmaster ");
				oo.clickpg(o.administrator);Thread.sleep(5000);
				
			}

			oo.clickpg(o.usrrole);
			
			Thread.sleep(3000);
			oo.waits(o.badduserrole);
			 
			 oo.clickpg(o.adduserrole);
			 
			 
			 // user details
			 Thread.sleep(3000);
				oo.waits(o.btypeofroles);
				
				
				 if(Level==null || Level.equals(""))
				 {}else{
				oo.dropdownpg(o.Level, Level);
				 Thread.sleep(2000);
				 }
				 
				 

				 if(typeofroles==null || typeofroles.equals(""))
				 {}else{
			 oo.textpg(o.typeofroles, typeofroles);
			 Thread.sleep(2000);
				 }
			 
				 
				 if(RoleDesc==null || RoleDesc.equals(""))
				 {}else{ 
			 oo.textpg(o.RoleDesc, RoleDesc);
			 Thread.sleep(2000);
				 }
			 
			// oo.jsdownbye(o.reggistraion);Thread.sleep(2000);
			 oo.clickpg(o.sub); Thread.sleep(4000);
			 
			 if(o.Remark.isDisplayed()  ){
				 

				 
				 
				 
				 Thread.sleep(3000);
					oo.waits(o.bRemark);
					
					Thread.sleep(2000);
					 oo.textpg(o.Remark, Remark);
					Thread.sleep(2000);
					 oo.textpg(o.pwd, ppwd);
					 oo.clickpg(o.validateuser);	Thread.sleep(2000); 
					 
					 Logger ll =Logger.getLogger("n role ");
					 ll.trace(o.uaalert.getText());

					
					 //Electronic Signature Process Completed Successfully /html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div/div[1]/div/div[1]/h4
					 Assert.assertEquals(o.uaalert.getText(), "ROLE IS ALRAEDY EXISTS");	 
					 oo.waits(o.bErrorbtn);	 
					 oo.clickpg(o.Errorbtn);
					 
					 
					 
					 
				 
				
		 }
			 else{
			 
			 
				 
				 Thread.sleep(2000);
			 System.out.println("please put something here");
			 Logger ll =Logger.getLogger("n role ");
			 ll.trace(o.useralert.getText());

			 Assert.assertEquals(o.useralert.getText(), "please put something here");
			
				 
				 
				 
			 }
			 
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);
	 
	 
	 
	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("roleadd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
		
		}
	 
	 
	 
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
} 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
	
 extent.flush();

}



/*@AfterSuite
public void tearDown() {
   driver.quit();
}
*/

 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
