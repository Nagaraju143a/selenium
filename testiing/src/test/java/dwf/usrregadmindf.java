package dwf;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import pages.agilepg;
import plantpages.*;
import test1.base;
import planttests.plantbase;
import pages.*;

public class usrregadmindf {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {
		System.out.println("testing...");
		 test = extent.createTest("plant clicked");
			test.createNode("click on plant2");
			 Assert.assertTrue(true);
}
 
  
	






///////////////////////////////////////////////////////////////////////





@DataProvider(name="auseradd")
public Object[][] adminuseradddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("adduser","excel/defwf.xlsx");
	
  return object;	 
}






//public  void adminuseraddf(String name, String n,String alname,String fname, String lastn,String gen,String email,String pwd, String cpwd,String mnum, String rnum,String roles, String add1, String add2,String coun,String state,String city, String zip) throws InterruptedException {
	@Test(dataProvider ="auseradd", priority=2)
public void adminuseraddf(String n, String p,String plantname,String username ,String Uname ,String email ,String role,String dep ,String pwd ,String cpwd ,String cname,String Remark,String ppwd,String value) throws InterruptedException, IOException{

System.out.println(plantname+"plant name is...............am showing here ");


	 try {
		 
		 
		 Properties u= new Properties();
			u.load(ds.class.getResourceAsStream("object.properties"));
			System.out.println(u.getProperty("url")+"dddddddddddddd");
			
			
		 System.out.println("block master  clickedssssssssssssssssssssssssss");	
		 this.driver=plantbase.driver;
			agilepg o = PageFactory.initElements(driver, agilepg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
	
		 
	
	
	
		
		Thread.sleep(4000);
		oo.waits(o.blogout1);
		oo.clickpg(o.logout1);
	Thread.sleep(3000);//oo.clickpg(o.usrreg);
	oo.clickpg(o.logout2);
	Thread.sleep(3000);
	
	driver.get(u.getProperty("url"));
				
	//driver.get(u.getProperty("url"));
	Thread.sleep(4000);
	if(n.contains(".")){
		oo.textipg(o.loginname, n);
	}else{
	oo.textpg(o.loginname, n);
	
	}
	
	
	
			
Thread.sleep(5000);
oo.textpg(o.loginpassword, p);

Thread.sleep(4000);
oo.clickpg(o.loginsub);

Thread.sleep(3000);
System.out.println("addddddddddd");

if(o.usrreg.isDisplayed()){	 Thread.sleep(3000);

System.out.println("clicked ");}
else{
System.out.println("administrator clicked then fmaster ");
	oo.clickpg(o.administratorf);Thread.sleep(5000);
	
}


oo.waits(o.busrreg);
Thread.sleep(3000);
oo.clickpg(o.usrreg);
			
			Thread.sleep(3000);
			oo.waits(o.badduser);
			 
			 oo.clickpg(o.adduser);
			 
			 
			 // user details
			 Thread.sleep(3000);
				oo.waits(o.busername);
				 Thread.sleep(3000);
				
				if(plantname.equals("Unit � 15")){
					System.out.println("same values");
					oo.dropdownpg(o.plantname,"Unit - 15");
				}else{
				oo.dropdownpg(o.plantname, plantname);
				}
				 Thread.sleep(2000);
			 oo.textpg(o.username, username);
			 Thread.sleep(2000);
			 oo.textpg(o.Uname, Uname);
			 Thread.sleep(2000);
			 oo.textpg(o.email, email);
			 Thread.sleep(2000);
			 oo.dropdownpg(o.role, role);
			 
			 
			 Thread.sleep(2000);
			 oo.dropdownpg(o.Department, dep);
			 Thread.sleep(2000);
			 oo.textpg(o.password, pwd);
			 Thread.sleep(2000);
			 oo.textpg(o.confirmpassword, cpwd);
			 Thread.sleep(2000);
			 oo.dropdownpg(o.companyname, cname);
			
			
	List<WebElement> ddff=driver.findElements(By.xpath(o.asgblk));
	System.out.println(ddff.size()+"testing size");
	
	
		if(ddff.size()==1){
			
			Thread.sleep(2000);
			
			//BLOCK-02266       PRODUCTION BLOCK-1
			System.out.println(o.weasgblk.getAttribute("value"));
			
			oo.clickpg(o.weasgblk);
			
			
			 System.out.println("single ele");
		}else if(ddff.size()==0){
			 System.out.println("no  ele");
			
		}else{
			//String value="BLOCK-02246";
			//String ac ="/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div[2]/form/div/div[7]/div/input";
			List rows = driver.findElements(By.xpath(o.asgblk));
			
			System.out.println("No of rows areeeeeeeeeee : " + rows.size());
		
			for (int i = 1; i <= rows.size(); i++) {
				String b =o.asgblkdiv;
				String a = b + "input[" + i + "]";////////////////////
				
				WebElement tableRow = driver.findElement(By.xpath(a));

			
				WebElement cellIneed = tableRow.findElement(By.xpath(a));

				String valueIneed = cellIneed.getAttribute("value");
				System.out.println(valueIneed+"data");
				if (valueIneed.equals(value)) {	
					Thread.sleep(2000);
				
					cellIneed.click();
					
				}
				
				
			}
			
		}
					 
					
				
			 
			 Thread.sleep(2000);
			 oo.jsdownbye(o.reggistraion);Thread.sleep(2000);
			 oo.clickpg(o.reggistraion); Thread.sleep(2000);
			 
			 
			 Thread.sleep(3000);
				oo.waits(o.bRemark);
				
				Thread.sleep(2000);
				 oo.textpg(o.Remark, Remark);
				Thread.sleep(2000);
				 oo.textpg(o.pwd, ppwd);Thread.sleep(2000);
				 oo.clickpg(o.validateuser);	 	Thread.sleep(4000);
				// Electronic Signature Process Completed Successfully
				 Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");
				 Thread.sleep(4000);
				 oo.waits(o.bERES);	 
				 oo.clickpg(o.ERES);		Thread.sleep(4000);
				 
				 
				 System.out.println("status1"+o.uaalert.getText());
				 
				 Logger ll =Logger.getLogger("user add ");
				 ll.trace(o.uaalert.getText());

				 
				// Assert.assertEquals(o.uaalert.getText(), "User Approve Process Completed Successfully");
				 oo.waits(o.bokbtn);	
					
				 oo.clickpg(o.okbtn);
				 Thread.sleep(4000);
					driver.navigate().refresh(); 
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);


	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
	 
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
		}
	 
	 
	 

	 
	 
	 
	
	
	
	

@DataProvider(name="addrev")
public Object[][] addrev() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("usrdf","excel/defwf.xlsx");
	
  return object;	 
}

	
	
	
	@Test(dataProvider ="addrev", priority=3)
	public void rev1(String n,String p ,String n1,String p1 ,String ni,String pi , String FORMULATIONID ,String reva, String apr) throws InterruptedException, IOException{
		
			
			 try {
				 
				
					Properties u= new Properties();
					u.load(ds.class.getResourceAsStream("object.properties"));
					System.out.println(u.getProperty("url")+"dddddddddddddd");
					
					
				 System.out.println("block master  clickedssssssssssssssssssssssssss");	
				 this.driver=plantbase.driver;
					agilepg o = PageFactory.initElements(driver, agilepg.class);
					util oo = PageFactory.initElements(driver, util.class);
				 Thread.sleep(3000);
			
				 
			
			
			
				
				Thread.sleep(4000);
				oo.waits(o.blogout1);
				oo.clickpg(o.logout1);
			Thread.sleep(3000);//oo.clickpg(o.usrreg);
			oo.clickpg(o.logout2);
			Thread.sleep(3000);
			
			driver.get(u.getProperty("url"));
						
			//driver.get(u.getProperty("url"));
			Thread.sleep(4000);
			if(n.contains(".")){
				oo.textipg(o.loginname, n);
			}else{
			oo.textpg(o.loginname, n);
			
			}
			
			
			
					
	Thread.sleep(5000);
	oo.textpg(o.loginpassword, p);

	Thread.sleep(4000);
	oo.clickpg(o.loginsub);

	Thread.sleep(3000);
	System.out.println("addddddddddd");

	 if(o.usrreg.isDisplayed()){	 Thread.sleep(3000);
		
		System.out.println("clicked ");}
	 else{
		 System.out.println("administrator clicked then fmaster ");
			oo.clickpg(o.administratorf);Thread.sleep(5000);
			
		}

	 
oo.waits(o.busrreg);
Thread.sleep(3000);
oo.clickpg(o.usrreg);


//depsearch

	Thread.sleep(4000);
		 oo.textpg(o.depsearch, FORMULATIONID);
		 Thread.sleep(4000);
		 
		
		 List <WebElement> searchusize =driver.findElements(o.bdepupdatebtn1);
		 System.out.println(searchusize.size()+"size......");
		 if(searchusize.size()==1){	


	 Thread.sleep(3000);
				 
		 oo.clickpg(o.depupdatebtn1);
		 Thread.sleep(4000);
		
		 oo.waits(o.busrr);
		 
		 
		 //review//////////////////////////////////////////////////
		 if(reva.equals("raproved")){
			 oo.jsdownbye(o.usrr);
			 
		 oo.clickpg(o.usrr);
		 Thread.sleep(4000);
		 
		 oo.textpg(o.Remark, "ok");
	Thread.sleep(2000);
	 oo.textpg(o.pwd, p);Thread.sleep(2000);
		 
	 oo.clickpg(o.validateuser);Thread.sleep(2000);
	 oo.clickpg(o.ERES);Thread.sleep(2000);
	 oo.clickpg(o.okbtn);Thread.sleep(2000);
		
	
	 


		//Thread.sleep(4000);
		oo.waits(o.blogout1);
		oo.clickpg(o.logout1);
		Thread.sleep(3000);//oo.clickpg(o.usrreg);
		oo.clickpg(o.logout2);
		Thread.sleep(3000);
		driver.get(u.getProperty("url"));
		Thread.sleep(4000);
		if(n.contains(".")){
		oo.textipg(o.loginname, n1);
		}else{
		oo.textpg(o.loginname, n1);

		}



			
		Thread.sleep(5000);
		oo.textpg(o.loginpassword, p1);

		Thread.sleep(4000);
		oo.clickpg(o.loginsub);

		Thread.sleep(3000);
		
		if(o.usrreg.isDisplayed()){	 Thread.sleep(3000);

		System.out.println("clicked ");}
		else{
		System.out.println("matster clicked then fmaster ");
		oo.clickpg(o.administratorf);
		Thread.sleep(5000);


		}
		
		oo.waits(o.busrreg);
		Thread.sleep(3000);
		oo.clickpg(o.usrreg);

		

		Thread.sleep(4000);
		oo.textpg(o.depsearch, FORMULATIONID);
		Thread.sleep(4000);

		
		List <WebElement> searchusizes =driver.findElements(o.bdepupdatebtn1);
		System.out.println(searchusizes.size()+"size......");
		if(searchusizes.size()==1){	

			 Thread.sleep(4000);
			 oo.clickpg(o.depupdatebtn1);
			 Thread.sleep(4000);
			
			 oo.waits(o.busra);

			 
				
				//review by apr 
			 
			 if(apr.equals("aproved")){
				
				 
				 oo.jsdownbye(o.usra);
				  
			 oo.clickpg(o.usra);
		Thread.sleep(4000);
		
		 oo.textpg(o.Remark, "ok");
			Thread.sleep(2000);
			 oo.textpg(o.pwd, p1);Thread.sleep(2000);
				 
			 oo.clickpg(o.validateuser);Thread.sleep(2000);
			 oo.clickpg(o.ERES);Thread.sleep(2000);
			 oo.clickpg(o.okbtn);Thread.sleep(2000);
		
			 
									 }else{
										 
										 // apr reject 
										 
										 
										 Thread.sleep(2000);
										 oo.jsdownbye(o.usra);
										 oo.clickpg(o.usra);
										 oo.textpg(o.Remark, "ok");
											Thread.sleep(2000);
											 oo.textpg(o.pwd, p1);Thread.sleep(2000);
												 
											 oo.clickpg(o.validateuser);Thread.sleep(2000);
											 oo.clickpg(o.ERES);Thread.sleep(2000);
											 oo.clickpg(o.okbtn);Thread.sleep(2000);
										 
										
											 
										 
										 // intiaor drop here/////////////////////////////////////////////////////////
										 
											
							
											Thread.sleep(4000);
											oo.waits(o.blogout1);
											oo.clickpg(o.logout1);
										Thread.sleep(3000);//oo.clickpg(o.usrreg);
										oo.clickpg(o.logout2);
										Thread.sleep(3000);
										driver.get(u.getProperty("url"));
										Thread.sleep(4000);
										if(n.contains(".")){
											oo.textipg(o.loginname, ni);
										}else{
										oo.textpg(o.loginname, ni);
										
										}
										
										
										
												
								Thread.sleep(5000);
								oo.textpg(o.loginpassword, pi);
							
								Thread.sleep(4000);
								oo.clickpg(o.loginsub);
							
								Thread.sleep(3000);
								
							
								if(o.usrreg.isDisplayed()){	 Thread.sleep(3000);
									
									System.out.println("clicked ");}
								else{
									 System.out.println("matster clicked then fmaster ");
										//oo.clickpg(o.master);
										oo.clickpg(o.administratorf);
										Thread.sleep(5000);
										
										
									}
								
								oo.waits(o.busrreg);
								Thread.sleep(3000);
									oo.clickpg(o.usrreg);
										 
										 
										 
									Thread.sleep(4000);
									 oo.textpg(o.depsearch, FORMULATIONID);
									 Thread.sleep(4000);
									 
									 
									 List <WebElement> searchusized =driver.findElements(o.bdepupdatebtn1);
									 System.out.println(searchusize.size()+"size......");
									 if(searchusized.size()==1){	
							
										 
								 Thread.sleep(3000);
									oo.clickpg(o.depupdatebtn1);
									 
									 Thread.sleep(3000);
								oo.jsdownbye(o.usrdrp);
									oo.clickpg(o.usrdrp);
									
									Thread.sleep(3000);
						
									
									 oo.textpg(o.Remark, "ok");
									Thread.sleep(2000);
									 oo.textpg(o.pwd, pi);Thread.sleep(2000);
										 
									 oo.clickpg(o.validateuser);Thread.sleep(2000);
									 oo.clickpg(o.ERES);Thread.sleep(2000);
									 oo.clickpg(o.okbtn);Thread.sleep(2000);
									
									
							}else{
								
								System.out.println("no need perform any action initaor not there");
							}
							
							
							
							Thread.sleep(6000);
										 
										 
									
									 
									 
									 
									 }
		

			 
		 
			 
			 
		}else{

		System.out.println("no need perform any action no apr record"); Thread.sleep(3000);



		}


	 
	 
	 
	 
		 
		 }else{
			 
		
		 // rev rej
			 Thread.sleep(4000);
			 oo.jsdownbye(o.usrrj); Thread.sleep(2000);
		oo.clickpg(o.usrrj);	 
			 
		 oo.textpg(o.Remark, "ok");
			Thread.sleep(2000);
			 oo.textpg(o.pwd, p);Thread.sleep(2000);
				 
			 oo.clickpg(o.validateuser);Thread.sleep(2000);
			 oo.clickpg(o.ERES);Thread.sleep(2000);
			 oo.clickpg(o.okbtn);Thread.sleep(2000);
			 
	 
			 
			 // intiaor drop here/////////////////////////////////////////////////////////
			 
		
			 
			 Thread.sleep(4000);
				oo.waits(o.blogout1);
				oo.clickpg(o.logout1);
			Thread.sleep(3000);//oo.clickpg(o.usrreg);
			oo.clickpg(o.logout2);
			Thread.sleep(3000);
			driver.get(u.getProperty("url"));
			Thread.sleep(4000);
			if(n.contains(".")){
				oo.textipg(o.loginname, ni);
			}else{
			oo.textpg(o.loginname, ni);
			
			}
			
			
			
					
	Thread.sleep(5000);
	oo.textpg(o.loginpassword, pi);

	Thread.sleep(4000);
	oo.clickpg(o.loginsub);

	Thread.sleep(3000);
	

	if(o.usrreg.isDisplayed()){	 Thread.sleep(3000);
		
		System.out.println("clicked ");}
	else{
		 System.out.println("matster clicked then fmaster ");
			//oo.clickpg(o.master);
			oo.clickpg(o.administratorf);
			Thread.sleep(5000);
			
			
		}
	
	oo.waits(o.busrreg);
	Thread.sleep(3000);
		oo.clickpg(o.usrreg);
			 
			 
			 
		Thread.sleep(4000);
		 oo.textpg(o.depsearch, FORMULATIONID);
		 Thread.sleep(4000);
		 
		 
		 List <WebElement> searchusized =driver.findElements(o.bdepupdatebtn1);
		 System.out.println(searchusize.size()+"size......");
		 if(searchusized.size()==1){	

			 
	 Thread.sleep(3000);
		oo.clickpg(o.depupdatebtn1);
		 
		 Thread.sleep(3000);
	oo.jsdownbye(o.usrdrp);
		oo.clickpg(o.usrdrp);
		
		Thread.sleep(3000);

		
		 oo.textpg(o.Remark, "ok");
		Thread.sleep(2000);
		 oo.textpg(o.pwd, pi);Thread.sleep(2000);
			 
		 oo.clickpg(o.validateuser);Thread.sleep(2000);
		 oo.clickpg(o.ERES);Thread.sleep(2000);
		 oo.clickpg(o.okbtn);Thread.sleep(2000);
		
		
}else{
	
	System.out.println("no need perform any action initaor not there");
}



Thread.sleep(6000);
			 
			 
					 
			 
		 }
	 
	 
}else{
	
	System.out.println("no need perform any action bcs reviwer not matched"); Thread.sleep(3000);

	
}



Thread.sleep(6000);
	//////////////////////////////////////////////////////////////////////////////////////

 
		 }catch (Exception e)
		 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
		// TODO: handle exception
		 Logger ll =Logger.getLogger("n role ");
		 ll.trace(e.getMessage());
		 Thread.sleep(4000);
			util oo = PageFactory.initElements(driver, util.class);
		oo.srnsht(driver);

		 	}
		 	test = extent.createTest("adminuseraddf");
			test.createNode("useradd with valid input");
			 Assert.assertTrue(true);
			 Thread.sleep(2000);
				driver.navigate().refresh();Thread.sleep(2000);
		
			}
		 
		 
		 
	
		 
	
	
	
	
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{	System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
