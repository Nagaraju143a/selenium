package dwf;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import pages.agilepg;
import plantpages.*;
import test1.base;
import planttests.plantbase;
import pages.*;

public class chdepadmindf {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
}
 
  
	






///////////////////////////////////////////////////////////////////////




@DataProvider(name="adminchdep")
public Object[][] adminchdepdata() throws IOException{
	
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("changedep","excel/defwf.xlsx");
	
  return object;	 
}






@Test(dataProvider ="adminchdep", priority=2)
public void adminchdepf(String n , String p, String Userid,String dep,String Remark,String ppwd ) throws InterruptedException, IOException{



	 try {
		 
		 
		 Properties u= new Properties();	u.load(ds.class.getResourceAsStream("object.properties"));
		 
		 System.out.println("block master  clickedssssssssssssssssssssssssss");	
		 this.driver=plantbase.driver;
			agilepg o = PageFactory.initElements(driver, agilepg.class);
			util oo = PageFactory.initElements(driver, util.class);
				
		Thread.sleep(4000);
		oo.waits(o.blogout1);
		oo.clickpg(o.logout1);
	Thread.sleep(3000);//oo.clickpg(o.usrreg);
	oo.clickpg(o.logout2);
	Thread.sleep(3000);
	driver.get(u.getProperty("url"));
	Thread.sleep(4000);
	if(n.contains(".")){
		oo.textipg(o.loginname, n);
	}else{
	oo.textpg(o.loginname, n);
	 
	}
	
			
Thread.sleep(3000);
oo.textpg(o.loginpassword, p);

Thread.sleep(4000);
oo.clickpg(o.loginsub);

Thread.sleep(3000);

if(o.usrreg.isDisplayed()){	 Thread.sleep(3000);

System.out.println("clicked ");}
else{
 System.out.println("administrator clicked then fmaster ");
	oo.clickpg(o.administratorf);Thread.sleep(5000);
	
}


oo.waits(o.bchdep);
Thread.sleep(3000);
oo.clickpg(o.chdep);




			
			Thread.sleep(3000);
			oo.clickpg(o.adduser);
			
			oo.waits(o.bchdepUserid);	
			
			// drop down list in values existing or not checking here 
			// more than 1 bcs default select options is concider as 1 
			
			Select si= new Select (o.chdepUserid);
		
			System.out.println(si.getOptions().size()+"heheee");
			
			if(si.getOptions().size()>1){
				
			//System.out.println("drop down list is available "+si.getOptions());
								
					
			
			
			if(Userid.contains(".")){
				double d = Double.parseDouble(Userid);
				int i = (int) d;
				oo.dropdownpg(o.chdepUserid, String.valueOf(i));
			}else{
				oo.dropdownpg(o.chdepUserid, Userid);
			
			}
			
	
				 Thread.sleep(3000);
				 
				 
				 
					Select si1= new Select (o.Tobechangedept);	
					if(si1.getOptions().size()>1){		
						oo.dropdownpg(o.Tobechangedept, dep);
					}else{
						System.out.println("no drop down values");
					}
				 
				 
				 
				 
				 Thread.sleep(2000);
				  oo.clickpg(o.chdepsubmit);
				 				 
				 Thread.sleep(3000);
				 				 
				// Remark  pwd  validateuser      ERES   okbtn
				 Thread.sleep(3000);
					oo.waits(o.bRemark);
					
					Thread.sleep(2000);
					 oo.textpg(o.Remark, Remark);
					Thread.sleep(2000);
					 oo.textpg(o.pwd, ppwd);
					oo.clickpg(o.validateuser);
					 
					
					 
					 
					 	if(o.uaalert.getText().equals("Electronic Signature Process Completed Successfully")){
					 		Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");
							 oo.clickpg(o.ERES);Thread.sleep(2000);	
							 Logger ll =Logger.getLogger("change dep ");
						      	ll.trace(o.uaalert.getText());

						// Assert.assertEquals(o.uaalert.getText(), "Change Department Initiation Process Completed Successfully ");
						 Thread.sleep(2000);
						 oo.clickpg(o.okbtn);
					 
					
					 
					 
					
						 }else{		Thread.sleep(2000);
							 Logger ll =Logger.getLogger("change dep");
						      	ll.trace(o.uaalert.getText());
							 //Assert.assertEquals(o.uaalert.getText(), "EQUIPMENTS HAS BEEN ASSINGED TO AREA UNABLE TO DE-ACTIVE");
							 oo.waits(o.bErrorbtn);	 
							 oo.clickpg(o.Errorbtn);	Thread.sleep(2000);	
					}
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
				
					 
			}else{
				 Logger ll =Logger.getLogger("change dep");
			      	ll.trace("drop down problem");
				
				System.out.println("drop down list is emp there is no userid list");
			}	 
					 
					 
					 
			 
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);


	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("roleadd with valid input");
		 Assert.assertTrue(true);
	 
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
		}
	 
	 	

@DataProvider(name="addrev")
public Object[][] addrev() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("chdepdf","excel/defwf.xlsx");
	
  return object;	 
}

	
	
	
	
	@Test(dataProvider ="addrev", priority=3)
	public void rev1(String n,String p ,String n1,String p1 ,String ni,String pi , String FORMULATIONID ,String reva, String apr) throws InterruptedException, IOException{
			
			
			
			 try {Properties u= new Properties();	u.load(ds.class.getResourceAsStream("object.properties"));
				 
				 System.out.println("block master  clickedssssssssssssssssssssssssss");	
				 this.driver=plantbase.driver;
					agilepg o = PageFactory.initElements(driver, agilepg.class);
					util oo = PageFactory.initElements(driver, util.class);
						
				Thread.sleep(4000);
				oo.waits(o.blogout1);
				oo.clickpg(o.logout1);
			Thread.sleep(3000);//oo.clickpg(o.usrreg);
			oo.clickpg(o.logout2);
			Thread.sleep(3000);
			driver.get(u.getProperty("url"));
			Thread.sleep(4000);
			if(n.contains(".")){
				oo.textipg(o.loginname, n);
			}else{
			oo.textpg(o.loginname, n);
			 
			}
			
					
	Thread.sleep(3000);
	oo.textpg(o.loginpassword, p);

	Thread.sleep(4000);
	oo.clickpg(o.loginsub);

	Thread.sleep(3000);

	 if(o.usrreg.isDisplayed()){	 Thread.sleep(3000);
		
		System.out.println("clicked ");}
	 else{
		 System.out.println("administrator clicked then fmaster ");
			oo.clickpg(o.administratorf);Thread.sleep(5000);
			
		}

	 
	 oo.waits(o.bchdep);
	 Thread.sleep(3000);
	 oo.clickpg(o.chdep);

	


		Thread.sleep(4000);
			 oo.textpg(o.rolepvsearch, FORMULATIONID);
			 Thread.sleep(4000);
			 
			
			 List <WebElement> searchusize =driver.findElements(o.bchdepu);
			 System.out.println(searchusize.size()+"size......");
			 if(searchusize.size()==1){	


		 Thread.sleep(3000);
					 
			 oo.clickpg(o.chdepu);
			 Thread.sleep(4000);
			
			 oo.waits(o.bchdepa);
			 
			 
			 //review//////////////////////////////////////////////////
			 if(reva.equals("raproved")){
				 oo.jsdownbye(o.chdepa);
				 
			 oo.clickpg(o.chdepa);
			 Thread.sleep(4000);
			 
			 oo.textpg(o.Remark, "ok");
		Thread.sleep(2000);
		 oo.textpg(o.pwd, p);Thread.sleep(2000);
			 
		 oo.clickpg(o.validateuser);Thread.sleep(2000);
		 oo.clickpg(o.ERES);Thread.sleep(2000);
		 oo.clickpg(o.okbtn);Thread.sleep(2000);
			
		
		 


			//Thread.sleep(4000);
			oo.waits(o.blogout1);
			oo.clickpg(o.logout1);
			Thread.sleep(3000);//oo.clickpg(o.usrreg);
			oo.clickpg(o.logout2);
			Thread.sleep(3000);
			driver.get(u.getProperty("url"));
			Thread.sleep(4000);
			if(n.contains(".")){
			oo.textipg(o.loginname, n1);
			}else{
			oo.textpg(o.loginname, n1);

			}



				
			Thread.sleep(5000);
			oo.textpg(o.loginpassword, p1);

			Thread.sleep(4000);
			oo.clickpg(o.loginsub);

			Thread.sleep(3000);
			
			if(o.usrreg.isDisplayed()){	 Thread.sleep(3000);

			System.out.println("clicked ");}
			else{
			System.out.println("matster clicked then fmaster ");
			oo.clickpg(o.administratorf);
			Thread.sleep(5000);


			}
			 
			oo.waits(o.bchdep);
			Thread.sleep(3000);
			oo.clickpg(o.chdep);

			

			Thread.sleep(4000);
			oo.textpg(o.rolepvsearch, FORMULATIONID);
			Thread.sleep(4000);

			
			List <WebElement> searchusizes =driver.findElements(o.bchdepu);
			System.out.println(searchusizes.size()+"size......");
			if(searchusizes.size()==1){	

				 Thread.sleep(4000);
				 oo.clickpg(o.chdepu);
				 Thread.sleep(4000);
				
				 oo.waits(o.bchdepa);

				 
					
					//review by apr 
				 
				 if(apr.equals("aproved")){
					
					 
					 oo.jsdownbye(o.chdepa);
					 
				 oo.clickpg(o.chdepa);
			Thread.sleep(4000);
			
			 oo.textpg(o.Remark, "ok");
				Thread.sleep(2000);
				 oo.textpg(o.pwd, p1);Thread.sleep(2000);
					 
				 oo.clickpg(o.validateuser);Thread.sleep(2000);
				 oo.clickpg(o.ERES);Thread.sleep(2000);
				 oo.clickpg(o.okbtn);Thread.sleep(2000);
			
			
										 }else{
											 
											 // apr reject 
											 
											 
											 Thread.sleep(2000);
											 oo.jsdownbye(o.chdepa);
											 oo.clickpg(o.chdepr);
											 oo.textpg(o.Remark, "ok");
												Thread.sleep(2000);
												 oo.textpg(o.pwd, p1);Thread.sleep(2000);
													 
												 oo.clickpg(o.validateuser);Thread.sleep(2000);
												 oo.clickpg(o.ERES);Thread.sleep(2000);
												 oo.clickpg(o.okbtn);Thread.sleep(2000);
											 
											
											
											 
											 // intiaor drop here/////////////////////////////////////////////////////////
											 
												
								
												Thread.sleep(4000);
												oo.waits(o.blogout1);
												oo.clickpg(o.logout1);
											Thread.sleep(3000);//oo.clickpg(o.usrreg);
											oo.clickpg(o.logout2);
											Thread.sleep(3000);
											driver.get(u.getProperty("url"));
											Thread.sleep(4000);
											if(n.contains(".")){
												oo.textipg(o.loginname, ni);
											}else{
											oo.textpg(o.loginname, ni);
											
											}
											
											
											
													
									Thread.sleep(5000);
									oo.textpg(o.loginpassword, pi);
								
									Thread.sleep(4000);
									oo.clickpg(o.loginsub);
								
									Thread.sleep(3000);
									
								
									if(o.usrreg.isDisplayed()){	 Thread.sleep(3000);
										
										System.out.println("clicked ");}
									else{
										 System.out.println("matster clicked then fmaster ");
											//oo.clickpg(o.master);
											oo.clickpg(o.administratorf);
											Thread.sleep(5000);
											
											
										}
								
									oo.waits(o.bchdep);
									Thread.sleep(3000);
										oo.clickpg(o.chdep);
											 
											 
											 
										Thread.sleep(4000);
										 oo.textpg(o.rolepvsearch, FORMULATIONID);
										 Thread.sleep(4000);
										 
										 
										 List <WebElement> searchusized =driver.findElements(o.bchdepu);
										 System.out.println(searchusize.size()+"size......");
										 if(searchusized.size()==1){	
								
								
									 Thread.sleep(3000);
										oo.clickpg(o.chdepu);
										 
										 Thread.sleep(3000);
									oo.jsdownbye(o.chdepa);
										oo.clickpg(o.chdepa);
										
										Thread.sleep(3000);
							
										
										 oo.textpg(o.Remark, "ok");
										Thread.sleep(2000);
										 oo.textpg(o.pwd, pi);Thread.sleep(2000);
											 
										 oo.clickpg(o.validateuser);Thread.sleep(2000);
										 oo.clickpg(o.ERES);Thread.sleep(2000);
										 oo.clickpg(o.okbtn);Thread.sleep(2000);
										
										
								}else{
									
									System.out.println("no need perform any action initaor not there");
								}
								
								
								
								Thread.sleep(6000);
											 
											 
										
										 
										 
										 
										 }
			

				 
			 
				 
				 
			}else{

			System.out.println("no need perform any action no apr record"); Thread.sleep(3000);



			}


		 
		 
		 
		 
			 
			 }else{
			
			 // rev rej
				 Thread.sleep(4000);
				 oo.jsdownbye(o.chdepr); Thread.sleep(2000);
			oo.clickpg(o.chdepr);	 
				 
			 oo.textpg(o.Remark, "ok");
				Thread.sleep(2000);
				 oo.textpg(o.pwd, p);Thread.sleep(2000);
					 
				 oo.clickpg(o.validateuser);Thread.sleep(2000);
				 oo.clickpg(o.ERES);Thread.sleep(2000);
				 oo.clickpg(o.okbtn);Thread.sleep(2000);
				 
		 
				 
				 // intiaor drop here/////////////////////////////////////////////////////////
				 
			
	
					Thread.sleep(4000);
					oo.waits(o.blogout1);
					oo.clickpg(o.logout1);
				Thread.sleep(3000);//oo.clickpg(o.usrreg);
				oo.clickpg(o.logout2);
				Thread.sleep(3000);
				driver.get(u.getProperty("url"));
				Thread.sleep(4000);
				if(n.contains(".")){
					oo.textipg(o.loginname, ni);
				}else{
				oo.textpg(o.loginname, ni);
				
				}
				
				
				
						
		Thread.sleep(5000);
		oo.textpg(o.loginpassword, pi);
	
		Thread.sleep(4000);
		oo.clickpg(o.loginsub);
	
		Thread.sleep(3000);
	
	
		if(o.usrreg.isDisplayed()){	 Thread.sleep(3000);
			
			System.out.println("clicked ");}
		else{
			 System.out.println("matster clicked then fmaster ");
				//oo.clickpg(o.master);
				oo.clickpg(o.administratorf);
				Thread.sleep(5000);
				
				
			}
	
		oo.waits(o.bchdep);
		Thread.sleep(3000);
			oo.clickpg(o.chdep);
				 
				 
				 
			Thread.sleep(4000);
			 oo.textpg(o.rolepvsearch, FORMULATIONID);
			 Thread.sleep(4000);
			 
			 
			 List <WebElement> searchusized =driver.findElements(o.bchdepu);
			 System.out.println(searchusize.size()+"size......");
			 if(searchusized.size()==1){	
	
	
		 Thread.sleep(3000);
			oo.clickpg(o.chdepu);
			 
			 Thread.sleep(3000);
		oo.jsdownbye(o.chdepa);
			oo.clickpg(o.chdepa);
			
			Thread.sleep(3000);

			
			 oo.textpg(o.Remark, "ok");
			Thread.sleep(2000);
			 oo.textpg(o.pwd, pi);Thread.sleep(2000);
				 
			 oo.clickpg(o.validateuser);Thread.sleep(2000);
			 oo.clickpg(o.ERES);Thread.sleep(2000);
			 oo.clickpg(o.okbtn);Thread.sleep(2000);
			
			
	}else{
		
		System.out.println("no need perform any action initaor not there");
	}
	
	
	
	Thread.sleep(6000);
				 
				 
			
			 
			 
			 
			  
				 
				 
				 
				 
				 
				 
				 
				 
				 
			 }
		 
		 
	}else{
		
		System.out.println("no need perform any action bcs reviwer not matched"); Thread.sleep(3000);
	
		
		
	}



	Thread.sleep(6000);
		//////////////////////////////////////////////////////////////////////////////////////
	
	


	
	
	



			 
		 }catch (Exception e)
		 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
		// TODO: handle exception
		 Logger ll =Logger.getLogger("n role ");
		 ll.trace(e.getMessage());
		 Thread.sleep(4000);
			util oo = PageFactory.initElements(driver, util.class);
		oo.srnsht(driver);

		 	}
		 	test = extent.createTest("adminuseraddf");
			test.createNode("useradd with valid input");
			 Assert.assertTrue(true);
			 Thread.sleep(2000);
				driver.navigate().refresh();Thread.sleep(2000);
		
			}
		 
		 
		 
	
		 
	
	
		 
		 
	
		 
	
	
	
	
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{
		

		System.out.println("testing...");
		 test = extent.createTest("plant clicked");
			test.createNode("click on plant2");
			 Assert.assertTrue(true);

		
		
	}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
