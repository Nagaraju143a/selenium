package dwf;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import dwf.ds;
import pages.agilepg;
import plantpages.*;
import planttests.*;
import test1.base;
import dwf.ds;
public class clcreationdf {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {	System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
}
 

///////////////////////////////////////////////////////////////////////





@DataProvider(name="addbmaster")
public Object[][] adminuseradddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("chlcreation","excel/defwf.xlsx");
	
  return object;	 
}

@Test(dataProvider ="addbmaster", priority=2)

public void adminuseraddf(String n, String p,String clformategrp,String clformatidd, String clformatname, String vv) throws InterruptedException, IOException{



	 try {
		 // import import dwf.ds;
				 Properties u= new Properties();	u.load(ds.class.getResourceAsStream("object.properties"));
				    System.out.println(u.getProperty("url"));
		
				 System.out.println("block master  clickedssssssssssssssssssssssssss");	
				 this.driver=plantbase.driver;
					plantpg o = PageFactory.initElements(driver, plantpg.class);
					util oo = PageFactory.initElements(driver, util.class);
				 Thread.sleep(3000);
			
				 
				 driver.manage().timeouts().implicitlyWait(4000, TimeUnit.SECONDS);
			
				
				Thread.sleep(4000);
				oo.waits(o.blogout1);
				oo.clickpg(o.logout1);
			Thread.sleep(3000);//oo.clickpg(o.usrreg);
			oo.clickpg(o.logout2);
			Thread.sleep(3000);
			driver.get(u.getProperty("url"));
			Thread.sleep(4000);
			if(n.contains(".")){
				oo.textipg(o.loginname, n);
			}else{
			oo.textpg(o.loginname, n);
			
			}
			
			
			
					
		Thread.sleep(5000);
		oo.textpg(o.loginpassword, p);
		
		Thread.sleep(4000);
		oo.clickpg(o.loginsub);
		
		Thread.sleep(3000);
		
		oo.clickpg(o.clm);
		Thread.sleep(3000);
		
		if(o.clc.isDisplayed()){	 Thread.sleep(3000);
		
		System.out.println("clicked ");}
		else{
		 System.out.println("matster clicked then fmaster ");
		
			oo.clickpg(o.clm);
			Thread.sleep(5000);
			
			
		}
		
		oo.waits(o.bclc);
		Thread.sleep(3000);
		
		oo.clickpg(o.clc);	
						 
		Thread.sleep(3000);
		oo.waits(o.baddbm);
		 
		 oo.clickpg(o.addbm);
	
		 
	
		 oo.waits(o.bclformategrp);	 Thread.sleep(3000);	
	 oo.dropdownpg(o.clformategrp, clformategrp);	Thread.sleep(3000);
	 oo.textpg(o.clformatidd, clformatidd);Thread.sleep(3000);	 
	 oo.textpg(o.clformatname, clformatname);Thread.sleep(3000);
	oo.clickpg(o.clreqf);Thread.sleep(3000);
		
	


	 //xpath based on text using
	 //System.out.println(driver.findElement(By.xpath("//label[text()='AREA ID']")).getText()+"txt1");	 
	 //below is checkbox values getting using label
	// System.out.println(driver.findElement(By.xpath("//label[text()='AREA ID']// ../input")).getAttribute("value")+"att");
	
	 
	 List <WebElement> ch =driver.findElements(o.bclreqid);
	 for(WebElement dk : ch){ 
		 double d = Double.parseDouble(vv);
			int i = (int) d;
					 
		 if( dk.getAttribute("value").equals(String.valueOf(i))){
				Thread.sleep(3000);
				
		
		 dk.click();
		 }
		 else if(vv.equals("all")){
			 
			 dk.click();
				 System.out.println(dk.getAttribute("value"));Thread.sleep(3000);
			 
		 }
	 }
		 
	 oo.clickpg(o.clreqf);Thread.sleep(3000);
		 
			
		
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);

	 driver.navigate().refresh();Thread.sleep(2000);
	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
//driver.navigate().refresh();Thread.sleep(2000);
	
		}
	 
	 





@Test(dataProvider ="addbmaster", priority=3)
public void extra(String n, String p,String clformategrp,String clformatidd, String clformatname, String vv) throws InterruptedException, IOException{
		
		
		// only p use here , this method is part of insert
		 try {
			 this.driver=plantbase.driver;
				plantpg o = PageFactory.initElements(driver, plantpg.class);
				util oo = PageFactory.initElements(driver, util.class);
			 Thread.sleep(3000);			 
			 driver.manage().timeouts().implicitlyWait(4000, TimeUnit.SECONDS);


			 Thread.sleep(3000);
				oo.jsdownbye(o.clwf1);Thread.sleep(3000);
				oo.clickpg(o.clwf1);Thread.sleep(2000);
				oo.clickpg(o.clwfi1);Thread.sleep(2000);
				
				
				oo.clickpg(o.clcprv);Thread.sleep(2000);
				oo.clickpg(o.clcsub1);
				 Thread.sleep(3000);
					oo.waits(o.bRemark);
					
					Thread.sleep(2000);
					 oo.textpg(o.Remark, "ok");
					Thread.sleep(2000);
					 oo.textpg(o.pwd, p);
					 oo.clickpg(o.validateuser);	Thread.sleep(4000);
					 
			
					 Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");	 
					 oo.waits(o.bERES);	 	Thread.sleep(2000);
					 oo.clickpg(o.ERES);	System.out.println("tested");
					 oo.waits(o.bokbtn);Thread.sleep(4000); 
					 Logger ll =Logger.getLogger("group master in checklist ");
					 ll.trace(o.uaalert.getText());
				
					 oo.clickpg(o.okbtn);
					 Thread.sleep(4000);


			 
		 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);

	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
		}
	 




	 
	 


















@DataProvider(name="addrev")
public Object[][] addrev() throws IOException{
 util oo = PageFactory.initElements(driver, util.class);
Object[][] object=	oo.getDataFromDataprovider("chcreatdf","excel/defwf.xlsx");

return object;	 
}





@Test(dataProvider ="addrev", priority=4)
public void rev1(String n,String p ,String n1,String p1 ,String ni,String pi , String FORMULATIONID ,String reva, String apr) throws InterruptedException, IOException{
		
		
		
		 try {
			 Properties u= new Properties();	u.load(ds.class.getResourceAsStream("object.properties"));

			 System.out.println("block master  clickedssssssssssssssssssssssssss");	
			 this.driver=plantbase.driver;
				plantpg o = PageFactory.initElements(driver, plantpg.class);
				util oo = PageFactory.initElements(driver, util.class);
			 Thread.sleep(3000);
		
			 
		
		
		
			
			Thread.sleep(4000);
			oo.waits(o.blogout1);
			oo.clickpg(o.logout1);
		Thread.sleep(3000);//oo.clickpg(o.usrreg);
		oo.clickpg(o.logout2);
		Thread.sleep(3000);
		driver.get(u.getProperty("url"));
		Thread.sleep(4000);
		if(n.contains(".")){
			oo.textipg(o.loginname, n);
		}else{
		oo.textpg(o.loginname, n);
		
		}
		
		
		
				
Thread.sleep(3000);
oo.textpg(o.loginpassword, p);

Thread.sleep(4000);
oo.clickpg(o.loginsub);
Thread.sleep(3000);
oo.clickpg(o.clm);
Thread.sleep(3000);

if(o.clc.isDisplayed()){	 Thread.sleep(3000);

System.out.println("clicked ");}
else{
 System.out.println("matster clicked then fmaster ");

	oo.clickpg(o.clm);
	Thread.sleep(5000);
	
	
}

oo.waits(o.bclc);
Thread.sleep(3000);

oo.clickpg(o.clc);	
				 



	Thread.sleep(4000);
		 oo.textpg(o.fsearch1, FORMULATIONID);
		 Thread.sleep(4000);
		 
		
		 List <WebElement> searchusize =driver.findElements(o.bbmfm);
		 System.out.println(searchusize.size()+"size......");
		 if(searchusize.size()==1){	


	 Thread.sleep(3000);
				 
		 oo.clickpg(o.bmfm);
		 Thread.sleep(4000);
		
		 oo.waits(o.bclcdfr);
		 
		 
		 //review//////////////////////////////////////////////////
		 if(reva.equals("raproved")){
		 oo.clickpg(o.clcdfr);
		 Thread.sleep(4000);
		
		 oo.textpg(o.Remark, "ok");
	Thread.sleep(2000);
	 oo.textpg(o.pwd, p);Thread.sleep(2000);
		 
	 oo.clickpg(o.validateuser);Thread.sleep(2000);
	 oo.clickpg(o.ERES);Thread.sleep(2000);
	 oo.clickpg(o.okbtn);Thread.sleep(2000);
		
	



		//Thread.sleep(4000);
		oo.waits(o.blogout1);
		oo.clickpg(o.logout1);
		Thread.sleep(3000);//oo.clickpg(o.usrreg);
		oo.clickpg(o.logout2);
		Thread.sleep(3000);
		driver.get(u.getProperty("url"));
		Thread.sleep(4000);
		if(n.contains(".")){
		oo.textipg(o.loginname, n1);
		}else{
		oo.textpg(o.loginname, n1);

		}



			
		Thread.sleep(5000);
		oo.textpg(o.loginpassword, p1);

		Thread.sleep(4000);
		oo.clickpg(o.loginsub);
		Thread.sleep(4000);
		oo.clickpg(o.clm);
		Thread.sleep(3000);

		if(o.clc.isDisplayed()){	 Thread.sleep(3000);

		System.out.println("clicked ");}
		else{
		 System.out.println("matster clicked then fmaster ");

			oo.clickpg(o.clm);
			Thread.sleep(5000);
			
			
		}

		oo.waits(o.bclc);
		Thread.sleep(3000);

		oo.clickpg(o.clc);	

		Thread.sleep(4000);
		oo.textpg(o.fsearch1, FORMULATIONID);
		Thread.sleep(4000);

	
		 List <WebElement> searchusize1 =driver.findElements(o.bbmfm);
		 System.out.println(searchusize.size()+"size......");
		 if(searchusize1.size()==1){	


	 Thread.sleep(3000);
				 
		 oo.clickpg(o.bmfm);
		 Thread.sleep(4000);
	
			 oo.waits(o.bclcdfr);
			
			 
				
				//review by apr 
			 
			 if(apr.equals("aproved")){
			 
			 oo.clickpg(o.clcdfr);
		Thread.sleep(4000);
		
		 oo.textpg(o.Remark, "ok");
			Thread.sleep(2000);
			 oo.textpg(o.pwd, p1);Thread.sleep(2000);
				 
			 oo.clickpg(o.validateuser);Thread.sleep(2000);
			 oo.clickpg(o.ERES);Thread.sleep(2000);
			 oo.clickpg(o.okbtn);Thread.sleep(2000);
		
		
									 }else{
										 
										 // apr reject 
										 
														
										 Thread.sleep(2000);			 
										 oo.clickpg( o.clcdfrj);
										 oo.textpg(o.Remark, "ok");
											Thread.sleep(2000);
											 oo.textpg(o.pwd, p1);Thread.sleep(2000);
												 
											 oo.clickpg(o.validateuser);Thread.sleep(2000);
											 oo.clickpg(o.ERES);Thread.sleep(2000);
											 oo.clickpg(o.okbtn);Thread.sleep(2000);
										 
										
											 
										 
										 // intiaor drop here/////////////////////////////////////////////////////////
										 
											 
							
											Thread.sleep(4000);
											oo.waits(o.blogout1);
											oo.clickpg(o.logout1);
										Thread.sleep(3000);//oo.clickpg(o.usrreg);
										oo.clickpg(o.logout2);
										Thread.sleep(3000);
										driver.get(u.getProperty("url"));
										Thread.sleep(4000);
										if(n.contains(".")){
											oo.textipg(o.loginname, ni);
										}else{
										oo.textpg(o.loginname, ni);
										
										}
										
										
										
												
								Thread.sleep(5000);
								oo.textpg(o.loginpassword, pi);
							
								Thread.sleep(4000);
								oo.clickpg(o.loginsub);
								Thread.sleep(3000);
								

								oo.clickpg(o.clm);
								Thread.sleep(3000);
								
								if(o.clc.isDisplayed()){	 Thread.sleep(3000);
								
								System.out.println("clicked ");}
								else{
								 System.out.println("matster clicked then fmaster ");
								
									oo.clickpg(o.clm);
									Thread.sleep(5000);								
									
								}
								
								oo.waits(o.bclc);
								Thread.sleep(3000);
								
								oo.clickpg(o.clc);								

								Thread.sleep(4000);
									 oo.textpg(o.fsearch1, FORMULATIONID);
									 Thread.sleep(4000);
										
									 List <WebElement> searchusized =driver.findElements(o.bbmfm);
									 System.out.println(searchusize.size()+"size......");
									 if(searchusized.size()==1){	
							
											
								 Thread.sleep(3000);
									oo.clickpg(o.bmfm);
									 
									 Thread.sleep(3000);
								
								
									oo.clickpg(o.clcddr);
									
									Thread.sleep(3000);
						
									
									 oo.textpg(o.Remark, "ok");
									Thread.sleep(2000);
									 oo.textpg(o.pwd, pi);Thread.sleep(2000);
										 
									 oo.clickpg(o.validateuser);Thread.sleep(2000);
									 oo.clickpg(o.ERES);Thread.sleep(2000);
									 oo.clickpg(o.okbtn);Thread.sleep(2000);
									
									
							}else{
								
								System.out.println("no need perform any action initaor not there");
							}
							
							
							
							Thread.sleep(6000);
								
									 }
		

			 
		}else{

		System.out.println("no need perform any action no apr record"); Thread.sleep(3000);



		}

		 
		 }else{
		
		 // rev rej
			 Thread.sleep(4000);
			 
		oo.clickpg(o.clcdfrj);	 
			 
		 oo.textpg(o.Remark, "ok");
			Thread.sleep(2000);
			 oo.textpg(o.pwd, p);Thread.sleep(2000);
				 
			 oo.clickpg(o.validateuser);Thread.sleep(2000);
			 oo.clickpg(o.ERES);Thread.sleep(2000);
			 oo.clickpg(o.okbtn);Thread.sleep(2000);
			 
	 
			 // intiaor drop here/////////////////////////////////////////////////////////
		 
				Thread.sleep(4000);
				oo.waits(o.blogout1);
				oo.clickpg(o.logout1);
			Thread.sleep(3000);//oo.clickpg(o.usrreg);
			oo.clickpg(o.logout2);
			Thread.sleep(3000);
			driver.get(u.getProperty("url"));
			Thread.sleep(4000);
			if(n.contains(".")){
				oo.textipg(o.loginname, ni);
			}else{
			oo.textpg(o.loginname, ni);
			
			}
			
			
			
					
	Thread.sleep(5000);
	oo.textpg(o.loginpassword, pi);

	Thread.sleep(4000);
	oo.clickpg(o.loginsub);
	Thread.sleep(3000);
	

	oo.clickpg(o.clm);
	Thread.sleep(3000);
	
	if(o.clc.isDisplayed()){	 Thread.sleep(3000);
	
	System.out.println("clicked ");}
	else{
	 System.out.println("matster clicked then fmaster ");
	
		oo.clickpg(o.clm);
		Thread.sleep(5000);								
		
	}
	
	oo.waits(o.bclc);
	Thread.sleep(3000);
	
	oo.clickpg(o.clc);								

	Thread.sleep(4000);
		 oo.textpg(o.fsearch1, FORMULATIONID);
		 Thread.sleep(4000);
			
		 List <WebElement> searchusized =driver.findElements(o.bbmfm);
		 System.out.println(searchusize.size()+"size......");
		 if(searchusized.size()==1){	

				
	 Thread.sleep(3000);
		oo.clickpg(o.bmfm);
		 
		 Thread.sleep(3000);
	
	
		oo.clickpg(o.clcddr);
		
		Thread.sleep(3000);

		
		 oo.textpg(o.Remark, "ok");
		Thread.sleep(2000);
		 oo.textpg(o.pwd, pi);Thread.sleep(2000);
			 
		 oo.clickpg(o.validateuser);Thread.sleep(2000);
		 oo.clickpg(o.ERES);Thread.sleep(2000);
		 oo.clickpg(o.okbtn);Thread.sleep(2000);
		
		
			}else{
				
				System.out.println("no need perform any action initaor not there");
			}
			
			
			
			Thread.sleep(6000);
				
				
					
						 
						 
					 }
				 
				 
			}else{
				
				System.out.println("no need perform any action bcs reviwer not matched"); Thread.sleep(3000);

				
				
			}



			Thread.sleep(6000);
				//////////////////////////////////////////////////////////////////////////////////////









		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);

	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
		}
	 
	 
	 

	 








	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
