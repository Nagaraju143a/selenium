package dwf;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import dwf.ds;
import pages.agilepg;
import plantpages.*;
import planttests.*;
import test1.base;
import dwf.ds;
public class clAssigningdf {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
}
 

///////////////////////////////////////////////////////////////////////





@DataProvider(name="addbmaster")
public Object[][] adminuseradddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("classign","excel/defwf.xlsx");
	
  return object;	 
}


@Test(dataProvider ="addbmaster", priority=2)

public void adminuseraddf(String n, String p, String claname, String claid, String claarea,String claeqp, String cldep, String clsearch ,String Remark) throws InterruptedException, IOException{



	 try {
		 // import import dwf.ds;
				 Properties u= new Properties();	u.load(ds.class.getResourceAsStream("object.properties"));
				    System.out.println(u.getProperty("url"));
		
				 System.out.println("block master  clickedssssssssssssssssssssssssss");	
				 this.driver=plantbase.driver;
					plantpg o = PageFactory.initElements(driver, plantpg.class);
					util oo = PageFactory.initElements(driver, util.class);
				 Thread.sleep(3000);
			
				 
				 driver.manage().timeouts().implicitlyWait(4000, TimeUnit.SECONDS);
			
				
				Thread.sleep(4000);
				oo.waits(o.blogout1);
				oo.clickpg(o.logout1);
			Thread.sleep(3000);//oo.clickpg(o.usrreg);
			oo.clickpg(o.logout2);
			Thread.sleep(3000);
			driver.get(u.getProperty("url"));
			Thread.sleep(4000);
			if(n.contains(".")){
				oo.textipg(o.loginname, n);
			}else{
			oo.textpg(o.loginname, n);
			
			}
			
			
			
					
		Thread.sleep(5000);
		oo.textpg(o.loginpassword, p);
		
		Thread.sleep(4000);
		oo.clickpg(o.loginsub);
		
		Thread.sleep(3000);
		
		oo.clickpg(o.clm);
		Thread.sleep(3000);
		
		if(o.cla.isDisplayed()){	 Thread.sleep(3000);
		
		System.out.println("clicked ");}
		else{
		 System.out.println("matster clicked then fmaster ");
		
			oo.clickpg(o.clm);
			Thread.sleep(5000);
		
			
		}
		
		oo.waits(o.bcla);
		Thread.sleep(3000);
		oo.clickpg(o.cla);	
		 
		Thread.sleep(3000);
		oo.waits(o.baddbm);
		 
		 oo.clickpg(o.addbm);
		 
		 
			Thread.sleep(3000);
			oo.dropdownpg(o.claname, claname);
			Thread.sleep(3000);
			oo.dropdownpg(o.claid, claid);
			Thread.sleep(3000);
			oo.dropdownpg(o.claarea, claarea);	
			Thread.sleep(3000);
			oo.dropdownpg(o.claeqp, claeqp);
			Thread.sleep(3000);
			oo.dropdownpg(o.cldep, cldep);Thread.sleep(3000);
			 oo.clickpg(o.clsub);
			
			oo.jsdownbye(o.clsearch);
			 Thread.sleep(3000);			
			 oo.textpg(o.clsearch, clsearch);
			 
			 Thread.sleep(4000);
			 

			 List <WebElement> searchusize =driver.findElements(By.xpath(o.bcldata1));
				
				System.out.println(searchusize.size()+"size......");
				
				
				
				if(searchusize.size()==1){		Thread.sleep(3000);	
				System.out.println("matched  record");
				
				Thread.sleep(3000);	
				oo.clickpg(o.cldata);
				
					}else{
						System.out.println("not mathed records");
					}
				
				Thread.sleep(4000);
				oo.clickpg(o.clsubmit);
			
				 
		 Thread.sleep(3000);
			oo.waits(o.bRemark);
			
			Thread.sleep(2000);
			 oo.textpg(o.Remark, Remark);
			Thread.sleep(2000);
			 oo.textpg(o.pwd, p);
			 oo.clickpg(o.validateuser);	Thread.sleep(4000);
			 
	
			 Assert.assertEquals(o.ealert.getText(), "Electronic Signature Process Completed Successfully");	 
			 oo.waits(o.bERES);	 	Thread.sleep(2000);
			 oo.clickpg(o.ERES);	System.out.println("tested");
			 oo.waits(o.bokbtn);Thread.sleep(4000); 
			 Logger ll =Logger.getLogger("check list in creation ");
			 ll.trace(o.ealert.getText());
		
			 oo.clickpg(o.okbtn);
			 Thread.sleep(4000);

 
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);

	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
		}
	 
	 
	 
	 






@DataProvider(name="addrev")
public Object[][] addrev() throws IOException{
 util oo = PageFactory.initElements(driver, util.class);
Object[][] object=	oo.getDataFromDataprovider("chadf","excel/defwf.xlsx");

return object;	 
}





@Test(dataProvider ="addrev", priority=3)
public void rev1(String n,String p ,String n1,String p1 ,String ni,String pi , String FORMULATIONID ,String reva, String apr) throws InterruptedException, IOException{
		
		
		
		 try {
			 Properties u= new Properties();	u.load(ds.class.getResourceAsStream("object.properties"));

			 System.out.println("block master  clickedssssssssssssssssssssssssss");	
			 this.driver=plantbase.driver;
				plantpg o = PageFactory.initElements(driver, plantpg.class);
				util oo = PageFactory.initElements(driver, util.class);
			 Thread.sleep(3000);
		
			 
		
		
		
			
			Thread.sleep(4000);
			oo.waits(o.blogout1);
			oo.clickpg(o.logout1);
		Thread.sleep(3000);//oo.clickpg(o.usrreg);
		oo.clickpg(o.logout2);
		Thread.sleep(3000);
		driver.get(u.getProperty("url"));
		Thread.sleep(4000);
		if(n.contains(".")){
			oo.textipg(o.loginname, n);
		}else{
		oo.textpg(o.loginname, n);
		
		}
		
		
		
				
Thread.sleep(3000);
oo.textpg(o.loginpassword, p);

Thread.sleep(4000);
oo.clickpg(o.loginsub);
Thread.sleep(3000);

oo.clickpg(o.clm);
Thread.sleep(3000);

if(o.cla.isDisplayed()){	 Thread.sleep(3000);

System.out.println("clicked ");}
else{
 System.out.println("matster clicked then fmaster ");

	oo.clickpg(o.clm);
	Thread.sleep(5000);

	
}

oo.waits(o.bcla);
Thread.sleep(3000);
oo.clickpg(o.cla);	




	Thread.sleep(4000);
		 oo.textpg(o.fsearch1, FORMULATIONID);
		 Thread.sleep(4000);
		 
		
		 List <WebElement> searchusize =driver.findElements(o.bclassdiv);
		 System.out.println(searchusize.size()+"size......");
		 if(searchusize.size()==1){	


	 Thread.sleep(3000);
				 
		 oo.clickpg(o.classdiv);
		 Thread.sleep(4000);
		
		 oo.waits(o.bclasrev);
		 
		 
		 //review//////////////////////////////////////////////////
		 if(reva.equals("raproved")){
		 oo.clickpg(o.clasrev);
		 Thread.sleep(4000);
		
		 oo.textpg(o.Remark, "ok");
	Thread.sleep(2000);
	 oo.textpg(o.pwd, p);Thread.sleep(2000);
		 
	 oo.clickpg(o.validateuser);Thread.sleep(2000);
	 oo.clickpg(o.ERES);Thread.sleep(2000);
	 oo.clickpg(o.okbtn);Thread.sleep(2000);
		




		//Thread.sleep(4000);
		oo.waits(o.blogout1);
		oo.clickpg(o.logout1);
		Thread.sleep(3000);//oo.clickpg(o.usrreg);
		oo.clickpg(o.logout2);
		Thread.sleep(3000);
		driver.get(u.getProperty("url"));
		Thread.sleep(4000);
		if(n.contains(".")){
		oo.textipg(o.loginname, n1);
		}else{
		oo.textpg(o.loginname, n1);

		}



			
		Thread.sleep(5000);
		oo.textpg(o.loginpassword, p1);

		Thread.sleep(4000);
		oo.clickpg(o.loginsub);
		Thread.sleep(4000);

oo.clickpg(o.clm);
Thread.sleep(3000);

if(o.cla.isDisplayed()){	 Thread.sleep(3000);

System.out.println("clicked ");}
else{
 System.out.println("matster clicked then fmaster ");

	oo.clickpg(o.clm);
	Thread.sleep(5000);

	
}

oo.waits(o.bcla);
Thread.sleep(3000);
oo.clickpg(o.cla);	



		Thread.sleep(4000);
		oo.textpg(o.fsearch1, FORMULATIONID);
		Thread.sleep(4000);

	
		 List <WebElement> searchusize1 =driver.findElements(o.bclassdiv);
		 System.out.println(searchusize.size()+"size......");
		 if(searchusize1.size()==1){	


	 Thread.sleep(3000);
				 
		 oo.clickpg(o.classdiv);
		 Thread.sleep(4000);
		
			 oo.waits(o.bclasrev);
			
			 
				
				//review by apr 
			 
			 if(apr.equals("aproved")){
			 
			 oo.clickpg(o.clasrev);
		Thread.sleep(4000);
		
		 oo.textpg(o.Remark, "ok");
			Thread.sleep(2000);
			 oo.textpg(o.pwd, p1);Thread.sleep(2000);
				 
			 oo.clickpg(o.validateuser);Thread.sleep(2000);
			 oo.clickpg(o.ERES);Thread.sleep(2000);
			 oo.clickpg(o.okbtn);Thread.sleep(2000);
		
		
									 }else{
										 
										 // apr reject 
										 
													
										 Thread.sleep(2000);			 
										 oo.clickpg( o.clasrej);
										 oo.textpg(o.Remark, "ok");
											Thread.sleep(2000);
											 oo.textpg(o.pwd, p1);Thread.sleep(2000);
												 
											 oo.clickpg(o.validateuser);Thread.sleep(2000);
											 oo.clickpg(o.ERES);Thread.sleep(2000);
											 oo.clickpg(o.okbtn);Thread.sleep(2000);
										 
										
											 
										 
										 // intiaor drop here/////////////////////////////////////////////////////////
										 
									
							
											Thread.sleep(4000);
											oo.waits(o.blogout1);
											oo.clickpg(o.logout1);
										Thread.sleep(3000);//oo.clickpg(o.usrreg);
										oo.clickpg(o.logout2);
										Thread.sleep(3000);
										driver.get(u.getProperty("url"));
										Thread.sleep(4000);
										if(n.contains(".")){
											oo.textipg(o.loginname, ni);
										}else{
										oo.textpg(o.loginname, ni);
										
										}
										
										
										
												
								Thread.sleep(5000);
								oo.textpg(o.loginpassword, pi);
							
								Thread.sleep(4000);
								oo.clickpg(o.loginsub);
								Thread.sleep(3000);
							
								oo.clickpg(o.clm);
								Thread.sleep(3000);

								if(o.cla.isDisplayed()){	 Thread.sleep(3000);

								System.out.println("clicked ");}
								else{
								 System.out.println("matster clicked then fmaster ");

									oo.clickpg(o.clm);
									Thread.sleep(5000);

									
								}

								oo.waits(o.bcla);
								Thread.sleep(3000);
								oo.clickpg(o.cla);		

							

								Thread.sleep(4000);
									 oo.textpg(o.fsearch1, FORMULATIONID);
									 Thread.sleep(4000);
										
									 List <WebElement> searchusized =driver.findElements(o.bclassdiv);
									 System.out.println(searchusize.size()+"size......");
									 if(searchusized.size()==1){	
							
										
								 Thread.sleep(3000);
									oo.clickpg(o.classdiv);
									 
									 Thread.sleep(3000);
								
								
									oo.clickpg(o.clad);
									
									Thread.sleep(3000);
						
									
									 oo.textpg(o.Remark, "ok");
									Thread.sleep(2000);
									 oo.textpg(o.pwd, pi);Thread.sleep(2000);
										 
									 oo.clickpg(o.validateuser);Thread.sleep(2000);
									 oo.clickpg(o.ERES);Thread.sleep(2000);
									 oo.clickpg(o.okbtn);Thread.sleep(2000);
									
									
							}else{
								
								System.out.println("no need perform any action initaor not there");
							}
							
							
							
							Thread.sleep(6000);
										 
										 
									
									 
									 
									 
									 }
		

			 
		 
			 
			 
		}else{

		System.out.println("no need perform any action no apr record"); Thread.sleep(3000);



		}


	 
	 
	 
	 
		 
		 }else{
		
		 // rev rej
			 Thread.sleep(4000);
			
		oo.clickpg(o.clasrej);	 
			 
		 oo.textpg(o.Remark, "ok");
			Thread.sleep(2000);
			 oo.textpg(o.pwd, p);Thread.sleep(2000);
				 
			 oo.clickpg(o.validateuser);Thread.sleep(2000);
			 oo.clickpg(o.ERES);Thread.sleep(2000);
			 oo.clickpg(o.okbtn);Thread.sleep(2000);
			 
	 
			 // intiaor drop here/////////////////////////////////////////////////////////
			 

				Thread.sleep(4000);
				oo.waits(o.blogout1);
				oo.clickpg(o.logout1);
			Thread.sleep(3000);//oo.clickpg(o.usrreg);
			oo.clickpg(o.logout2);
			Thread.sleep(3000);
			driver.get(u.getProperty("url"));
			Thread.sleep(4000);
			if(n.contains(".")){
				oo.textipg(o.loginname, ni);
			}else{
			oo.textpg(o.loginname, ni);
			
			}
			
			
			
					
	Thread.sleep(5000);
	oo.textpg(o.loginpassword, pi);

	Thread.sleep(4000);
	oo.clickpg(o.loginsub);
	Thread.sleep(3000);

	oo.clickpg(o.clm);
	Thread.sleep(3000);

	if(o.cla.isDisplayed()){	 Thread.sleep(3000);

	System.out.println("clicked ");}
	else{
	 System.out.println("matster clicked then fmaster ");

		oo.clickpg(o.clm);
		Thread.sleep(5000);

		
	}

	oo.waits(o.bcla);
	Thread.sleep(3000);
	oo.clickpg(o.cla);		



	Thread.sleep(4000);
		 oo.textpg(o.fsearch1, FORMULATIONID);
		 Thread.sleep(4000);
			
		 List <WebElement> searchusized =driver.findElements(o.bclassdiv);
		 System.out.println(searchusize.size()+"size......");
		 if(searchusized.size()==1){	

			
	 Thread.sleep(3000);
		oo.clickpg(o.classdiv);
		 
		 Thread.sleep(3000);
	
	
		oo.clickpg(o.clad);
		
		Thread.sleep(3000);

		
		 oo.textpg(o.Remark, "ok");
		Thread.sleep(2000);
		 oo.textpg(o.pwd, pi);Thread.sleep(2000);
			 
		 oo.clickpg(o.validateuser);Thread.sleep(2000);
		 oo.clickpg(o.ERES);Thread.sleep(2000);
		 oo.clickpg(o.okbtn);Thread.sleep(2000);
		
		 }else{
				
				System.out.println("no need perform any action initaor not there");
			}



			Thread.sleep(6000);
					
						 
						 
					 }
				 
				 
			}else{
				
				System.out.println("no need perform any action bcs reviwer not matched"); Thread.sleep(3000);

				
				
			}



			Thread.sleep(6000);
				//////////////////////////////////////////////////////////////////////////////////////









		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);

	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
		}
	 
	 
	 

	 








	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
