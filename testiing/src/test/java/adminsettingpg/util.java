package adminsettingpg;

import java.awt.AWTException;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.google.common.io.Files;

import plantpages.plantpg;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import java.time.Duration;
import searchmaster.*;
public class util {

	WebDriver driver;

	public util(WebDriver driver) {
		this.driver = driver;

		// driver from tc assigned to webdriver driver by using this.driver here
		// assigned to our WebDriver driver;
	}
	
	


	
	public static String getScreenshot(WebDriver driver, String screenshotName) throws IOException {
		
		// here  screenshotName means failed method name get
		  String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
		  
		  
		  
		  
		  TakesScreenshot ts = (TakesScreenshot) driver;
		  File source = ts.getScreenshotAs(OutputType.FILE);		  
		  // after execution, you could see a folder "FailedTestsScreenshots" under src folder
		  String destination = System.getProperty("user.dir") + "/screenshot/" + screenshotName + dateName + ".png";
		  File finalDestination = new File(destination);
		  Files.copy(source, finalDestination);
		  return destination;
		  
		  
		  
		 }

public void ddd() throws IOException{
	Object[][] object = null;  
	File f1 = new File("");
	FileInputStream fis = new FileInputStream(f1);
	Workbook book = new XSSFWorkbook(fis);
	Sheet sh1= book.getSheet("sh11");
	int rc= sh1.getLastRowNum()-sh1.getFirstRowNum();
	int cc= sh1.getRow(0).getLastCellNum();
	object = new Object[rc][cc];
	
	
	for(int i=0; i<rc;i++){
		Row row= sh1.getRow(i+1);
		
		
		
	}
	
}
	
	public Object[][] getDataFromDataprovider(String sheetnameenter,String fileName1) throws IOException {

		//data provider function name base class in here not be same if same getting error
	Object[][] object = null;     
	 	
	File file1 =	new File(System.getProperty("user.dir")+"/"+fileName1);   
	FileInputStream inputStream = new FileInputStream(file1);
	Workbook guru99Workbook = null;

	String fileExtensionName = fileName1.substring(fileName1.indexOf("."));

	if(fileExtensionName.equals(".xlsx")){  
	guru99Workbook = new XSSFWorkbook(inputStream);
	}      
	else if(fileExtensionName.equals(".xls")){      		
		guru99Workbook = new HSSFWorkbook(inputStream);
	}     



	Sheet  guru99Sheet1 = guru99Workbook.getSheet(sheetnameenter);

	int rowCount = guru99Sheet1.getLastRowNum()-guru99Sheet1.getFirstRowNum();

	System.out.println(rowCount);
	//object = new Object[rowCount][5];
	int colCount = guru99Sheet1.getRow(0).getLastCellNum();
		object = new Object[rowCount][colCount];
		
		
		
		
		
	for (int i = 0; i < rowCount; i++) {

		Row row = guru99Sheet1.getRow(i+1);
		
		for (int j = 0; j < row.getLastCellNum(); j++) {     			
				
				if(row.getCell(j)==null || row.getCell(j).toString().equals("")) {
				
				
			}
			else {
				object[i][j] = row.getCell(j).toString();    
			
			
			}         	 	
	}

	}
	
	
	
	
	

	  return object;	 
	
		
		
	}
	
	

	
	
	
	public void submenuselect(By userName2, WebElement menu, WebElement user) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated((By) userName2));
		Actions actions = new Actions(driver);
		Thread.sleep(5000);
		actions.moveToElement(menu).perform();
		Thread.sleep(2000);
		user.click();
	}
	

	public void waits(By userName2) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated((By) userName2));
		System.out.println("............. wait for ............");
	

	}
	
	public void robo( ) throws AWTException, InterruptedException {	
	Robot robot = new Robot();  // Robot class throws AWT Exception	
	Thread.sleep(2000); // Thread.sleep throws InterruptedException	
	robot.keyPress(KeyEvent.VK_DOWN);  // press arrow down key of keyboard to navigate and select Save radio button	

	Thread.sleep(2000);  // sleep has only been used to showcase each event separately	
	robot.keyPress(KeyEvent.VK_TAB);	
	Thread.sleep(2000);	
	robot.keyPress(KeyEvent.VK_TAB);	
	Thread.sleep(2000);	
	robot.keyPress(KeyEvent.VK_TAB);	
	Thread.sleep(2000);	
	robot.keyPress(KeyEvent.VK_ENTER);			
	}
	

	
	
	
	public void srnsht(WebDriver driver ) throws IOException   {

		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		DateFormat df = new SimpleDateFormat("ddMMyy HHmmss");
	       Date dateobj = new Date();
	       String s= df.format(dateobj);
	      // System.out.println(df.format(dateobj));
		Files.copy(scrFile,	new File(System.getProperty("user.dir") + "/screenshot/" +s+ ".png"));
		
	
	
	
	}
	
		

	public void textpg(WebElement name2, String n) {

		//System.out.println(name2);
	

		name2.sendKeys(n);

		}
	
	

	public void clearpg(WebElement name2) {

		name2.clear();
	}

	public void textipg(WebElement name2, String n) {

		double d = Double.parseDouble(n);
		int i = (int) d;
		name2.sendKeys(String.valueOf(i));

	}

	public void clickpg(WebElement name2) {
		name2.click();
	
	}
	
	public void dropdownpgint(WebElement ee, String n) {
		Select drop1 = new Select(ee);

		double d = Double.parseDouble(n);
		int i = (int) d;
		
		
		drop1.selectByVisibleText(String.valueOf(i));
	}
	
	

	public void dropdownpg(WebElement ee, String n) {
		Select drop1 = new Select(ee);

		drop1.selectByVisibleText(n);		
	}

	public void dropdowndspg(WebElement ee) {

		Select drop3 = new Select(ee);
		drop3.deselectAll();
	}

	public void tabrobopg(WebElement ee) {
		WebElement we = ee;
		we.sendKeys(Keys.TAB);

	}





	public void enterrobopg(WebElement ee) {
		WebElement wew = ee;

		wew.sendKeys(Keys.ENTER);
	}
	
	public void jsclickpg(WebElement ee) {
		
		
		WebElement element = 	ee;
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
	  	
	}
	

	public void screenshotpg(WebElement ee) throws IOException, InterruptedException {
		
WebElement ele1=ee;
//driver.findElement(By.xpath("//img[contains(@src,'images/Sofththink Logo.PNG')]"));// in our project footer also getting so eliminated 
		
        
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
  jsExecutor.executeScript( "arguments[0].parentNode.removeChild(arguments[0])", ele1);
  //used to eliminate footer for screen shot case	
		Thread.sleep(2000);
		
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		
		
		if(System.getProperty("user.dir").contains("C:\\") ||  System.getProperty("user.dir").contains("D:\\")|| System.getProperty("user.dir").contains("E:\\")   )
			 
		{
			Files.copy(scrFile,	new File(System.getProperty("user.dir") + "/screenshot/" + System.currentTimeMillis() + ".png"));
			System.out.println("screnshot taken");
		}
		else {
		
			Files.copy(scrFile,	new File("/home/Images/ctts"+System.currentTimeMillis()+".png"));
			System.out.println("screnshot taken");
		// File("/home/Images/ctts"+System.currentTimeMillis()+".png"));
		}
	}

	public void screenshotdivpg(WebElement ee, WebElement asd) throws IOException, InterruptedException
	{
		
		//bellow use for ctts footer remove 
		//WebElement asd= driver.findElement(By.id("footer"));
		 JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		  jsExecutor.executeScript( "arguments[0].parentNode.removeChild(arguments[0])", asd);
		  //used to eliminate footer for screen shot case

			Thread.sleep(2000);
		 File scrFile1 = ((TakesScreenshot)ee).getScreenshotAs(OutputType.FILE);
           //The below method will save the screen shot in d drive with name "screenshot.png"		 
			
			if(System.getProperty("user.dir").contains("C:\\") ||  System.getProperty("user.dir").contains("D:\\")|| System.getProperty("user.dir").contains("E:\\")   )
				 
			{
				Files.copy(scrFile1,	new File(System.getProperty("user.dir") + "/excel/" + System.currentTimeMillis() + ".png"));
				System.out.println("screnshot taken");
				
			}
			else {
				Files.copy(scrFile1,	new File(System.getProperty("user.dir") + "/excel/" + System.currentTimeMillis() + ".png"));
				
				//Files.copy(scrFile1,	new File("/home/Images/ctts/"+System.currentTimeMillis()+".png"));
				System.out.println("screnshot taken");
			
			}
		 
		
			
	}
	
	
		
	
	public void alertpg() throws InterruptedException {
		//if ((ExpectedConditions.alertIsPresent()) == null) {
			System.out.println((ExpectedConditions.alertIsPresent()).apply(driver));
			
			if((ExpectedConditions.alertIsPresent()).apply(driver)==null) {
			System.out.println("alert was not present");
		} else {
			System.out.println("alert was present");

			// Switching to Alert
			Alert alert = driver.switchTo().alert();

			// Capturing alert message.
			String alertMessage = driver.switchTo().alert().getText();

			// Displaying alert message
			System.out.println(alertMessage);
			Thread.sleep(5000);

			// Accepting alert
			alert.accept();
		}
	}
	
	
	public void gettextfieldpg(WebElement ee) {

		System.out.println(ee.getAttribute("value"));

	}
	
	
	public void jsloadpg() throws InterruptedException {	      	
		for (int i=0; i<3; i++){ 
          
             System.out.println("wait for agile4 secc");        
            JavascriptExecutor js = (JavascriptExecutor) driver;
            if (js.executeScript("return document.readyState").toString().equals("complete")){ 
                  System.out.println("Page Is loaded.");
                      return;   
            }
		}
	
}
	

	
	public void calenderpg( By userName2, String value) throws InterruptedException {

		// userName2 here is just element 
		
		System.out.println("start5");
		//java.util.List<WebElement> allDates = driver.findElements((this.getObject(p, objectName, objectType)));
		java.util.List<WebElement> allDates = driver.findElements((By) userName2);
		for (WebElement ele : allDates) {
			
			String date = ele.getText();
			System.out.println(date);

			String s2 = String.valueOf(value);
			System.out.println("nnnnnnnnnnnnnnnnnnnnnnnnnnnnna");
			Thread.sleep(2000);
			System.out.println("objecttostring" + s2);
			double d = Double.parseDouble(s2);
			int i = (int) d;
			Thread.sleep(2000);
			if (date.equalsIgnoreCase(String.valueOf(i))) {
				ele.click();
				System.out.println("date is clicked");
				break;
			}

		}
		Thread.sleep(5000);

	}

	
		
public void jsdownby(){
	((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1000)");
}
public void jsdownbyme(){
	((JavascriptExecutor) driver).executeScript("window.scrollBy(0,100)");
}
public void jsdownbye(WebElement ee){
	
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ee);
}

///////////////////////////////////
	
	public void tablesearchpg(String b, String r, String value, String su) throws InterruptedException {

		System.out.println("table");

		List rows = driver.findElements(By.xpath(r));

		System.out.println("No of rows areeeeeeeeeee : " + rows.size());
		int searchCount = 0;
		for (int i = 1; i <= rows.size(); i++) {

			String a = b + "tr[" + i + "]";////////////////////
			// System.out.println(a+"rowsssssssssssssssssssssssssssssssssss");
			WebElement tableRow = driver.findElement(By.xpath(a));

		/*	String aa = b + "tr[" + i + "]/td";
			List colu = driver.findElements(By.xpath(aa));
			 System.out.println("No of columns are : " + colu.size());
*/
			String aaa = b + "tr[" + i + "]/td[1]";
			WebElement cellIneed = tableRow.findElement(By.xpath(aaa));

			
			
			String valueIneed = cellIneed.getText();
			System.out.println(valueIneed);
			Thread.sleep(6000);
			if (valueIneed.equals(value)) {
				System.out.println("record is PRESENT AT ");
				System.out.println(i);
				System.out.println("Cell value is : " + valueIneed);
				Thread.sleep(6000);
				if (su.equals("update")) {
					String id_of_element = b + "tr[" + i + "]/td[1]/a";///////////////
					//html/body/div[4]/div/div[2]/div[2]/ul/li[2]/div/div[2]/div/table/tbody/tr[1]/td[2]/div/a
					WebElement elements = driver.findElement(By.xpath(id_of_element));

					// used for scroll down until elemnt is visible
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elements);
					
					
					elements.click();
				}

				// System.out.println(elements.getText());

				break;

			} else {
				searchCount++;

			}

		}

		if (searchCount == rows.size()) {
			System.out.println("no such backlog present");
		}

	}

	/*
	 * 
	 * public void name(String n) {
	 * 
	 * driver.findElement(userName).sendKeys(""); } public void name(String n) {
	 * name.sendKeys(n); } public void pass(String p) { password.sendKeys(p); }
	 * public void save() { submit.click(); }
	 */
	
	
	
	
	// in a table single record present case
	public void tablesearchpgs1(String zz, String value, String su) throws InterruptedException {
	
		String  dd =	driver.findElement(By.xpath("//*[@id=\"example1_info\"]")).getText();
		System.out.println(dd+"hhhhhhhhhhhhhhhhhhh");
		 
		
		if(dd.contains("Showing 0 to 0 of 0 entries"))
		{
			System.out.println("no records");
		}else {		
		
			WebElement cellIneed = driver.findElement(By.xpath(zz));
			
			 
			String valueIneed = cellIneed.getText();
			System.out.println(valueIneed);
			Thread.sleep(6000);
			if (valueIneed.equals(value)) {
				System.out.println("record is PRESENT AT ");
				
				System.out.println("Cell value is : " + valueIneed);
				Thread.sleep(6000);
				if (su.equals("update")) {
					
					WebElement elements = driver.findElement(By.xpath(zz));

				
					//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elements);
					
					
				driver.findElement(By.xpath("//*[@id=\"link60\"]")).click();System.out.println("fffffffffff");
				}

			

				

			}
			}

		

	}
	
/*	role based privilieges */
	
	
	public void tablesearchpg1(String b,String c, String value, String su) throws InterruptedException {

		System.out.println("table");
		String ac = b + "li";
		System.out.println(ac);
		List rows = driver.findElements(By.xpath(ac));

		System.out.println("No of rows areeeeeeeeeee : " + rows.size());
		int searchCount = 0;
		for (int i = 1; i <= rows.size(); i++) {

			String a = b + "li[" + i + "]";////////////////////
			
			WebElement tableRow = driver.findElement(By.xpath(a));

		
			String aaa = b + "li[" + i + "]"+c;
			System.out.println(aaa+"dddddddddddddddddddddddddddddddddddddddddddddddddddd");
			WebElement cellIneed = tableRow.findElement(By.xpath(aaa));
			
		
			

			String valueIneed = cellIneed.getAttribute("value");
			System.out.println(valueIneed+"data");
			Thread.sleep(6000);
			if (valueIneed.equals(value)) {
				System.out.println("record is PRESENT AT ");
				System.out.println(i);
				System.out.println("Cell value is : " + valueIneed);
				Thread.sleep(6000);
				System.out.println(aaa+"rigt ele...................");
				
					//update means if not selected then select heading means insert edit list checking
							if(driver.findElement(By.xpath(aaa)).isSelected()){
								System.out.println("alredy selected ");
								
										if(su.equals("selectlist")){
											//System.out.println("now am selecting   list");
										 String cl ="/div/div[2]/div[1]/input";
											String aaacl = b + "li[" + i + "]"+cl;
											if(driver.findElement(By.xpath(aaacl)).isSelected()){
												System.out.println("alerdy   list selected");
											}else{
												System.out.println("now am selecting extra value  list");
											driver.findElement(By.xpath(aaacl)).click();}
										}
										if(su.equals("selectinsert")){
											//System.out.println("now am selecting   list");
										 String cl ="/div/div[2]/div[2]/input";
											String aaacl = b + "li[" + i + "]"+cl;
											if(driver.findElement(By.xpath(aaacl)).isSelected()){
												System.out.println("alerdy   insert selected");
											}else{
												System.out.println("now am selecting extra value  insert");
											driver.findElement(By.xpath(aaacl)).click();}
										}
								
							
							}else if(su.equals("selectall")){
							System.out.println("now am selecting all");
							driver.findElement(By.xpath(aaa)).click();
						}
						else if(su.equals("selectlist")){
							System.out.println("now am selecting only  list");
							 String cl ="/div/div[2]/div[1]/input";
								String aaacl = b + "li[" + i + "]"+cl;
							driver.findElement(By.xpath(aaacl)).click();
						}
						else if(su.equals("selectinsert")){
							System.out.println("now am selecting  only insert");
							 String ci ="/div/div[2]/div[2]/input";
								String aaaci = b + "li[" + i + "]"+ci;
							driver.findElement(By.xpath(aaaci)).click();
						}
							
						else if(su.equals("selectedit")){
							System.out.println("now am selecting  only  edit");
							 String ce ="/div/div[2]/div[3]/input";
								String aaace = b + "li[" + i + "]"+ce;
							driver.findElement(By.xpath(aaace)).click();
						}
						else if(su.equals("selecteditANDinsert")){
							System.out.println("now am selecting   edit and insert");
							 String ce ="/div/div[2]/div[3]/input";
								String aaace = b + "li[" + i + "]"+ce;
							driver.findElement(By.xpath(aaace)).click();
							
							 String ci ="/div/div[2]/div[2]/input";
								String aaaci = b + "li[" + i + "]"+ci;
							driver.findElement(By.xpath(aaaci)).click();
						}
				
			
				
				

			
				break;

			} else {
				searchCount++;

			}

		}
System.out.println(searchCount+"..................."+rows.size());
		if (searchCount == rows.size()) {
			
			
			if( searchCount<1){
				System.out.println("no such record present");
			}else{
			}
		}

	}
	
	
	public String gettextdddddddd(WebElement dd) {
		dd.getText();
		String a=	dd.getText();
		return a;
	}
	
	
public WebElement gettext1(WebElement dd) {
	dd.getText();
	String a=	dd.getText();
	System.out.println(a+"tc existing");
	return dd;
}
public void gettext(WebElement dd) {
	dd.getText();
	String a=	dd.getText();
	System.out.println(a+"tc existing");
}


public void attr(WebElement username_tooltip, String tc) throws InterruptedException {
			String actualTooltip =username_tooltip.getAttribute("value");
		//System.out.println(actualTooltip+"toooooooooool");
	if((actualTooltip==null || actualTooltip.equals(""))) {							
        System.out.println(tc);Thread.sleep(2000);
        
        
	}
	
			
		}
		
	
	
	

	public void flu(By userName2) {
		
		 // WebElement name22=   driver.findElement(By.xpath("/html/body/table/tbody/tr/td[1]/div/div/ol/li[2]/a"));
	  Wait<WebDriver> fluentWait = new FluentWait<WebDriver>(driver)

			    .withTimeout(70, TimeUnit.SECONDS)

			    .pollingEvery(5, TimeUnit.SECONDS)

			    .ignoring(NoSuchElementException.class);

			  WebElement content = fluentWait.until(new Function<WebDriver, WebElement>() {
				  
			   public WebElement apply(WebDriver driver) {
			
				
				  // function in  WebElement passing then here using case getting error 
			    return  driver.findElement((By) userName2);
			   }
			   
			   
			  });
			
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	////////////////////main sprint//////subsprint////
	public void tablesearchmsp(String b,String c, String value, String su) throws InterruptedException {

		System.out.println("table");
		String ac = b + "div";
		System.out.println(ac);
		List rows = driver.findElements(By.xpath(ac));

		System.out.println("No of rows areeeeeeeeeee : " + rows.size());
		int searchCount = 0;
		for (int i = 1; i <= rows.size(); i++) {

			String a = b + "div[" + i + "]";////////////////////
			
			WebElement tableRow = driver.findElement(By.xpath(a));

			
			String aaa = b + "div[" + i + "]"+c;
			WebElement cellIneed = tableRow.findElement(By.xpath(aaa));
			
			
			
			

			String valueIneed = cellIneed.getText();
			System.out.println(valueIneed+"data");
			Thread.sleep(6000);
			if (valueIneed.equals(value)) {
				
				System.out.println("record is PRESENT AT ");
				System.out.println(i);
				System.out.println("Cell value is : " + valueIneed);
				Thread.sleep(6000);
				if (su.equals("update")) {
					
					
					String id_of_element =b + "div[" + i + "]"+c;
					WebElement elements = driver.findElement(By.xpath(id_of_element));

				
					//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elements);
					
					
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", elements);
					
					
					
					
				}



				break;

			} else {
				searchCount++;

			}

		}
System.out.println(searchCount+"..................."+rows.size());
		if (searchCount == rows.size()) {
			
			
			if( searchCount<1){
				System.out.println("no such record present");
			}else{
			}
		}

	}

	
	
	
	
	
	
	//*[@id="datatable-responsive"]/tbody/
	
	
	public void usersearch(String b, String value) throws InterruptedException {

		System.out.println("table");
		String ac = b + "tr";
		System.out.println(ac);
		List rows = driver.findElements(By.xpath(ac));

		System.out.println("No of rows areeeeeeeeeee : " + rows.size());
		int searchCount = 0;
		for (int i = 1; i <= rows.size(); i++) {

			String a = b + "tr[" + i + "]";////////////////////
			
			WebElement tableRow = driver.findElement(By.xpath(a));

			String c1="/td[1]";
			String aaa = b + "tr[" + i + "]"+c1;
			String c2="/td[2]";
			String aaab = b + "tr[" + i + "]"+c2;
			System.out.println(aaa+"am writing records of xpaths ");
			WebElement cellIneed1 = tableRow.findElement(By.xpath(aaa));
			
			WebElement cellIneed2 = tableRow.findElement(By.xpath(aaab));
	
			String valueIneed2 = cellIneed2.getText();
					
			String valueIneed1 = cellIneed1.getText();
			System.out.println(value);
		System.out.println(valueIneed1);System.out.println(valueIneed2);
			Thread.sleep(6000);
			if (valueIneed2.equals(value)) {
				
				System.out.println("record is PRESENT AT ");
				System.out.println(i);
		
				Thread.sleep(6000);
		
				System.out.println("user name present ");
				///////////////


				break;

			} else if (valueIneed1.equals(value)) {
				
				System.out.println("record is PRESENT AT ");
				System.out.println(i);
		
				Thread.sleep(6000);
		
				System.out.println("user id present ");
				////////////


				break;

			} else {
				searchCount++;

			}

		}
System.out.println(searchCount+"..................."+rows.size());
		if (searchCount == rows.size()) {
			
			
			if( searchCount<1){
				System.out.println("no such record present");
			}else{
			}
		}

	}

	

	
	
	public void ulocation(String b,String ud, String value, String c1, String c2) throws InterruptedException {
		//*[@id='datatable-responsives']/tbody/tr ac
		
		//*[@id='datatable-responsives']/tbody/tr a... tr
		
		
		System.out.println("table");
		String ac = b + "tr";
		System.out.println(ac);
		List rows = driver.findElements(By.xpath(ac));

		System.out.println("No of rows areeeeeeeeeee : " + rows.size());
		int searchCount = 0;
		for (int i = 1; i <= rows.size(); i++) {

			String a = b + "tr[" + i + "]";
			
			WebElement tableRow = driver.findElement(By.xpath(a));

			
			String aaa = b + "tr[" + i + "]"+c1;
	
			String aaab = b + "tr[" + i + "]"+c2;
			
			System.out.println(aaa+"am writing records of xpaths ");
			WebElement cellIneed1 = tableRow.findElement(By.xpath(aaa));
			
			WebElement cellIneed2 = tableRow.findElement(By.xpath(aaab));
	
			String valueIneed2 = cellIneed2.getText();
					
			String valueIneed1 = cellIneed1.getText();
			System.out.println(value);
		System.out.println(valueIneed1);System.out.println(valueIneed2);
			Thread.sleep(6000);
			if (valueIneed2.equals(value)) {
				
				System.out.println("record is PRESENT AT ");
				System.out.println(i);
		
				Thread.sleep(6000);
		
				System.out.println("user name present ");
				///////////////
				
				
		
				if(ud.equals("scg")){
					String c7="/td[3]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[4]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					////////driver.findElement(By.xpath(cl)).click();
					break;}}
				

				if(ud.equals("ucg")){
					String c7="/td[3]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[4]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					driver.findElement(By.xpath(cl)).click();
					break;}}
				

				if(ud.equals("sca")){
					String c7="/td[1]/a";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[7]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					////////driver.findElement(By.xpath(cl)).click();
					break;}}
				
				
				if(ud.equals("uca")){
					String c7="/td[1]/a";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[7]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					driver.findElement(By.xpath(cl)).click();
					break;}}
				if(ud.equals("scpa")){
					String c7="/td[1]/a/u";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[7]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					////////driver.findElement(By.xpath(cl)).click();
					break;}}
				
				if(ud.equals("ucpa")){
					String c7="/td[1]/a/u";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[7]";
					String cl2 = b + "tr[" + i + "]"+c8;
					//here not only active other status case
					if(!driver.findElement(By.xpath(cl2)).getText().equals("")){
						Logger ll =Logger.getLogger("record present ");
						ll.trace("reocrd present ");
					driver.findElement(By.xpath(cl)).click();
					break;}}
				
				if(ud.equals("scc")){
					String c7="/td[4]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[5]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					////////driver.findElement(By.xpath(cl)).click();
					break;}}
				
				if(ud.equals("ucc")){
					String c7="/td[4]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[5]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
						driver.findElement(By.xpath(cl)).click();
					break;}}
				
				
				
				
				
				
				
				
				
				

				if(ud.equals("uu")){
					String c7="/td[4]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[3]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					driver.findElement(By.xpath(cl)).click();
					break;}}
				if(ud.equals("su")){
					String c7="/td[4]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[3]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					////////driver.findElement(By.xpath(cl)).click();
					break;}}
				
				
				if(ud.equals("scd")){
					//String c7="/td[4]/i";
					//String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[5]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					////////driver.findElement(By.xpath(cl)).click();
					break;}}
				
				////////////////////////////////////////////////////////////////////////////
				
				if(ud.equals("ul")){
				String c7="/td[7]/i";
				String cl = b + "tr[" + i + "]"+c7;
				
				String c8="/td[8]";
				String cl2 = b + "tr[" + i + "]"+c8;
				if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
					Logger ll =Logger.getLogger("record present ACTIVE");
					ll.trace("reocrd present ACTIVE");
				driver.findElement(By.xpath(cl)).click();
				break;}}
				
				if(ud.equals("sl")){
					String c7="/td[7]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[8]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					////////driver.findElement(By.xpath(cl)).click();
					break;}}

				if(ud.equals("ua")){
					String c7="/td[12]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[10]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						System.out.println("active...............");
						Thread.sleep(5000);
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");

					driver.findElement(By.xpath(cl)).click();
					
					
					
					
					break;}}
				

				if(ud.equals("sa")){
					String c7="/td[12]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[10]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						System.out.println("active...............");
						Thread.sleep(5000);
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");

				//////////	driver.findElement(By.xpath(cl)).click();
					
					break;}}
				
				
				if(ud.equals("uf")){
					String c7="/td[3]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[4]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						System.out.println("active........11.......");
						Thread.sleep(5000);
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
						Thread.sleep(4000);
					driver.findElement(By.xpath(cl)).click();Thread.sleep(5000);
					break;}}
				
				if(ud.equals("sf")){
					String c7="/td[3]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[4]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						System.out.println("active...............");
						Thread.sleep(5000);
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					///////driver.findElement(By.xpath(cl)).click();
					break;}}
				
				
				if(ud.equals("uc")){
					System.out.println("enter...............");
					String c7="/td[7]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[8]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("Active")){
						System.out.println("active...............");
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
						Thread.sleep(5000);
						
					driver.findElement(By.xpath(cl)).click();
					break;
					}}
				
				if(ud.equals("sc")){
					System.out.println("enter...............");
					String c7="/td[7]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[8]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("Active")){
						System.out.println("active...............");
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
						Thread.sleep(5000);
						
					///driver.findElement(By.xpath(cl)).click();
					break;
					}}
				
				//break;

			} 
			else if (valueIneed1.equals(value)) {
				
				System.out.println("record is PRESENT AT ");
				System.out.println(i);
		
				Thread.sleep(6000);
		
				System.out.println("user id present ");
				
				if(ud.equals("uu")){
					String c7="/td[4]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[3]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					driver.findElement(By.xpath(cl)).click();
					break;}}
				if(ud.equals("su")){
					String c7="/td[4]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[3]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					////////driver.findElement(By.xpath(cl)).click();
					break;}}
				
				
				
				
				
				////////////
				if(ud.equals("ul")){
					String c7="/td[7]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[8]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						Thread.sleep(5000);
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					driver.findElement(By.xpath(cl)).click();
					break;}}
				
				
				if(ud.equals("sl")){
					String c7="/td[7]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[8]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						Thread.sleep(5000);
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					//driver.findElement(By.xpath(cl)).click();
					break;}}
				
				if(ud.equals("ua")){
					String c7="/td[12]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[10]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						System.out.println("active...............");
						Thread.sleep(5000);
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					driver.findElement(By.xpath(cl)).click();
					break;}}
				
				
				if(ud.equals("sa")){
					String c7="/td[12]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[10]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						System.out.println("active...............");
						Thread.sleep(5000);
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					driver.findElement(By.xpath(cl)).click();
					break;}}
				
				
				if(ud.equals("uf")){
					String c7="/td[3]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[4]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						System.out.println("active......1.........");
						Thread.sleep(5000);
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					driver.findElement(By.xpath(cl)).click();
					Thread.sleep(5000);
					break;}}
				
				
				if(ud.equals("sf")){
					String c7="/td[3]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[4]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("ACTIVE")){
						System.out.println("active...............");
						Thread.sleep(5000);
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					//driver.findElement(By.xpath(cl)).click();
					break;}}
				
				if(ud.equals("uc")){
					System.out.println("enter...............");
					String c7="/td[7]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[8]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("Active")){
						System.out.println("active...............");
						Thread.sleep(5000);
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					driver.findElement(By.xpath(cl)).click();
					break;
					}}

				
				
				
				if(ud.equals("sc")){
					System.out.println("enter...............");
					String c7="/td[7]/i";
					String cl = b + "tr[" + i + "]"+c7;
					
					String c8="/td[8]";
					String cl2 = b + "tr[" + i + "]"+c8;
					if(driver.findElement(By.xpath(cl2)).getText().equalsIgnoreCase("Active")){
						System.out.println("active...............");
						Thread.sleep(5000);
						Logger ll =Logger.getLogger("record present ACTIVE");
						ll.trace("reocrd present ACTIVE");
					/////////driver.findElement(By.xpath(cl)).click();
					break;
					}}
			

			} else {
				searchCount++;

			}

		}
System.out.println(searchCount+"..................."+rows.size());
		if (searchCount == rows.size()) {
			
			
			if( searchCount<1){
				System.out.println("no such record present");
			}else{
			}
		}

	}

	
	
	
	


	private updateamaster updateamaster() {
		// TODO Auto-generated method stub
		return null;
	}





	public void formupdate(String b, String value, String status) throws InterruptedException {

		System.out.println("table");
		String ac = b + "tr";
		System.out.println(ac);
		List rows = driver.findElements(By.xpath(ac));

		System.out.println("No of rows areeeeeeeeeee : " + rows.size());
		int searchCount = 0;
		for (int i = 1; i <= rows.size(); i++) {

			String a = b + "tr[" + i + "]";////////////////////
			
			WebElement tableRow = driver.findElement(By.xpath(a));

			String c1="/td[1]";
			String aaa = b + "tr[" + i + "]"+c1;
			String c2="/td[2]";
			String aaab = b + "tr[" + i + "]"+c2;
			System.out.println(aaa+"am writing records of xpaths ");
			WebElement cellIneed1 = tableRow.findElement(By.xpath(aaa));
			
			WebElement cellIneed2 = tableRow.findElement(By.xpath(aaab));
	
			String valueIneed2 = cellIneed2.getText();
					
			String valueIneed1 = cellIneed1.getText();
			System.out.println(value);
		System.out.println(valueIneed1);System.out.println(valueIneed2);
			Thread.sleep(6000);
			if (valueIneed2.equals(value)) {
				
				System.out.println("record is PRESENT AT ");
				System.out.println(i);
		
				Thread.sleep(6000);
		
				System.out.println("user name present ");
				///////////////
					
				if(status.equals("delete")|| status.equals("update")){ 
					System.out.println("111111111");
					String c3="/td[3]/i";
					String a3 = b + "tr[" + i + "]"+c3;Thread.sleep(4000);
					System.out.println(a3+"111111");
					//*[@id="datatable-responsives"]/tbody/tr[1]/td[3]/i
					tableRow.findElement(By.xpath("//*[@id='datatable-responsives']/tbody/tr[1]/td[3]/i")).click();
				
				}
				

				break;

			} else if (valueIneed1.equals(value) ) {
				
				System.out.println("record is PRESENT AT ");
				System.out.println(i);
		
				Thread.sleep(6000);
		
				System.out.println("user id present ");
				////////////
				if(status.equals("delete") || status.equals("update")){ 
					System.out.println("22222222222");
					
					
					//*[@id="datatable-responsives"]/tbody/tr[2]/td[3]/i
					String c3="/td[3]/i";
					String a3 = b + "tr[" + i + "]"+c3;
					tableRow.findElement(By.xpath("//*[@id='datatable-responsives']/tbody/tr[1]/td[3]/i")).click();
				}
				

				break;

			} else {
				searchCount++;

			}

		}
System.out.println(searchCount+"..................."+rows.size());
		if (searchCount == rows.size()) {
			
			
			if( searchCount<1){
				System.out.println("no such record present");
			}else{
			}
		}

	}


	
	

	
	
	
	
	
	
	
	
	public void updaterole(String b, String value) throws InterruptedException {

		System.out.println("table");
		String ac = b + "tr";
		System.out.println(ac);
		List rows = driver.findElements(By.xpath(ac));

		System.out.println("No of rows areeeeeeeeeee : " + rows.size());
		int searchCount = 0;
		for (int i = 1; i <= rows.size(); i++) {

			String a = b + "tr[" + i + "]";////////////////////
			
			WebElement tableRow = driver.findElement(By.xpath(a));

			String c1="/td[1]";
			String aaa = b + "tr[" + i + "]"+c1;
			String c2="/td[2]";
			String aaab = b + "tr[" + i + "]"+c2;
			System.out.println(aaa+"am writing records of xpaths ");
			WebElement cellIneed1 = tableRow.findElement(By.xpath(aaa));
			
			WebElement cellIneed2 = tableRow.findElement(By.xpath(aaab));
	
			String valueIneed2 = cellIneed2.getText();
					
			String valueIneed1 = cellIneed1.getText();
			System.out.println(value);
		System.out.println(valueIneed1);System.out.println(valueIneed2);
			Thread.sleep(6000);
			if (valueIneed2.equals(value)) {
				
				System.out.println("record is PRESENT AT ");
				System.out.println(i);
		
				Thread.sleep(6000);
		
				System.out.println("user name present ");
			
				////////////update
					String c2i="/td[4]/i";
					String abab = b + "tr[" + i + "]"+c2i;				
					driver.findElement(By.xpath(abab)).click();


				break;

			} else if (valueIneed1.equals(value)) {
				
				System.out.println("record is PRESENT AT ");
				System.out.println(i);
		
				Thread.sleep(6000);
		
				System.out.println("user id present ");
				////////////update
				String c2i="/td[4]/i";
				String abab = b + "tr[" + i + "]"+c2i;				
				driver.findElement(By.xpath(abab)).click();

				break;

			} else {
				searchCount++;

			}

		}
System.out.println(searchCount+"..................."+rows.size());
		if (searchCount == rows.size()) {
			
			
			if( searchCount<1){
				System.out.println("no such record present");
			}else{
			}
		}

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	///////////////////////////
	// user checkbox in used 
	
	public void usercheck(String b, String value) throws InterruptedException {

		System.out.println("table");
		String ac = b + "input";
		System.out.println(ac);
		List rows = driver.findElements(By.xpath(ac));

		System.out.println("No of rows areeeeeeeeeee : " + rows.size());
		int searchCount = 0;
		for (int i = 1; i <= rows.size(); i++) {

			String a = b + "input[" + i + "]";////////////////////
			
			WebElement tableRow = driver.findElement(By.xpath(a));

			
			String aaa = b + "input[" + i + "]";
			System.out.println(aaa+"am writing records of xpaths ");
			WebElement cellIneed = tableRow.findElement(By.xpath(aaa));
			
			
			String valueIneed2= 	driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div[2]/form/div/div[7]/div/text()[5]")).getAttribute("value");
			
					
			String valueIneed = cellIneed.getAttribute("value");
			System.out.println(valueIneed+"data");
			Thread.sleep(6000);
			if (valueIneed2.equals(value)) {
				
				System.out.println("record is PRESENT AT ");
				System.out.println(i);
				System.out.println("Cell value is : " + valueIneed);
				Thread.sleep(6000);
			/*	if (su.equals("update")) {
					
					
					String id_of_element =b + "div[" + i + "]"+c;
					WebElement elements = driver.findElement(By.xpath(id_of_element));

				
					//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elements);
					
					
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", elements);
					
					
					
					
				}*/
				cellIneed.click();


				break;

			} else {
				searchCount++;

			}

		}
System.out.println(searchCount+"..................."+rows.size());
		if (searchCount == rows.size()) {
			
			
		if( searchCount<1){
				System.out.println("no such record present");
			}else{
			}
		}

	}

	
	
	
	
	
	
}
