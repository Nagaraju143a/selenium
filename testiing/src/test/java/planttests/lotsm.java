package planttests;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import pages.agilepg;
import plantpages.*;
import test1.base;

public class lotsm {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {

		System.out.println("testing...");
		 test = extent.createTest("plant clicked");
			test.createNode("click on plant2");
			 Assert.assertTrue(true);
	
	}
 
  
	






///////////////////////////////////////////////////////////////////////






@DataProvider(name="lotsm")
public Object[][] lotsmadddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("lotsm","excel/plant.xlsx");
	
  return object;	 
}






//public  void adminuseraddf(String name, String n,String alname,String fname, String lastn,String gen,String email,String pwd, String cpwd,String mnum, String rnum,String roles, String add1, String add2,String coun,String state,String city, String zip) throws InterruptedException {
	@Test(dataProvider ="lotsm", priority=2)
public void lotsmaddf(String bid,String Holdareaid1  ,String Batchno,String Lot, String chk, String Remark,String ppwd) throws InterruptedException, IOException{



	 try {
		 
		 System.out.println("role  clicked");	
		 this.driver=plantbase.driver;
			plantpg o = PageFactory.initElements(driver, plantpg.class);
			util oo = PageFactory.initElements(driver, util.class);
			

			 if(o.lsm.isDisplayed()){	 Thread.sleep(3000);
			
				System.out.println("clicked ");}
			 else{
				 System.out.println("matster clicked then fmaster ");
					oo.clickpg(o.master);Thread.sleep(5000);
					
				}
			
			
			 oo.waits(o.blsm);
		 Thread.sleep(4000);
		 
	
			oo.clickpg(o.lsm);
			
			Thread.sleep(3000);
			oo.waits(o.baddbm);
			 
			 oo.clickpg(o.addbm);
		 
		 
				Thread.sleep(3000);
		 //create

				oo.waits(o.bbid);	 Thread.sleep(3000);
				oo.dropdownpg(o.bid, bid);
				 Thread.sleep(2000);
				
			
			 oo.textpg(o.Holdareaid1, Holdareaid1);
			 Thread.sleep(2000);
			 
			 oo.clickpg(o.Batchno);Thread.sleep(2000);
		oo.textpg(o.Batchno, Batchno);
		Thread.sleep(2000);			 
		 oo.clickpg(o.Lot);Thread.sleep(2000);
		oo.textpg(o.Lot, Lot);
		Thread.sleep(4000);
		
		
		driver.findElement(By.id("packetC_0")).click();
/*		String a = "0122220210109073808110@853.000";
		driver.findElement(By.xpath("//*[@class ='form-control' and @value='" + a + "']")).click();*/

		
		Thread.sleep(2000);			 
		 oo.clickpg(o.reggistraion);Thread.sleep(2000);
		 oo.waits(o.bRemark);
			
			Thread.sleep(2000);
			 oo.textpg(o.Remark, Remark);
			Thread.sleep(2000);
			 oo.textpg(o.pwd, ppwd);
			 oo.clickpg(o.validateuser);	Thread.sleep(4000);
			 
			 
			 Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");
		
			 Thread.sleep(3000);
					 
				 oo.waits(o.bERES);	 
				 oo.clickpg(o.ERES);
				 oo.waits(o.bokbtn);Thread.sleep(4000); 
				 Logger ll =Logger.getLogger("n role ");
				 ll.trace(o.uaalert.getText());

				 oo.clickpg(o.okbtn);
				 Thread.sleep(4000);
				 
				
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver); Thread.sleep(2000);

	 	}
	 	test = extent.createTest("intr reg in master ");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
			
			
	System.out.println(driver.getCurrentUrl());
		}
	 
	 
	 
	 
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{
		


		System.out.println("test");
		 test = extent.createTest("instr reg clicked");
			test.createNode("click on plant2");
			 Assert.assertTrue(true);
		
	}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
