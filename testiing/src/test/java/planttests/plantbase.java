package planttests;


import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.google.common.base.Function;
import com.google.common.io.Files;

import adminsettingpg.NewTestexcel;

import adminsettingpg.util;

import pages.agilepg;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
 
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import plantpages.*;
public class plantbase {
 public  static WebDriver driver; 
 util oo = PageFactory.initElements(driver, util.class);
 
 public static String url ="http://10.10.10.71/PMTS_WOE/UI/Login/NewLogin";
 
 //public static String url ="http://10.10.10.71/PMTSBV/UI/Login/NewLogin";
 
 


 
 
 
 
 
 
 

 
@BeforeMethod

	public static WebDriver launchapp() 
	{
		if(driver == null) {			
			try{				
				  
	
				
			
				System.out.println(System.getProperty("user.dir"));
		    	
		    			    		
		    		
		    		if(System.getProperty("user.dir").contains("C:\\") ||  System.getProperty("user.dir").contains("D:\\")|| System.getProperty("user.dir").contains("E:\\")   )
		    			 
		    		{
		    			
		    		 System.setProperty("webdriver.chrome.driver", "Z:\\chromedriver.exe");	
		    		}
		    		else {
		    			
		    		
		    			
		    			System.out.println(" run in linux");
		    			System.setProperty("webdriver.gecko.driver", "/usr/bin/geckodriver");	
		    			
		    		
		    		}
		    		
			 
		    		 
			 
				
				  // head less browser testing(GUI Not visible)
		    	 /*  ChromeOptions options = new ChromeOptions();
		    	   options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors", "--silent");
		    	       
		    		 
				  driver=new ChromeDriver(options);*/
		    		 
		    		 
		    		 
		    		 
				 driver=new ChromeDriver();
		    		 
				   Thread.sleep(4000);
			       driver.get(url);
			       Thread.sleep(4000);
			       Logger ll =Logger.getLogger("nani");
			       
			
			     ll.info("Browser Opened");
			    ll.debug("--information--");
			       
			       
			       driver.manage().window().maximize();
			     
			   }catch(Exception e) {
				   System.out.println(".....nagaraju......"+e.getMessage());
			   }
			
		}
		
		 return driver;
	}


@DataProvider(name="pom")
public Object[][] logindataprovider() throws IOException{
	
	Object[][] object=	oo.getDataFromDataprovider("login","excel/plant.xlsx");	
  return object;	 
}



@Test(dataProvider ="pom")

public  void alogin(String n,String p) throws InterruptedException {
	
	System.out.println("hiiiiii");
	
	
	
	
	plantpg o = PageFactory.initElements(driver, plantpg.class); 
	 test = extent.createTest("login pmtsbv");
	 test.createNode("Login with valid input");
	 Assert.assertTrue(true);
		 
	
		 try {
			
			// driver.findElement(By.xpath("/html/body/div/button")).click();
		     //  Thread.sleep(4000);
			
				Thread.sleep(5000);
				
				System.out.println(driver.getCurrentUrl());
				
				if(n.contains(".")){
					oo.textipg(o.loginname, n);
				}else{
				oo.textpg(o.loginname, n);
				
				}
				
				System.out.println("please enter user paswd");
			

		
				Thread.sleep(5000);
				oo.textpg(o.loginpassword, p);
			
				 Logger ll =Logger.getLogger("nani");
				ll.trace(p);
			
				Thread.sleep(4000);
				oo.clickpg(o.loginsub);
				
			
				Thread.sleep(4000);
				//oo.srnsht(driver);
			//////////////	Thread.sleep(4000);
				
				
				/////// try to failed here for screenshot
				test.createNode("Login with valid input");
				 Assert.assertTrue(true);
						
		 }catch (Exception e)
		 {System.out.println(e.getMessage()+"eorrrrrrrrrrr");
		// TODO: handle exception
		 	}
		
		 
		
		}

//////////////////////////////////////

@Test
public void automaticsceenschot() {
test = extent.createTest("noCommerceLoginTest");

test.createNode("Login with Valid input");
Assert.assertTrue(true);

test.createNode("Login with In-valid input");
Assert.assertTrue(true);
}





public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;

@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }
 

}














}

