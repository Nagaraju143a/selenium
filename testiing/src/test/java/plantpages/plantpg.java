
package plantpages;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;



import org.openqa.selenium.Keys;

public class plantpg {
	
	WebDriver driver;
public plantpg(WebDriver driver){ 
        this.driver=driver; 
                
        // driver from tc assigned to  webdriver  driver by using this.driver here assigned to our WebDriver driver;
}
// //*[@id="layoutbody"]/div/nav/ul/li[1]/ul/li[2]/a
@FindBy(xpath="//*[@class='user-profile dropdown-toggle']")  public WebElement logout1;
public By blogout1 = By.xpath("//*[@class='user-profile dropdown-toggle']");

/*@FindBy(xpath="//a[@href='/PMTSBV/UI/Login/Logout']") public WebElement logout2;*/
@FindBy(xpath="//a[contains(@href,'Login/Logout')]")  public WebElement logout2;
	
//login
	@FindBy(xpath="//*[@id='username']")  public WebElement loginname;
	public By bloginname =By.xpath("//*[@id='username']");
	
	@FindBy(xpath="//*[@id='password']")  public WebElement loginpassword;
	public By bloginpassword =By.xpath("//*[@id='password']");
	
	/*@FindBy(xpath="//*[@value='LogIn']")  public WebElement loginsub;
	public By bloginsub =By.xpath("//*[@value='LogIn']");*/
	
	
	@FindBy(xpath="/html/body/div/div/div/div[2]/form/div[6]/input")  public WebElement loginsub;
	public By bloginsub =By.xpath("/html/body/div/div/div/div[2]/form/div[6]/input");
	
	//userreq
	
	@FindBy(xpath="//*[@id='sidebar-menu']/div/ul/li[6]/a/i")  public WebElement usrreq;
	public By busrreq =By.xpath("//*[@id='sidebar-menu']/div/ul/li[6]/a/i");
	
	
	//User Request For Updating the Proposed Product And Batch 
	
	
	@FindBy(xpath="//a[contains(@href,'UserRequsestForChangeProductAndBatch_List')]")  public WebElement usrreq1;
	public By busrreq1 =By.xpath("//a[contains(@href,'UserRequsestForChangeProductAndBatch_List')]");
	
	
	@FindBy(xpath="//*[@id='regform]/div[2]/button[1]")  public WebElement us1r1;
	public By bus1r1 =By.xpath("//*[@id='regform]/div[2]/button[1]");
	@FindBy(xpath="//*[@id='regform]/div[2]/button[2]")  public WebElement us1r2;
	public By bus1r2 =By.xpath("//*[@id='regform]/div[2]/button[2]");
	
	@FindBy(xpath="//*[@id='regform']/div/div[6]/div[2]")  public WebElement wve;
	public By bwve =By.xpath("//*[@id='regform']/div/div[6]/div[2]");
	
	@FindBy(xpath="//*[@id='ID']")  public WebElement usrarea;
	public By busrarea =By.xpath("//*[@id='ID']");
	
	@FindBy(xpath="//*[@id='Eqid']")  public WebElement Eqid;
	public By bEqid =By.xpath("//*[@id='Eqid']");
	@FindBy(xpath="//*[@id='Block']")  public WebElement Block;
	public By bBlock =By.xpath("//*[@id='Block']");
	
	@FindBy(xpath="//*[@id='Location']")  public WebElement Location;
	public By bLocation =By.xpath("//*[@id='Location']");
	@FindBy(xpath="//*[@id='submit']")  public WebElement submit;
	public By bsubmit =By.xpath("//*[@id='submit']");

	//*[@id="sidebar-menu"]/div/ul/li[6]/ul/li[2]/a
	
	
	
// User Request For Updating the Equipment Category And Area 

	@FindBy(xpath="//a[contains(@href,'UserRequestForChangeNextCatAndArea_List')]")  public WebElement usrreq2;
	public By busrreq2 =By.xpath("//a[contains(@href,'UserRequestForChangeNextCatAndArea_List')]");
	
		
	@FindBy(xpath="//*[@id='EqcatOrArea1' and  @value='0']")  public WebElement usr2r0;
	public By busr2r0 =By.xpath("//*[@id='EqcatOrArea1' and  @value='0']");	
	@FindBy(xpath="//*[@id='EqcatOrArea1' and  @value='1']")  public WebElement usr2r1;
	public By busr2r1 =By.xpath("//*[@id='EqcatOrArea1' and  @value='1']");		
	@FindBy(xpath="//*[@id='AreaBatch']")  public WebElement AreaBatch;
	public By bAreaBatch =By.xpath("//*[@id='AreaBatch']");	
	@FindBy(xpath="//*[@id='NextArea']")  public WebElement NextArea;
	public By bNextArea =By.xpath("//*[@id='NextArea']");
	

	//
	@FindBy(xpath="//*[@id='EqcatBatch']")  public WebElement EqcatBatch;
	public By bEqcatBatch =By.xpath("//*[@id='EqcatBatch']");
	@FindBy(xpath="//*[@id='AreaRefid']")  public WebElement AreaRefid;
	public By bAreaRefid =By.xpath("//*[@id='AreaRefid']");
	@FindBy(xpath="//*[@id='EquipmentRefID']")  public WebElement EquipmentRefID;
	public By bEquipmentRefID =By.xpath("//*[@id='EquipmentRefID']");
	@FindBy(xpath="//*[@id='Barcode']")  public WebElement Barcode;
	public By bBarcode =By.xpath("//*[@id='Barcode']");
	@FindBy(xpath="//*[@id='NextCategory']")  public WebElement NextCategory;
	public By bNextCategory =By.xpath("//*[@id='NextCategory']");
	
	
	
	//reports
	
	@FindBy(xpath="//*[@id='sidebar-menu']/div/ul/li[5]/a/i")  public WebElement reports;
	public By breports =By.xpath("//*[@id='sidebar-menu']/div/ul/li[5]/a/i");
	
	@FindBy(xpath="//a[contains(@href,'AdminReports')]")  public WebElement AdminReports;
	public By bAdminReports =By.xpath("//a[contains(@href,'AdminReports')]");
	
	
	@FindBy(xpath="//*[@id='page']")  public WebElement page;
	public By bpage =By.xpath("//*[@id='page']");
	
	
	@FindBy(xpath="//*[@id='Type']")  public WebElement Type;
	public By bType =By.xpath("//*[@id='Type']");
	
	@FindBy(xpath="//*[@id='validateuser']")  public WebElement adminr;
	public By badminr =By.xpath("//*[@id='validateuser']");
		
	@FindBy(xpath="//*[@id='example_filter']/label/input")  public WebElement adminrur;
	public By badminrur =By.xpath("//*[@id='example_filter']/label/input");
	
	@FindBy(xpath="//*[@id='example']/tbody/tr/td[3]")  public WebElement ur1;
	public By bur1 =By.xpath("//*[@id='example']/tbody/tr/td[3]");
	
	//Master Reports 
	
	@FindBy(xpath="//a[contains(@href,'ReportsUI/Reports')]")  public WebElement masterr;
	public By bmasterr =By.xpath("//a[contains(@href,'ReportsUI/Reports')]");
	
	@FindBy(xpath="//*[@id='example']/tbody/tr")  public WebElement rt1;
	public By brt1 =By.xpath("//*[@id='example']/tbody/tr");
	
	// hold area reports 

	
	@FindBy(xpath="//a[contains(@href,'HoldAreaReport')]")  public WebElement har;
	public By bhar =By.xpath("//a[contains(@href,'HoldAreaReport')]");
	
	@FindBy(xpath="//*[@id='holdarea']")  public WebElement holdarea;
	public By bholdarea =By.xpath("//*[@id='holdarea']");
	
	
	@FindBy(xpath="//*[@id='rbholdin']")  public WebElement rbholdin;
	public By brbholdin =By.xpath("//*[@id='rbholdin']");
	
	
	@FindBy(xpath="//*[@id='selectdate1']")  public WebElement selectdate1;
	public By bselectdate1 =By.xpath("//*[@id='selectdate1']");
	@FindBy(xpath="//*[@id='selectdate2']")  public WebElement selectdate2;
	public By bselectdate2 =By.xpath("//*[@id='selectdate2']");
	
	@FindBy(xpath="//*[@id='example']/tbody")  public WebElement hab;
	public By bhab =By.xpath("//*[@id='example']/tbody");

	@FindBy(xpath="//*[@id='ui-datepicker-div']/table/tbody")  public WebElement dp;
	public By bdp =By.xpath("//*[@id='ui-datepicker-div']/table/tbody");
	
	@FindBy(xpath="//*[@id='ui-datepicker-div']/table/tbody")  public WebElement dp1;
	public By bdp1 =By.xpath("//*[@id='ui-datepicker-div']/table/tbody");
	
	
	//Checklist Reports(Historical)
	@FindBy(xpath="//a[contains(@href,'Batchdetails')]")  public WebElement Batchdetails;
	public By bBatchdetails =By.xpath("//a[contains(@href,'Batchdetails')]");
	
	
	@FindBy(xpath="//*[@id='select2-ddlBatchNo-container']")  public WebElement bdrp;
	public By bbdrp =By.xpath("//*[@id='select2-ddlBatchNo-container']");
	
	@FindBy(xpath="//*[@class='select2-search__field']")  public WebElement bdrp1;
	public By bbdrp1 = By.xpath("//*[@class='select2-search__field']");
	
	
	@FindBy(xpath="//*[@class='CAslideContent']")  public WebElement CAslideContent;
	public By bCAslideContent = By.xpath("//*[@class='CAslideContent']");
	

	
	//Checklist Reports 
	
	@FindBy(xpath="//a[contains(@href,'LogBookReports')]")  public WebElement LogBookReports;
	public By bLogBookReports =By.xpath("//a[contains(@href,'LogBookReports')]");
	
	
	@FindBy(xpath="//*[@id='Areaid']")  public WebElement Areaid;
	public By bAreaid =By.xpath("//*[@id='Areaid']");
/*	@FindBy(xpath="//*[@id='Eqid']")  public WebElement Eqid;
	public By bEqid =By.xpath("//*[@id='Eqid']");*/
	
	@FindBy(xpath="//*[@id='formategrp']")  public WebElement formategrp;
	public By bformategrp =By.xpath("//*[@id='formategrp']");
	
	
	@FindBy(xpath="//*[@id='ProposedBatch']")  public WebElement ProposedBatch;
	public By bProposedBatch =By.xpath("//*[@id='ProposedBatch']");
	
	@FindBy(xpath="//*[@id='selectdate']")  public WebElement selectdate;
	public By bselectdate =By.xpath("//*[@id='selectdate']");
	
	@FindBy(xpath="//*[@id='btnreport']")  public WebElement btnreport;
	public By bbtnreport =By.xpath("//*[@id='btnreport']");		
	
	@FindBy(xpath="//*[@id='printviewerjs1']/div[2]/label")  public WebElement lbl;
	public By blbl =By.xpath("//*[@id='printviewerjs1']/div[2]/label");
	

	//product operator

	@FindBy(xpath="//*[@id='sidebar-menu']/div/ul/li[4]/a/i")  public WebElement po;
	public By bpo =By.xpath("//*[@id='sidebar-menu']/div/ul/li[4]/a/i");
	

	
	//batch termination
	@FindBy(xpath="//a[contains(@href,'BatchTerminationList')]")  public WebElement bt;
	public By bbt =By.xpath("//a[contains(@href,'BatchTerminationList')]");
	
			//blockid   LOCATIONID  erAREAID   erEQUIPMENT_CATEGORY_ID  btnsubmit
	
	//Batch Completion 
	
	@FindBy(xpath="//a[contains(@href,'BatchCompletionList')]")  public WebElement bcl;
	public By bbcl =By.xpath("//a[contains(@href,'BatchCompletionList')]");
	
	//fsearch1	
	//erareaw
	
	
	@FindBy(xpath="//*[@id='No']")  public WebElement No;
	public By bNo =By.xpath("//*[@id='No']");
	
	@FindBy(xpath="//*[@id='Units']")  public WebElement Units;
	public By bUnits =By.xpath("//*[@id='Units']");
	
	// ingredientcode1;
	
	@FindBy(xpath="//*[@id='KeyMachines']/div/table/tbody/tr/td[2]/div/button")  public WebElement kmach;
	public By bkmach =By.xpath("//*[@id='KeyMachines']/div/table/tbody/tr/td[2]/div/button");
	
	///// 
	@FindBy(xpath="//*[@value='MEP_PR-PK-BPM-004_HOONGA_TABS']")  public WebElement kmach1;
	public By bkmach1 =By.xpath("//*[@value='MEP_PR-PK-BPM-004_HOONGA_TABS']");
	
	
	@FindBy(xpath="//*[@id='regform']/div[14]/button[1]")  public WebElement bcsubmit;
	public By bbcsubmit =By.xpath("//*[@id='regform']/div[14]/button[1]");
	
	
	@FindBy(xpath="//*[@id='Yes']")  public WebElement Yes;
	public By bYes =By.xpath("//*[@id='Yes']");
	@FindBy(xpath="//*[@id='SampleWeight']")  public WebElement SampleWeight;
	public By bSampleWeight =By.xpath("//*[@id='SampleWeight']");
	
	
	
	
	//Master
	
	
	
	@FindBy(xpath="//*[@id='sidebar-menu']/div/ul/li[3]/a/i")  public WebElement master;
	public By bmaster =By.xpath("//*[@id='sidebar-menu']/div/ul/li[3]/a/i");
	
	
	
	
	// balck master <a href="/PMTSBV/UI/MastersUI/BlockMasterList"> Block Master </a>
	
	@FindBy(xpath="//a[contains(@href,'BlockMasterList')]")  public WebElement blockmaster;
	public By bblockmaster =By.xpath("//a[contains(@href,'BlockMasterList')]");
	
	@FindBy(xpath="//*[@class='collapse-link']")  public WebElement addbm;
	public By baddbm = By.xpath("//*[@class='collapse-link']");
	
	
	@FindBy(xpath="//*[@id='BLOCKID']")  public WebElement BLOCKID;
	public By bBLOCKID =By.xpath("//*[@id='BLOCKID']");
	
	
	@FindBy(xpath="//*[@id='BLOCKNAME']")  public WebElement BLOCKNAME;
	public By bBLOCKNAME =By.xpath("//*[@id='BLOCKNAME']");
	


	@FindBy(xpath="//*[@id='BLOCKDESCRIPTION']")  public WebElement BLOCKDESCRIPTION;
	public By bBLOCKDESCRIPTION =By.xpath("//*[@id='BLOCKDESCRIPTION']");
	
	@FindBy(xpath="//*[@id='sub1']")  public WebElement sub1;
	public By bsub1 =By.xpath("//*[@id='sub1']");
	
	// electronic sign
		@FindBy(xpath="//*[@name='Remark']")  public WebElement Remark;
		public By bRemark = By.xpath("//*[@name='Remark']");	
		@FindBy(xpath="//*[@name='pwd']")  public WebElement pwd;
		public By bpwd = By.xpath("//*[@name='pwd']");	
		@FindBy(xpath="//*[@name='validateuser']")  public WebElement validateuser;
		public By bvalidateuser = By.xpath("//*[@name='validateuser']");		
		@FindBy(xpath="//*[@id='myModal1']/div/div/div[1]/div/div[1]/h4")  public WebElement uaalert;
		
		
		public By buaalert = By.xpath("//*[@id='myModal1']/div/div/div[1]/div/div[1]/h4");
		@FindBy(xpath="//*[@id='ERES']")  public WebElement ERES;
		public By bERES = By.xpath("//*[@id='ERES']");
		@FindBy(xpath="//*[@id='okbtn']")  public WebElement okbtn;
		public By bokbtn = By.xpath("//*[@id='okbtn']");
		@FindBy(xpath="//*[@id='okbtn1']")  public WebElement okbtn1;
		public By bokbtn1 = By.xpath("//*[@id='okbtn1']");
		

		
		@FindBy(xpath="//*[@id='valerror']")  public WebElement valerror;
		public By bvalerror = By.xpath("//*[@id='valerror']");
		 
			@FindBy(xpath="//*[@id='valBlocknull']")  public WebElement valBlocknull;
			public By bvalBlocknull = By.xpath("//*[@id='valBlocknull']");		
			@FindBy(xpath="//*[@id='Unamenull']")  public WebElement Unamenull;
			public By bUnamenull = By.xpath("//*[@id='Unamenull']");
			@FindBy(xpath="//*[@id='valdescriptionnull']")  public WebElement valdescriptionnull;
			public By bvaldescriptionnull = By.xpath("//*[@id='valdescriptionnull']");
		 
			@FindBy(xpath="//*[@class='alert']")  public WebElement useralert;
			public By buseralert = By.xpath("//*[@class='alert']");
			
			@FindBy(xpath="//*[@id='Errorbtn']")  public WebElement Errorbtn;
			public By bErrorbtn = By.xpath("//*[@id='Errorbtn']");
		
		
		
		//formulation //*[@id='regform']/div/div[5]/div[2]
		
			
		@FindBy(xpath="//a[contains(@href,'FormulationMasterList')]")  public WebElement fmasterl;
		public By bfmasterl =By.xpath("//a[contains(@href,'FormulationMasterList')]");
		public String bbfmaster1="//a[contains(@href,'FormulationMasterList')]";
		@FindBy(xpath="//*[@id='FORMULATIONID']")  public WebElement FORMULATIONID;
		public By bFORMULATIONID =By.xpath("//*[@id='FORMULATIONID']");
		
		@FindBy(xpath="//*[@id='FORMULATIONNAME']")  public WebElement FORMULATIONNAME;
		public By bFORMULATIONNAME =By.xpath("//*[@id='FORMFORMULATIONNAMEULATIONID']");
		
	public String FORMULATIONNAME1 ="//*[@id='FORMULATIONNAME']";
		@FindBy(xpath="//*[@id='sub']")  public WebElement sub;
		public By bsub =By.xpath("//*[@id='sub']");
		
		
		
		// search formation
		@FindBy(xpath="//a[contains(@href,'#menu11')]")  public WebElement drs;
		public By bdrs =By.xpath("//a[contains(@href,'#menu11')]");
		//*[@id="datatable-responsive_filter"]/label/input
		@FindBy(xpath="//*[@id='datatable-responsives_filter']/label/input")  public WebElement fsearch;
	
		public By bfsearch =By.xpath("//*[@id='datatable-responsives_filter']/label/input");
	
		
		//*[@id="
		@FindBy(xpath="//*[@id='datatable-responsive_filter']/label/input")  public WebElement fsearch1;
	
		public By bfsearch1 =By.xpath("//*[@id='datatable-responsive_filter']/label/input");
		
		public String bform="//*[@id='datatable-responsives']/tbody/";
		public String bformn="//*[@id='datatable-responsive']/tbody/";
	
	
		public String bformaster="//*[@id='datatable-responsives']/tbody/tr";
		public String bformastern="//*[@id='datatable-responsive']/tbody/tr";
	
		public String bfdiv="//*[@id='datatable-responsives']/tbody/tr/td";
	
		public String bfdivn="//*[@id='datatable-responsive']/tbody/tr/td";
		
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[1]")  public WebElement bfdiv1;
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[2]")  public WebElement bfdiv2;
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[3]/i")  public WebElement bfdiv3;
		
		public String b1fmfm="//*[@id='datatable-responsive']/tbody/tr/td[3]/i";	
		//*[@id="datatable-responsives']/tbody/tr/td[7]/i
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[3]/i")  public WebElement fmfm;		
		public By bfmfm =By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[3]/i");
		
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[4]/i")  public WebElement bmfm;	
		public By bbmfm=By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[4]/i");
		
		//*[@id="datatable-responsive"]/tbody/tr/td[4]/i
		
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[4]")  public WebElement bfdiv4;//ACTIVE
		
		@FindBy(xpath="//*[@id='regform']/div[4]/button[2]")  public WebElement fdeactive;
		public By bfdeactive =By.xpath("//*[@id='regform']/div[4]/button[2]");
		
		// location master dwf
		//*[@id='datatable-responsive']/tbody/tr/td[1]/a/u
		
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[7]/i")  public WebElement locfw;	
		public By blocfw=By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[7]/i");
		//*[@id="datatable-responsive"]/tbody/tr/td[7]/i
		//*[@id="datatable-responsive"]/tbody/tr/td[7]/i
		
		@FindBy(xpath="//*[@id='regform']/div/div[8]/button[1]")  public WebElement ulocfw;	
		public By bulocfw=By.xpath("//*[@id='regform']/div/div[8]/button[1]");
		//*[@id="regform"]/div/div[7]/button[1]
		@FindBy(xpath="//*[@id='regform']/div/div[8]/button[2]")  public WebElement rlocfw;	
		
		public By brlocfw=By.xpath("//*[@id='regform']/div/div[8]/button[2]");
		
		@FindBy(xpath="//*[@id='regform']/div/div[8]/button[1]")  public WebElement dlocfw;	
		public By bdlocfw=By.xpath("//*[@id='regform']/div/div[8]/button[1]");
		
		//*[@id="regform"]/div/div[8]/button[1]
	
		
		
	//block master
		//*[@id="submit"]

		@FindBy(xpath="//*[@id='submit']")  public WebElement rblock;
		public By brblock =By.xpath("//*[@id='submit']");
	
		@FindBy(xpath="//*[@id='Reject']")  public WebElement rrblock;
		public By brrblock =By.xpath("//*[@id='Reject']");
		
		@FindBy(xpath="//*[@id='drop']")  public WebElement ublock;
		public By bublock =By.xpath("//*[@id='drop']");
		
		
		@FindBy(xpath="//*[@id='Reject2']")  public WebElement rublock;
		public By brublock =By.xpath("//*[@id='Reject2']");
		
		// foramtion update
		
		
		@FindBy(xpath="//*[@id='regform']/div[4]/button[1]")  public WebElement fupdate;
		public By bfupdate =By.xpath("//*[@id='regform']/div[4]/button[1]");
		
		
		//LocationMasterList
		
		@FindBy(xpath="//a[contains(@href,'LocationMasterList')]")  public WebElement lmaster1;
		public By blmaster1 =By.xpath("//a[contains(@href,'LocationMasterList')]");
		
		@FindBy(xpath="//*[@id='blockid']")  public WebElement blockid;
		public By bblockid =By.xpath("//*[@id='blockid']");
		
		public String blockid1 ="//*[@id='blockid']";
		@FindBy(xpath="//*[@id='LOCATIONID']")  public WebElement LOCATIONID;
		public By bLOCATIONID =By.xpath("//*[@id='LOCATIONID']");
		
		@FindBy(xpath="//*[@id='LOCATIONNAME']")  public WebElement LOCATIONNAME;
		public String LOCATIONNAME1 ="//*[@id='LOCATIONNAME']";
		public By bLOCATIONNAME =By.xpath("//*[@id='LOCATIONNAME']");
		
		@FindBy(xpath="//*[@id='SUBINVENTORYCODE']")  public WebElement SUBINVENTORYCODE;
		public By bSUBINVENTORYCODE =By.xpath("//*[@id='SUBINVENTORYCODE']");
		
		@FindBy(xpath="//*[@id='LOCATIONDESCRIPTION']")  public WebElement LOCATIONDESCRIPTION;
		public By bLOCATIONDESCRIPTION =By.xpath("//*[@id='LOCATIONDESCRIPTION']");
		
		@FindBy(xpath="//*[@id='locationtype']")  public WebElement locationtype;
		public By blocationtype =By.xpath("//*[@id='locationtype']");
		//Location Master Approve Process Completed Successfully
		
		//search loaction
		
		//*[@id="datatable-responsives"]/tbody/tr/td[3]
		
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[3]")  public WebElement ldiv1;
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[4]")  public WebElement ldiv2;
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[3]")  public WebElement ldiv1n;
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[4]")  public WebElement ldiv2n;
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[8]")  public WebElement ldiv8;
	
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[7]")  public WebElement ldiv7;
		
		@FindBy(xpath="//*[@id='regform']/div/div[8]/button[2]")  public WebElement ldrop;
		public By bldrop =By.xpath("//*[@id='regform']/div/div[8]/button[2]");
		
		@FindBy(xpath="//*[@id='regform']/div/div[8]/button[1]")  public WebElement lupdate;
		public By blupdate =By.xpath("//*[@id='regform']/div/div[8]/button[1]");
		
		
		
		
		//area master wf
		
		//*[@id="regform"]/div[13]/button[1]
				
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[11]/i")  public WebElement amdiv;
		public By bamdiv =By.xpath("//*[@id='datatable-responsives']/tbody/tr/td[11]/i");
		
		@FindBy(xpath="//*[@id='regform']/div[13]/button[1]")  public WebElement ama;
		public By bama =By.xpath("//*[@id='regform']/div[13]/button[1]");
		@FindBy(xpath="//*[@id='regform']/div[13]/button[2]")  public WebElement amr;
		public By bamr =By.xpath("//*[@id='regform']/div[13]/button[2]");
	
		//AreaMasterList
	
		@FindBy(xpath="//a[contains(@href,'AreaMasterList')]")  public WebElement amaster1;
		public By bamaster1 =By.xpath("//a[contains(@href,'AreaMasterList')]");
			////LocationMasterList  blockid tc002
		
		//*[@id="datatable-responsive"]/tbody/tr/td[10]
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[10]")  public WebElement adiv10;
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[12]")  public WebElement adiv12;
		//LocationMasterList  LOCATIONID  loc002
		//*[@id="datatable-responsives"]/tbody/tr/td[4]
		@FindBy(xpath="//*[@id='ERPSTATUS']")  public WebElement ERPSTATUS;
		public By bERPSTATUS =By.xpath("//*[@id='ERPSTATUS']");
		

		
		@FindBy(xpath="//*[@id='regform']/div[13]/button[2]")  public WebElement droparea;
		public By bdroparea =By.xpath("//*[@id='regform']/div[13]/button[2]");
		
		@FindBy(xpath="//*[@id='regform']/div[13]/button[1]")  public WebElement uarea;
		public By buarea =By.xpath("//*[@id='regform']/div[13]/button[1]");
		
		///ERPSTATUS  WITH ERP    WITHOUT ERP
		
		/////////////button ingredientcode1
		
		@FindBy(xpath="//*[@id='areadivid']/div/button")  public WebElement button;
		public By bbutton =By.xpath("//*[@id='areadivid']/div/button");
		
		///////////ingredientcode1   MW1.APPRFGRL.0
		@FindBy(xpath="//*[@id='ingredientcode1']")  public WebElement ingredientcode1;
		public By bingredientcode1 =By.xpath("//*[@id='ingredientcode1']");
		
		@FindBy(xpath="//*[@id='ERPAREAID']")  public WebElement ERPAREAID;
		public By bERPAREAID =By.xpath("//*[@id='ERPAREAID']");
		@FindBy(xpath="//*[@id='AREANAME']")  public WebElement AREANAME;
		public By bAREANAME =By.xpath("//*[@id='AREANAME']");
		public String AREANAME1= "//*[@id='AREANAME']" ;
		
		@FindBy(xpath="//*[@id='ProcessingArea']")  public WebElement ProcessingArea;
		public By bProcessingArea =By.xpath("//*[@id='ProcessingArea']");
		
		@FindBy(xpath="//*[@id='HoldArea']")  public WebElement HoldArea;
		public By bHoldArea =By.xpath("//*[@id='HoldArea']");
		
		@FindBy(xpath="//*[@id='WashArea']")  public WebElement WashArea;
		public By bWashArea =By.xpath("//*[@id='WashArea']");
		@FindBy(xpath="//*[@id='wipholdarea']")  public WebElement wipholdarea;
		public By bwipholdarea =By.xpath("//*[@id='wipholdarea']");
		
		
		
		@FindBy(xpath="//*[@id='IS_IT_HAS_BALANCE']")  public WebElement IS_IT_HAS_BALANCE;
		public By bIS_IT_HAS_BALANCE =By.xpath("//*[@id='IS_IT_HAS_BALANCE']");
		
		@FindBy(xpath="//*[@id='IS_IT_HAS_TYPEA']")  public WebElement IS_IT_HAS_TYPEA;
		public By bIS_IT_HAS_TYPEA =By.xpath("//*[@id='IS_IT_HAS_TYPEA']");
		
		@FindBy(xpath="//*[@id='IS_IT_HAS_LOTS']")  public WebElement IS_IT_HAS_LOTS;
		public By bIS_IT_HAS_LOTS =By.xpath("//*[@id='IS_IT_HAS_LOTS']");
		
		@FindBy(xpath="//*[@id='CONT_WISE_AREA_START']")  public WebElement CONT_WISE_AREA_START;
		public By bCONT_WISE_AREA_START =By.xpath("//*[@id='CONT_WISE_AREA_START']");
		
		@FindBy(xpath="//*[@id='btnsubmit']")  public WebElement btnsubmit;
		public By bbtnsubmit =By.xpath("//*[@id='btnsubmit']");
		
	
		
		//EquipmentCategoryList


		
		
		@FindBy(xpath="//a[contains(@href,'EquipmentCategoryList')]")  public WebElement ecmaster1;
		public By becmaster1 =By.xpath("//a[contains(@href,'EquipmentCategoryList')]");
				//addbm
		
		@FindBy(xpath="//*[@id='blockid']")  public WebElement ecblockid;
		public By becblockid =By.xpath("//*[@id='blockid']");
		
		@FindBy(xpath="//*[@id='LOCATIONID']")  public WebElement ecLOCATIONID;
		public By becLOCATIONID =By.xpath("//*[@id='LOCATIONID']");
		
		@FindBy(xpath="//*[@id='EquipmentCategoryId']")  public WebElement ecEquipmentCategoryId;
		public By becEquipmentCategoryId =By.xpath("//*[@id='EquipmentCategoryId']");
		
		@FindBy(xpath="//*[@id='EquipmentCategoryName']")  public WebElement ecEquipmentCategoryName;
		public String ecEquipmentCategoryName1 ="//*[@id='EquipmentCategoryName']";
		public By becEquipmentCategoryName =By.xpath("//*[@id='EquipmentCategoryName']");
		
		@FindBy(xpath="//*[@id='btnsubmit']")  public WebElement ecbtnsubmit;
		public By becbtnsubmit =By.xpath("//*[@id='btnsubmit']");
		
		
		
		//search
		
		//*[@id='datatable-responsives']/tbody/tr/td[7]/i
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[8]")  public WebElement catdiv8;
		public By bcatdiv8 =By.xpath("//*[@id='datatable-responsives']/tbody/tr/td[8]");
		
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[7]/i")  public WebElement catdiv7;
		public By bcatdiv7 =By.xpath("//*[@id='datatable-responsives']/tbody/tr/td[7]/i");
	
		@FindBy(xpath="//*[@id='regform']/div/div[7]/button[2]")  public WebElement dropcat;
		public By bdropcat =By.xpath("//*[@id='regform']/div/div[7]/button[2]");
		public String dropcat1 ="/*[@id='regform']/div/div[7]/button[2]";
		
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[5]")  public WebElement cdiv5;
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[6]")  public WebElement cdiv6;

		//*[@id="regform"]/div/div[7]/button[1]
		@FindBy(xpath="//*[@id='regform']/div/div[7]/button[1]")  public WebElement updatecat;
		public By bupdatecat =By.xpath("//*[@id='regform']/div/div[7]/button[1]");
		
		//*[@id="regform"]/div/div[7]/button[2]
		
		
		@FindBy(xpath="//*[@id='regform']/div/div[7]/button[2]")  public WebElement recat;
		public By brecat =By.xpath("//*[@id='regform']/div/div[7]/button[2]");
		
		
		
		
		//eq reg fw
		//*[@id='datatable-responsive']/tbody/tr/td[4]
		
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[4]")  public WebElement erarea;
		public By berarea =By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[4]");
		
		
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[11]/i")  public WebElement eri;
		public By beri =By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[11]/i");
		//*[@id="datatable-responsive"]/tbody/tr/td[11]/i
		@FindBy(xpath="//*[@id='regform']/div/div[17]/button[1]")  public WebElement errevapv;
		public By berrevapv =By.xpath("//*[@id='regform']/div/div[17]/button[1]");
		//*[@id='regform']/div/div[17]/button[1]
		@FindBy(xpath="//*[@id='regform']/div/div[17]/button[2]")  public WebElement errevrej;
		public By berrevrej =By.xpath("//*[@id='regform']/div/div[17]/button[2]");
		//*[@id="regform"]/div/div[17]/button[2]
		
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr[2]/td[11]/i")  public WebElement eri2;
		public By beri2 =By.xpath("//*[@id='datatable-responsive']/tbody/tr[2]/td[11]/i");
		
		
		//*[@id='regform']/div/div[17]/button[1]
		
		@FindBy(xpath="//*[@id='regform']/div/div[17]/button[1]")  public WebElement erd;
		public By berd =By.xpath("//*[@id='regform']/div/div[17]/button[1]");
		
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[1]/a/u")  public WebElement erareaw;
		public By berareaw =By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[1]/a/u");
		
		
		//*[@id='datatable-responsive']/tbody/tr/td[1]/a/u
		
		//EqupmentRegistration
		

		@FindBy(xpath="//a[contains(@href,'AreastagesUniqueList')]")  public WebElement ermaster1;
		public By bermaster1 =By.xpath("//a[contains(@href,'AreastagesUniqueList')]");
		
		@FindBy(xpath="//*[@id='blockid']")  public WebElement erblockid;
		public By berblockid =By.xpath("//*[@id='blockid']");
		@FindBy(xpath="//*[@id='LOCATIONID']")  public WebElement erLOCATIONID;
		public By berLOCATIONID =By.xpath("//*[@id='LOCATIONID']");
		@FindBy(xpath="//*[@id='AREAID']")  public WebElement erAREAID;
		public By berAREAID =By.xpath("//*[@id='AREAID']");
		
		@FindBy(xpath="//*[@id='EQUIPMENT_CATEGORY_ID']")  public WebElement erEQUIPMENT_CATEGORY_ID;
		public By berEQUIPMENT_CATEGORY_ID =By.xpath("//*[@id='EQUIPMENT_CATEGORY_ID']");
		
		
		@FindBy(xpath="//*[@id='equiid1']")  public WebElement erequiid1;
		public By berequiid1 =By.xpath("//*[@id='equiid1']");
		@FindBy(xpath="//*[@id='equiname1']")  public WebElement erequiname1;
		public By berequiname1 =By.xpath("//*[@id='equiname1']");
		
		
		@FindBy(xpath="//*[@id='STINWEIGHT1']")  public WebElement erSTINWEIGHT1;
		public By berSTINWEIGHT1 =By.xpath("//*[@id='STINWEIGHT1']");
		@FindBy(xpath="//*[@id='STINTARNET1']")  public WebElement erSTINTARNET1;
		public By berSTINTARNET1 =By.xpath("//*[@id='STINTARNET1']");
		@FindBy(xpath="//*[@id='PARTIALSATRT1']")  public WebElement erPARTIALSATRT1;
		public By berPARTIALSATRT1 =By.xpath("//*[@id='PARTIALSATRT1']");
		@FindBy(xpath="//*[@id='STOUTWEIGHT1']")  public WebElement erSTOUTWEIGHT1;
		public By berSTOUTWEIGHT1 =By.xpath("//*[@id='STOUTWEIGHT1']");
		@FindBy(xpath="//*[@id='STOUTTARNET1']")  public WebElement erSTOUTTARNET1;
		public By berSTOUTTARNET1 =By.xpath("//*[@id='STOUTTARNET1']");
		@FindBy(xpath="//*[@id='Movable1']")  public WebElement erMovable1;
		public By berMovable1 =By.xpath("//*[@id='Movable1']");
		
		
		@FindBy(xpath="//*[@id='dynmicrows']/div[3]/button[1]")  public WebElement eraddrow;
		public By bereraddrow =By.xpath("//*[@id='dynmicrows']/div[3]/button[1]");
		
		
		
		@FindBy(xpath="//*[@id='btnsubmit']")  public WebElement erbtnsubmit;
		public By berbtnsubmit =By.xpath("//*[@id='btnsubmit']");
		
		
		@FindBy(xpath="//*[@id='myModal2']/div/div/div[1]/div/div[1]/h4")  public WebElement altsel;
		public By baltsel =By.xpath("//*[@id='myModal2']/div/div/div[1]/div/div[1]/h4");
	
		@FindBy(xpath="//*[@id='closebtn']")  public WebElement closebtn;
		public By bclosebtn =By.xpath("//*[@id='closebtn']");
		
	
		
		@FindBy(xpath="//*[@id='closebtn']")  public WebElement closebtn1;
		public By bclosebtn1 =By.xpath("//*[@id='closebtn']");
		
		//area process design
		@FindBy(xpath="//a[contains(@href,'AreaProcessDesignList')]")  public WebElement apdsg;
		
		
		@FindBy(xpath="//*[@id='Presentarea']")  public WebElement Presentarea;
		public By bPresentarea =By.xpath("//*[@id='Presentarea']");
		@FindBy(xpath="//*[@id='Areain']")  public WebElement Areain;
		public By bAreain =By.xpath("//*[@id='Areain']");
		@FindBy(xpath="//*[@id='Areaout']")  public WebElement Areaout;
		public By bAreaout =By.xpath("//*[@id='Areaout']");
		@FindBy(xpath="//*[@id='Nextarea']")  public WebElement Nextarea;
		public By bNextarea =By.xpath("//*[@id='Nextarea']");
		@FindBy(xpath="//*[@id='Nextequipment']")  public WebElement Nextequipment;
		public By bNextequipment =By.xpath("//*[@id='Nextequipment']");
		@FindBy(xpath="//*[@id='Equipmentin']")  public WebElement Equipmentin;
		public By bEquipmentin =By.xpath("//*[@id='Equipmentin']");
		@FindBy(xpath="//*[@id='Equipmentout']")  public WebElement Equipmentout;
		public By bEquipmentout =By.xpath("//*[@id='Equipmentout']");
		@FindBy(xpath="//*[@id='Presentequipment']")  public WebElement Presentequipment;
		public By bPresentequipment =By.xpath("//*[@id='Presentequipment']");
	/*	@FindBy(xpath="//*[@id='btnsubmit']")  public WebElement btnsubmit;
		public By bbtnsubmit =By.xpath("//*[@id='btnsubmit']");*/
		
		// instrument registration
		
		
		@FindBy(xpath="//a[contains(@href,'InstrumentList')]")  public WebElement insreg1;
		public By binsreg1 =By.xpath("//a[contains(@href,'InstrumentList')]");
		
		@FindBy(xpath="//*[@id='INSTRUMENT_ID']")  public WebElement inid;
		public By binid =By.xpath("//*[@id='INSTRUMENT_ID']");
		
		@FindBy(xpath="//*[@id='INSTRUMENT_NAME']")  public WebElement inname;
		public By binname =By.xpath("//*[@id='INSTRUMENT_NAME']");
		
		@FindBy(xpath="//*[@id='btnsubmit']")  public WebElement insub;
		public By binsub =By.xpath("//*[@id='btnsubmit']");
		
		// instrument registration flow
		//*[@id='datatable-responsive']/tbody/tr/td[5]/i 
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[5]/i ")  public WebElement irdiv;
		public By birdiv =By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[5]/i ");
		
		@FindBy(xpath="//*[@id='regform']/div/div[3]/button[1]")  public WebElement irr1;
		public By birr1 =By.xpath("//*[@id='regform']/div/div[3]/button[1]");
		
		@FindBy(xpath="//*[@id='Reject']")  public WebElement irrj;
		public By birrj =By.xpath("//*[@id='Reject']");
		
		@FindBy(xpath="//*[@id='regform']/div/div[3]/button[1]")  public WebElement irdrp;
		public By birdrp =By.xpath("//*[@id='regform']/div/div[3]/button[1]");
		//*[@id='regform']/div/div[3]/button[1]
		
		
		
		//Cleaning Master 
		
		@FindBy(xpath="//a[contains(@href,'CreanMasterList')]")  public WebElement clean;
		public By bclean =By.xpath("//a[contains(@href,'CreanMasterList')]");
		
		@FindBy(xpath="//*[@id='TYPEOFCLEANING']")  public WebElement cmtype;
		public By bcmtype =By.xpath("//*[@id='TYPEOFCLEANING']");
		@FindBy(xpath="//*[@id='valarea']")  public WebElement cmarea;
		public By bcmarea =By.xpath("//*[@id='valarea']");
		@FindBy(xpath="//*[@id='valeq']")  public WebElement cmeq;
		public By bcmeq =By.xpath("//*[@id='valeq']");
		@FindBy(xpath="//*[@id='valbin']")  public WebElement cmbin;
		public By bcmbin =By.xpath("//*[@id='valbin']");
		
		@FindBy(xpath="//*[@value='D']")  public WebElement cmdays;
		public By bcmdays =By.xpath("//*[@value='D']");
		
		
		@FindBy(xpath="//*[@value='H']")  public WebElement cmh;
		public By bcmh =By.xpath("//*[@value='H']");
		//btnsubmit
		
		// product assign to equipment category
	
		
		@FindBy(xpath="//a[contains(@href,'IngredientEquipmentcatList')]")  public WebElement paec;
		public By bpaec =By.xpath("//a[contains(@href,'IngredientEquipmentcatList')]");
		
		@FindBy(xpath="//*[@id='LocationType']")  public WebElement LocationType;
		public By bLocationType =By.xpath("//*[@id='LocationType']");
		
		@FindBy(xpath="//*[@id='productcode']")  public WebElement productcode;
		public By bproductcode =By.xpath("//*[@id='productcode']");
		
		@FindBy(xpath="//*[@id='productdescrptn']")  public WebElement productdescrptn;
		public By bproductdescrptn =By.xpath("//*[@id='productdescrptn']");
		
		
		@FindBy(xpath="//*[@id='category_1']")  public WebElement category_1;
		public By bcategory_1 =By.xpath("//*[@id='category_1']");
	
		@FindBy(xpath="//*[@id='btnAssign']")  public WebElement btnAssign;
		public By bbtnAssign =By.xpath("//*[@id='btnAssign']");
		
		@FindBy(xpath="//*[@id='divblock']/div/div/div[1]/div")  public WebElement btnbid;
		public By bbtnbid =By.xpath("//*[@id='divblock']/div/div/div[1]/div");
	
		
		@FindBy(xpath="//*[@id='divblock']/div/div/div[2]/div[3]/div/div")  public WebElement btnbid1;
		public By bbtnbid1 =By.xpath("//*[@id='divblock']/div/div/div[2]/div[3]/div/div");
		
		@FindBy(xpath="//*[@id='divblock']/div/div/div[2]/div[3]/div/span/i")  public WebElement bl1;
		public By bbl1 =By.xpath("//*[@id='divblock']/div/div/div[2]/div[3]/div/span/i");
		
		//*[@id="datatable-responsive"]/tbody/tr/td[1]/a/u
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[1]/a/u")  public WebElement showpaec;
		public By bshowpaec =By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[1]/a/u");
		
		
		
		
		
		
		@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div[1]/ul[2]/li/u/b/a")  public WebElement history;
		public By bhistory =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div[1]/ul[2]/li/u/b/a");
		
		@FindBy(xpath="//*[@id='regform']/div[7]/label")  public WebElement palabel;
		public By bpalabel =By.xpath("//*[@id='regform']/div[7]/label");
		
		//*[@id='regform']/div[7]/label
		
		
		//product assigned to master 
		
		
		@FindBy(xpath="//a[contains(@href,'BatchSplitMasterList')]")  public WebElement pam;
		public By bpam =By.xpath("//a[contains(@href,'BatchSplitMasterList')]");
		
		//BLOCKID   bid
		@FindBy(xpath="//*[@id='holdareaid']")  public WebElement holdareaid;
		public By bholdareaid =By.xpath("//*[@id='holdareaid']");	
	
		@FindBy(xpath="//*[@id='newbatchnumber']")  public WebElement newbatchnumber;
		public By bnewbatchnumber =By.xpath("//*[@id='newbatchnumber']");
		
	
		@FindBy(xpath="//*[@id='rbsamebl' and  @value='0']")  public WebElement rbsamebl;
		public By brbsamebl =By.xpath("//*[@id='rbsamebl' and  @value='0']");	
		
		@FindBy(xpath="//*[@id='rbsamebl' and  @value='1']")  public WebElement rbsameb2;
		public By brbsameb2 =By.xpath("//*[@id='rbsamebl' and  @value='1']");
		
		@FindBy(xpath="//*[@class='fs-label']")  public WebElement fslabel;
		public By bfslabel =By.xpath("//*[@class='fs-label']");
		
	
		@FindBy(xpath="//*[@id='regform1']/div/div[7]/div[2]/div/div[2]/div[3]/div[3]/span")  public WebElement bs1;
		public By bbs1 =By.xpath("//*[@id='regform1']/div/div[7]/div[2]/div/div[2]/div[3]/div[3]/span");
		
		
		
		//ingredientcode1
		
		@FindBy(xpath="//*[@id='btnok']")  public WebElement btnok1;
		public By bbtnok1 =By.xpath("//*[@id='btnok']");
		
		
		
		//Batch Ingredient Assigning To Equipment Category

		@FindBy(xpath="//a[contains(@href,'GetProductsForPacketsList')]")  public WebElement biaec;
		public By bbiaec =By.xpath("//a[contains(@href,'GetProductsForPacketsList')]");
		
		@FindBy(xpath="//*[@id='Blockid']")  public WebElement bid;
		public By bbid =By.xpath("//*[@id='Blockid']");
	
		@FindBy(xpath="//*[@id='Product']")  public WebElement pid;
		public By bpid =By.xpath("//*[@id='Product']");
		
		
		@FindBy(xpath="//*[@id='Batch']")  public WebElement pBatchid;
		public By bpBatchid =By.xpath("//*[@id='Batch']");
		
		@FindBy(xpath="//*[@id='ingredientcode1']")  public WebElement igc;
		public By bigc =By.xpath("//*[@id='ingredientcode1']");
		
		
		@FindBy(xpath="//*[@id='btnok']")  public WebElement btnok;
		public By bbtnok =By.xpath("//*[@id='btnok']");
		
		@FindBy(xpath="//*[@id='btnok1']")  public WebElement btnok11;
		public By bbtnok11 =By.xpath("//*[@id='btnok1']");
		
	
		// Lot Split Master 

		@FindBy(xpath="//a[contains(@href,'LotSplitMasterDetails')]")  public WebElement lsm;
		public By blsm =By.xpath("//a[contains(@href,'LotSplitMasterDetails')]");
		
		@FindBy(xpath="//*[@id='Holdareaid']")  public WebElement Holdareaid1;
		public By bHoldareaid1 =By.xpath("//*[@id='btnok']");
		@FindBy(xpath="//*[@id='Batchno']")  public WebElement Batchno;
		public By bBatchno =By.xpath("//*[@id='Batchno']");
		@FindBy(xpath="//*[@id='Lot']")  public WebElement Lot;
		public By bLot =By.xpath("//*[@id='Lot']");
		
		
		@FindBy(xpath="//*[@id='reggistraion']")  public WebElement reggistraion;
		public By breggistraion =By.xpath("//*[@id='reggistraion']");
		//flw
		

		//a[contains(@href,'BlockMasterList')]/../../../a/i
		
		@FindBy(xpath="//a[contains(@href,'BlockMasterList')]/../../../a/i")  public WebElement fm;
		public By bfm =By.xpath("//a[contains(@href,'BlockMasterList')]/../../../a/i");
		
		
		//user registration test1
		//In what city were you born?
	//	What high school did you attend?
			//	What is the name of your favorite pet?
				//		What is the name of your first school?
								
								
	@FindBy(xpath="//*[@id='s1']")  public WebElement s1;
	public By bs1a =By.xpath("//*[@id='s1']");						
								
	@FindBy(xpath="//*[@id='a1']")  public WebElement a1;
	public By ba1 =By.xpath("//*[@id='a1']");						
		
	@FindBy(xpath="//*[@id='s2']")  public WebElement s2;
	public By bs2 =By.xpath("//*[@id='s2']");						
								
	@FindBy(xpath="//*[@id='a2']")  public WebElement a2;
	public By ba2 =By.xpath("//*[@id='a2']");
	@FindBy(xpath="//*[@id='s3']")  public WebElement s3;
	public By bs3 =By.xpath("//*[@id='s3']");
	
	@FindBy(xpath="//*[@id='a3']")  public WebElement a3;
	public By ba3 =By.xpath("//*[@id='a3']");
	
	@FindBy(xpath="//*[@id='currentpassword']")  public WebElement currentpassword;
	public By bcurrentpassword =By.xpath("//*[@id='currentpassword']");
		
	@FindBy(xpath="//*[@id='password']")  public WebElement password;
	public By bpassword =By.xpath("//*[@id='password']");
	@FindBy(xpath="//*[@id='confirmpassword']")  public WebElement confirmpassword;
	public By bconfirmpassword =By.xpath("//*[@id='confirmpassword']");	
	
	@FindBy(xpath="//*[@type='submit']")  public WebElement usrsub;
	public By busrsub =By.xpath("//*[@type='submit']");
	
		
		
		
		// check list Master
		
		// group master
		
		@FindBy(xpath="//a[contains(@href,'FormateGroupMasterList')]/../../../a/i")  public WebElement clm;
		public By bclm =By.xpath("//a[contains(@href,'FormateGroupMasterList')]/../../../a/i");
		
		
		@FindBy(xpath="//a[contains(@href,'FormateGroupMasterList')]")  public WebElement clgm;
		public By bclgm =By.xpath("//a[contains(@href,'FormateGroupMasterList')]");
		@FindBy(xpath="//*[@id='FORMATEGROUP']")  public WebElement gmname;
		public By bgmname =By.xpath("//*[@id='FORMATEGROUP']");
		@FindBy(xpath="//*[@id='formatedescription']")  public WebElement gmdsc;
		public By bgmdsc =By.xpath("//*[@id='FORMATEGROUP']");
		@FindBy(xpath="//*[@id='btnsubmit']")  public WebElement gmsubmit;
		public By bgmsubmit =By.xpath("//*[@id='btnsubmit']");
		
		@FindBy(xpath="//*[@id='myModal']/div/div/div[1]/div/div[1]/h4")  public WebElement ealert;
				
		@FindBy(xpath="//*[@id='valtrue']")  public WebElement chlgmexist;
		public By bchlgmexist =By.xpath("//*[@id='valtrue']");
		
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[1]")  public WebElement chgdiv1;
		public By bchgdiv1 =By.xpath("//*[@id='datatable-responsives']/tbody/tr/td[1]");
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[2]")  public WebElement chgdiv2;
			public By bchgdiv2 =By.xpath("//*[@id='datatable-responsives']/tbody/tr/td[2]");
		
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[3]")  public WebElement chgdiv22;
		public By bchgdiv22 =By.xpath("//*[@id='datatable-responsives']/tbody/tr/td[3]");
	
		
		
		@FindBy(xpath="//*[@id='myModal']/div/div/div[3]/div/button[1]")  public WebElement chpas;
		public By bchpas =By.xpath("//*[@id='myModal']/div/div/div[3]/div/button[1]");
		
		
		
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[4]")  public WebElement chgdiv4;
		public By bchgdiv4 =By.xpath("//*[@id='datatable-responsives']/tbody/tr/td[4]");
		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[3]/i")  public WebElement chgu;
		
		public By bchgu =By.xpath("//*[@id='datatable-responsives']/tbody/tr/td[3]/i");
		
		@FindBy(xpath="//*[@id='Deactive']")  public WebElement chgpdrop;
		public By bchgpdrop =By.xpath("//*[@id='Deactive']");

	
		@FindBy(xpath="//*[@class='btn btn-primary float-right my-4']")  public WebElement clccopy;
		public By bclccopy =By.xpath("//*[@class='btn btn-primary float-right my-4']");
		
		
		@FindBy(xpath="//*[@id='CopyFromPreviousFormatformategrp']")  public WebElement CopyFromPreviousFormatformategrp;
		public By bCopyFromPreviousFormatformategrp =By.xpath("//*[@id='CopyFromPreviousFormatformategrp']");
		
		@FindBy(xpath="//*[@id='CopyFromPreviousFormatformatidd']")  public WebElement CopyFromPreviousFormatformatidd;
		public By bCopyFromPreviousFormatformatidd =By.xpath("//*[@id='CopyFromPreviousFormatformatidd']");
		
		@FindBy(xpath="//*[@id='copypreviosjson']")  public WebElement copypreviosjson;
		public By bcopypreviosjson =By.xpath("//*[@id='copypreviosjson']");
		
		
		
		@FindBy(xpath="//a[contains(@href,'FrontendFormDisplayList')]")  public WebElement clc;
		public By bclc =By.xpath("//a[contains(@href,'FrontendFormDisplayList')]");
		@FindBy(xpath="//*[@id='formategrp']")  public WebElement clformategrp;
		public By bclformategrp =By.xpath("//*[@id='formategrp']");
		
		@FindBy(xpath="//*[@id='formatidd']")  public WebElement clformatidd;
		public By bclformatidd =By.xpath("//*[@id='formatidd']");
		@FindBy(xpath="//*[@id='formatname']")  public WebElement clformatname;
		public By bclformatname =By.xpath("//*[@id='formatname']");
		@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[2]/div[7]/div[2]/button")  public WebElement clreqf;
		public By bclreqf =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[2]/div[7]/div[2]/button");
				
		@FindBy(xpath="//*[@id='ingredientcode1']")  public WebElement clreqid;
		public By bclreqid =By.xpath("//*[@id='ingredientcode1']");
		
	
		@FindBy(xpath="//*[@id='builder-textarea']")  public WebElement clctext;
		public By bclctext =By.xpath("//*[@id='builder-textarea']");
		@FindBy(xpath="//*[@class='null col-xs-8 col-sm-9 col-md-10 formarea drag-container']")  public WebElement clcdiv;
		public By bclcdiv =By.xpath("//*[@class='null col-xs-8 col-sm-9 col-md-10 formarea drag-container']");
		
		
		@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[1]/input")  public WebElement clcclr;
		public By bclcclr =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[1]/input");
		
		
		@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/ul/li[2]/a")  public WebElement clcdat2;
		public By bclcdat2 =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/ul/li[2]/a");
		
		
		@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div/textarea")  public WebElement clcdatav;
		public By bclcdatav =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div/textarea");
		
		@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[2]/div[2]/button[1]")  public WebElement clcdatas;
		public By bclcdatas =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[2]/div[2]/button[1]");
		
		
		
		@FindBy(xpath="//*[@id='builder-radio']")  public WebElement clcradio;
		public By bclcradio =By.xpath("//*[@id='builder-radio']");
		

		@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[4]/table/tbody/tr[1]/td[1]/div/input")  public WebElement clcradio1;
		public By bclcradio1 =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[4]/table/tbody/tr[1]/td[1]/div/input");
		

		
		@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[4]/table/tfoot/tr/td/button")  public WebElement clcradio1c;
		public By bclcradio1c =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[4]/table/tfoot/tr/td/button");
		
		
		@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[4]/table/tbody/tr[2]/td[1]/div/input")  public WebElement clcradio2;
		public By bclcradio2 =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[4]/table/tbody/tr[2]/td[1]/div/input");
		

		
		
		@FindBy(xpath="//*[@id='builder-select']")  public WebElement clcsel;
		public By bclcsel =By.xpath("//*[@id='builder-select']");
		
			
		
		@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div[4]/table/tbody/tr/td[1]/div/input")  public WebElement clcsel1;
		public By bclcsel1 =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div[4]/table/tbody/tr/td[1]/div/input");
		
		
		@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div[4]/table/tfoot/tr/td/button")  public WebElement clcselc;
		public By bclcselc =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div[4]/table/tfoot/tr/td/button");
		
		
		@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div[4]/table/tbody/tr[2]/td[1]/div/input")  public WebElement clcsel2;
		public By bclcsel2 =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div[4]/table/tbody/tr[2]/td[1]/div/input");
		

		@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/div[6]/div[1]/button")  public WebElement clwf1;
		public By bclwf1 =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/div[6]/div[1]/button");
		
		
		
		@FindBy(xpath="//*[@value='PRODUCTION SUPERVISOR'and @id='intiator']")  public WebElement clwfi1;
		public By bclwfi1 =By.xpath("//*[@value='PRODUCTION SUPERVISOR'and @id='intiator']");
		
		
				
           @FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/div[9]/div/button[1]")  public WebElement clcprv;
	       public By bclcprv =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/div[9]/div/button[1]");
		
				
				@FindBy(xpath="//*[@id='submit1']")  public WebElement clcsub1;
				public By bclcsub1 =By.xpath("//*[@id='submit1']");
		
			// check list creat df
				
				 
			@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div[9]/button[1]")  public WebElement clcdfr;
	 	   public By bclcdfr =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div[9]/button[1]");	
	 	  @FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div[9]/button[2]")  public WebElement clcdfrj;
	 	   public By bclcdfrj =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div[9]/button[2]");
	 	  @FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div[9]/button[1]")  public WebElement clcddr;
	 	   public By bclcddr =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div[9]/button[1]");
		
	 	
	 		@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[4]/i")  public WebElement clcd1;
		 	   public By bclcd1 =By.xpath("//*[@id='datatable-responsives']/tbody/tr/td[4]/i");	
	 	   
		//check list assigning
	
		
		@FindBy(xpath="//a[contains(@href,'FormatAssigningDetails')]")  public WebElement cla;
		public By bcla =By.xpath("//a[contains(@href,'FormatAssigningDetails')]");
		
		@FindBy(xpath="//*[@id='blockname']")  public WebElement claname;
		public By bclaname =By.xpath("//*[@id='blockname']");
		@FindBy(xpath="//*[@id='locationid']")  public WebElement claid;
		public By bclaid =By.xpath("//*[@id='blocklocationidname']");
		
		
		@FindBy(xpath="//*[@id='areaname']")  public WebElement claarea;
		public By bclaarea =By.xpath("//*[@id='areaname']");
		@FindBy(xpath="//*[@id='equimentname']")  public WebElement claeqp;
		public By bclaeqp =By.xpath("//*[@id='equimentname']");
		@FindBy(xpath="//*[@id='department']")  public WebElement cldep;
		public By bcldep =By.xpath("//*[@id='department']");
		
		
		@FindBy(xpath="//*[@id='btnViewChecklist']")  public WebElement clsub;
		public By bclsub =By.xpath("//*[@id='btnViewChecklist']");
		
		@FindBy(xpath="//*[@id='CityInfo_filter']/label/input")  public WebElement clsearch;
		public By bclsearch =By.xpath("//*[@id='CityInfo_filter']/label/input");
		
		@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[1]/div[2]/div/div/div[2]/div/table/tbody/tr/td[4]/input[1]")  public WebElement cldata;
		public By bcldata =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[1]/div[2]/div/div/div[2]/div/table/tbody/tr/td[4]/input[1]t");
		public String bcldata1="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[1]/div[2]/div/div/div[2]/div/table/tbody/tr/td[4]/input[1]";
		
		
		@FindBy(xpath="//*[@id='btnsubmit']")  public WebElement clsubmit;
		public By bclsubmit =By.xpath("//*[@id='btnsubmit']");
	
		//*[@id='datatable-responsive']/tbody/tr/td[5]
		//*[@id='datatable-responsive']/tbody/tr/td[4]/i
		
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[4]")  public WebElement clpad1;
		public By bclpad1 =By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[4]");
		
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[5]")  public WebElement cladiv1;
		public By bcladiv1 =By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[5]");
		
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[6]")  public WebElement cladiv2;
		public By bcladiv2 =By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[6]");
		
		
		// check list priority Assigning 
		@FindBy(xpath="//a[contains(@href,'FormatPriorityDetails')]")  public WebElement clpa;
		public By bclpa =By.xpath("//a[contains(@href,'FormatPriorityDetails')]");
		
		@FindBy(xpath="//*[@id='blockname']")  public WebElement clpb;
		public By bclpb =By.xpath("//*[@id='blockname']");
		@FindBy(xpath="//*[@id='locationid']")  public WebElement clpl;
		public By bclpl =By.xpath("//*[@id='locationid']");
		
		@FindBy(xpath="//*[@id='areaname']")  public WebElement clparea;
		public By bclparea =By.xpath("//*[@id='areaname']");
		@FindBy(xpath="//*[@id='department']")  public WebElement clpd;
		public By bclpd =By.xpath("//*[@id='department']");
		
		
		@FindBy(xpath="//*[@id='priorityid1']")  public WebElement clppri1;
		public By bclppri1 =By.xpath("//*[@id='priorityid1']");
		
		@FindBy(xpath="//*[@id='myModal']/div/div/div[3]/div/button[1]")  public WebElement clpprisub;
		public By bclpprisub =By.xpath("//*[@id='myModal']/div/div/div[3]/div/button[1]");
		
		// checklist dwf
		
		@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div[3]/button[1]")  public WebElement clgrev1;
		public By bclgrev1 =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div[3]/button[1]");
		@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div[3]/button[2]")  public WebElement clgrev2;
		public By bclgrev2 =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div[3]/button[2]");
		
		
		//checklist assign
		//*[@id='datatable-responsive']/tbody/tr/td[4]/i
		@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[1]/a/u")  public WebElement classdiv;
		public By bclassdiv =By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[1]/a/u");
		
		@FindBy(xpath="//*[@id='submit']")  public WebElement clasrev;
		public By bclasrev =By.xpath("//*[@id='submit']");
		
		
		@FindBy(xpath="//*[@id='regform']/div/div[2]/button[2]")  public WebElement clasrej;
		public By bclasrej =By.xpath("//*[@id='regform']/div/div[2]/button[2]");
		@FindBy(xpath="//*[@id='drop']")  public WebElement clad;
		public By bclad =By.xpath("//*[@id='drop']");
				
		//*[@id='datatable-responsive']/tbody/tr/td[1]/a/u
		// checklist priority assigning
		@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/div[2]/button[1]")  public WebElement clpasrev;
		public By bclpasrev =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/div[2]/button[1]");
		
		
				@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/div[2]/button[2]")  public WebElement clpasrej;
		public By bclpasrej =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/div[2]/button[2] ");
	}
