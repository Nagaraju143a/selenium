package searchuser;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import test1.*;
import pages.*;
public class updatedep {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {

		this.driver = base.driver;
		Thread.sleep(900);
		System.out.println("testing on admin user search module ");
		agilepg o = PageFactory.initElements(driver, agilepg.class);
		util oo = PageFactory.initElements(driver, util.class);
		
		try {	
			//oo.flu(o.badmin); 
			Thread.sleep(4000);
			oo.clickpg(o.administrator);
		Thread.sleep(3000);//oo.clickpg(o.usrreg);
		
		}catch (Exception e) {
			// TODO: handle exception
		System.out.println(e.getMessage());
			
		}
		 test = extent.createTest("adminc");
		test.createNode("click on admin");
		 Assert.assertTrue(true);
	}
 
  
	






///////////////////////////////////////////////////////////////////////






@DataProvider(name="auseradd")
public Object[][] adminuseradddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("updatedep","excel/agilen.xlsx");
	
  return object;	 
}






//public  void adminuseraddf(String name, String n,String alname,String fname, String lastn,String gen,String email,String pwd, String cpwd,String mnum, String rnum,String roles, String add1, String add2,String coun,String state,String city, String zip) throws InterruptedException {
	@Test(dataProvider ="auseradd", priority=2)
public void adminuseraddf(String dd, String status,String Remark,String ppwd, String dep, String dsc) throws InterruptedException, IOException{




	 try {
		 
		 
		 System.out.println("user  clicked");	
		 this.driver=base.driver;
			agilepg o = PageFactory.initElements(driver, agilepg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
		 
			oo.clickpg(o.clickdep);
			
			 Thread.sleep(3000);		
			oo.clickpg(o.drs);
			Thread.sleep(3000);	
			//String dd="BA1";
			oo.textpg(o.depsearch, dd);
			
			
			Thread.sleep(3000);	
			
			List <WebElement> searchusize =driver.findElements(By.xpath(o.busersearch1s1));
						
			System.out.println(searchusize.size()+"size......");
			
			
			
			if(searchusize.size()>1){
				System.out.println("matched more rows");
				
				//oo.usersearch(o.ussearchrows, dd);
oo.ulocation(o.ussearchrows, "ul",dd, "/td[1]", "/td[2]");
				
				
				System.out.println("update/drop now");
				Thread.sleep(4000);
				List <WebElement> searchusize1 =driver.findElements(By.xpath(o.buserroleda1));
				if(searchusize1.size()==1){
				
				
				//oo.clickpg(o.depupdatebtn1);
		
				oo.waits(o.buserroleda);		Thread.sleep(3000);	
				
				
				
				if(status.equals("delete")){
				//////////////deactivate
					oo.clickpg(o.userroleda);
					 Thread.sleep(3000);						
						oo.waits(o.bRemark);						
						Thread.sleep(2000);
						 oo.textpg(o.Remark, Remark);
						Thread.sleep(2000);
						 oo.textpg(o.pwd, ppwd);Thread.sleep(2000);
						 oo.clickpg(o.validateuser);
						 oo.alertpg();
						 //alert 
						 Thread.sleep(2000);
						 Assert.assertEquals(o.uaalert.getText(), "DE-ACTIVE IS IN PROCESS");
						 oo.waits(o.bErrorbtn);	 
						 oo.clickpg(o.Errorbtn);	Thread.sleep(2000);						
					
				}
				if(status.equals("update")){
					
					 Thread.sleep(2000);
					
					oo.textpg(o.updatedepname,dep);
					 Thread.sleep(2000);
						oo.textpg(o.updatedepdsc,dsc);
						 Thread.sleep(2000);
					oo.clickpg(o.updatedeps);
						 
					 oo.alertpg();Thread.sleep(2000);
					 Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");
						 oo.clickpg(o.ERES);Thread.sleep(2000);
						 Assert.assertEquals(o.uaalert.getText(), "Department Approve Process Completed Successfully");
						 Thread.sleep(2000);
						 oo.clickpg(o.okbtn);
						 
						 
						 
						 
					}
						 
						 
				}else{
					
					System.out.println("record is deactive");
					System.out.println("not active record");
					Logger ll =Logger.getLogger("deactive");
					ll.trace("deactive");
					
				}	 
						 
						 
						 
				
					}
			else{
	
						
						List <WebElement> searchusize1 =driver.findElements(By.xpath(o.busersearch1s1div));
						if(searchusize1.size()>1){
							System.out.println("record present");
							//System.out.println(o.ussearchdiv1.getText()+"text");
							String ee1=o.ussearchdiv1s.getText();
							String ee2=o.ussearchdiv2s.getText();
							if(ee1.equals(dd))
							{
								System.out.println("role");
								
								oo.clickpg(o.depupdatebtn1);
								Thread.sleep(3000);	
								
							}else if(ee2.equals(dd)){
								System.out.println("role dsc");
								oo.clickpg(o.depupdatebtn1);
								Thread.sleep(3000);	
							}
							
							
							
							oo.waits(o.buserroleda);		Thread.sleep(3000);													
							if(status.equals("delete")){
							//////////////deactivate
								oo.clickpg(o.userroleda);
								 Thread.sleep(3000);						
									oo.waits(o.bRemark);						
									Thread.sleep(2000);
									 oo.textpg(o.Remark, Remark);
									Thread.sleep(2000);
									 oo.textpg(o.pwd, ppwd);Thread.sleep(2000);
									 oo.clickpg(o.validateuser);
									 oo.alertpg();
									 //alert 
									 Thread.sleep(2000);
									 Assert.assertEquals(o.uaalert.getText(), "DE-ACTIVE IS IN PROCESS");
									 oo.waits(o.bErrorbtn);	 
									 oo.clickpg(o.Errorbtn);	Thread.sleep(2000);						
								
								}
							if(status.equals("update")){
								
								 Thread.sleep(2000);
								
								oo.textpg(o.updatedepname,dep);
								 Thread.sleep(2000);
									oo.textpg(o.updatedepdsc,dsc);
									 Thread.sleep(2000);
								oo.clickpg(o.updatedeps);
									 
								 oo.alertpg();Thread.sleep(2000);
								 Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");
									 oo.clickpg(o.ERES);Thread.sleep(2000);
									 Assert.assertEquals(o.uaalert.getText(), "Department Approve Process Completed Successfully");
									 Thread.sleep(2000);
									 oo.clickpg(o.okbtn);
								}
								
							
							
							
							
						}else{
							System.out.println("no record present");
							//trying to fail bcs no data case screenshot 
							test = extent.createTest("no search results we can't update");
							test.createNode("input data based results not there");
							 Assert.assertTrue(false);
						}
					
			}
			
			
			
			
			
					 
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("update dep ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);
	 
	 
	 
	 
	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
		}
	 
	 
	 
	 
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{
		


		this.driver = base.driver;
		Thread.sleep(100);
		System.out.println("testing on admin role module2 ");
		agilepg o = PageFactory.initElements(driver, agilepg.class);
		util oo = PageFactory.initElements(driver, util.class);
		
		try {	
			//oo.flu(o.badmin); 
			Thread.sleep(7000);
			oo.clickpg(o.administrator);
		Thread.sleep(3000);//oo.clickpg(o.usrreg);
		
		}catch (Exception e) {
			// TODO: handle exception
		System.out.println(e.getMessage());
			
		}
		 test = extent.createTest("adminc clicked");
		test.createNode("click on admin2");
		 Assert.assertTrue(true);
		
		
	}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
