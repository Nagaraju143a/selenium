package searchuser;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;
import test1.*;
import pages.*;
public class updatechangedep {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {

		this.driver = base.driver;
		Thread.sleep(900);
		System.out.println("testing on admin user search module ");
		agilepg o = PageFactory.initElements(driver, agilepg.class);
		util oo = PageFactory.initElements(driver, util.class);
		
		try {	
			//oo.flu(o.badmin); 
			Thread.sleep(4000);
			oo.clickpg(o.administrator);
		Thread.sleep(3000);//oo.clickpg(o.usrreg);
		
		}catch (Exception e) {
			// TODO: handle exception
		System.out.println(e.getMessage());
			
		}
		 test = extent.createTest("adminc");
		test.createNode("click on admin");
		 Assert.assertTrue(true);
	}
 
  
	






///////////////////////////////////////////////////////////////////////






@DataProvider(name="auseradd")
public Object[][] adminuseradddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("updatechangedep","excel/agilen.xlsx");
	
  return object;	 
}






//public  void adminuseraddf(String name, String n,String alname,String fname, String lastn,String gen,String email,String pwd, String cpwd,String mnum, String rnum,String roles, String add1, String add2,String coun,String state,String city, String zip) throws InterruptedException {
	@Test(dataProvider ="auseradd", priority=2)
public void adminuseraddf(String dd, String Remark,String ppwd) throws InterruptedException, IOException{




	 try {
		 
		 
		 System.out.println("user  clicked");	
		 this.driver=base.driver;
			agilepg o = PageFactory.initElements(driver, agilepg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
		 
		 if(o.usrreg.isDisplayed()){	 Thread.sleep(3000);
			
			System.out.println("clicked ");}
		 else{
			 System.out.println("administrator clicked then fmaster ");
				oo.clickpg(o.administrator);
				
			}
Thread.sleep(5000);

			oo.clickpg(o.chdep);
			
			 Thread.sleep(3000);		
			//////////////oo.clickpg(o.drs);
			/////////Thread.sleep(3000);	
			//String dd="Amrendra";
			oo.textpg(o.ussearch, dd);
			
			
			Thread.sleep(3000);	
			
			List <WebElement> searchusize =driver.findElements(By.xpath(o.chdepup1));
						
			System.out.println(searchusize.size()+"size......");
			
			
			
			if(searchusize.size()>1){
				System.out.println("matched more rows");
				Logger ll =Logger.getLogger("update change dep ");
				ll.trace("matched more rows");

				
				
				
				oo.ulocation(o.ussearchrow, "ul",dd, "/td[3]", "/td[4]");
				
				
				System.out.println("update/drop now");
				Thread.sleep(4000);
				List <WebElement> searchusize1 =driver.findElements(By.xpath(o.chngedepdrop1));
				if(searchusize1.size()==1){
			
				
				
				//oo.usersearch(o.ussearchrow, dd);
				
				Thread.sleep(1000);
				///oo.clickpg(o.updatechdep);
			
				
				oo.clickpg(o.chngedepdrop);
				
				/////
				 Thread.sleep(3000);
					
					oo.waits(o.bRemark);
					
					Thread.sleep(2000);
					 oo.textpg(o.Remark, Remark);
					Thread.sleep(2000);
					 oo.textpg(o.pwd, ppwd);Thread.sleep(2000);
					 oo.clickpg(o.validateuser);
					 Thread.sleep(2000);
					 
					 
					 Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");
					 oo.waits(o.bERES);	 
					 oo.clickpg(o.ERES);	Thread.sleep(2000);
					 Logger ll1 =Logger.getLogger("update change dep ");
					 ll1.trace(o.uaalert.getText());

					 Assert.assertEquals(o.uaalert.getText(), "Change Department Drop Process Completed Successfully");
				
					 oo.waits(o.bokbtn);	 
					 oo.clickpg(o.okbtn);
					 Thread.sleep(2000);
				}else{
					
					System.out.println("record is deactive");
					System.out.println("not active record");
					Logger lla =Logger.getLogger("not record");
					lla.trace("no record");
					
					
				}
				
					}
			else{
					
						List <WebElement> searchusize1 =driver.findElements(By.xpath(o.bupdatechdepdev));
						if(searchusize1.size()>1){
							System.out.println("record present");
							//System.out.println(o.ussearchdiv1.getText()+"text");
							String ee1=o.ussearchdiv1.getText();
							String ee2=o.ussearchdiv2.getText();
							
							if(o.st.getText().equals("Active"))
							{
							if(ee1.equals(dd))
							{
								System.out.println("userid");
								
								
							}else if(ee2.equals(dd)){
								System.out.println("username");
								
								
							}
							
							
							Thread.sleep(1000);
							oo.clickpg(o.updatechdep);
							
							oo.clickpg(o.chngedepdrop);
							/////
							 Thread.sleep(3000);
								
								oo.waits(o.bRemark);
								
								Thread.sleep(2000);
								 oo.textpg(o.Remark, Remark);
								Thread.sleep(2000);
								 oo.textpg(o.pwd, ppwd);Thread.sleep(2000);
								 oo.clickpg(o.validateuser);
								 Thread.sleep(2000);
								 
								 
								 Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");
								 oo.waits(o.bERES);	 
								 oo.clickpg(o.ERES);	Thread.sleep(2000);
								 
								 Logger ll =Logger.getLogger("updatechangedep ");
								 ll.trace(o.uaalert.getText());

								 Assert.assertEquals(o.uaalert.getText(), "Change Department Drop Process Completed Successfully");
							
								 oo.waits(o.bokbtn);	 
								 oo.clickpg(o.okbtn);
								 Thread.sleep(2000);
							
							}
							}else{
							System.out.println("no record present");
							//trying to fail bcs no data case screenshot 
							test = extent.createTest("no search results we can't update");
							test.createNode("input data based results not there");
							 Assert.assertTrue(false);
						}
					
						}
						
			
			
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);
	
	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
	 
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
		}
	 
	 
	 
	 
	@Test( priority=3)
	 
	public void addclick() throws InterruptedException{
		


		this.driver = base.driver;
		Thread.sleep(100);
		System.out.println("testing on admin role module2 ");
		agilepg o = PageFactory.initElements(driver, agilepg.class);
		util oo = PageFactory.initElements(driver, util.class);
		
		try {	
			//oo.flu(o.badmin); 
			Thread.sleep(7000);
			oo.clickpg(o.administrator);
		Thread.sleep(3000);//oo.clickpg(o.usrreg);
		
		}catch (Exception e) {
			// TODO: handle exception
		System.out.println(e.getMessage());
			
		}
		 test = extent.createTest("adminc clicked");
		test.createNode("click on admin2");
		 Assert.assertTrue(true);
		
		
	}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
